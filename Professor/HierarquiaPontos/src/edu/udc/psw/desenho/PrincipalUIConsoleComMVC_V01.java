package edu.udc.psw.desenho;

import edu.udc.psw.desenho.Controler;
import edu.udc.psw.desenho.ui.UsrInterfaceConsole;

public class PrincipalUIConsoleComMVC_V01 {

	public static void main(String[] args) {
		Controler c = new Controler(15);
		UsrInterfaceConsole ui = new UsrInterfaceConsole(c);
		
		ui.lacoInterface();

	}

}
