package edu.udc.psw.desenho;

import edu.udc.psw.desenho.util.Iterador;
import edu.udc.psw.desenho.util.ListaDupEnc;
import edu.udc.psw.desenho.formas.FormaGeometrica2D;
import edu.udc.psw.desenho.formas.Linha2D;
import edu.udc.psw.desenho.formas.Ponto2D;
import edu.udc.psw.desenho.formas.Retangulo;
import edu.udc.psw.desenho.formas.persistencia.FormaGeometricaArq;
import edu.udc.psw.desenho.formas.persistencia.FormaGeometricaArqSerial;
import edu.udc.psw.desenho.formas.persistencia.FormaGeometricaArqTexto;

import edu.udc.psw.desenho.gui.views.FormaGeometricaView;

public class Controler {
	// O vetor de formas est� sendo substituido por uma lista, 
	// para remover a restri��o de quantidade de formas que podem ser inseridas
	private ListaDupEnc listaFormas;
	
	private ListaDupEnc listaViews;
	private int minX = 0;
	private int maxX = 0;
	private int minY = 0;
	private int maxY = 0;
	
	private DestrutorFormas destrutorFormas;
	private GeradorFormas geradorFormas;
	
	// para manter a compatibilidade com as vers�es anteriores,
	// mantido o construtor que recebe um interio com o tamanho inicial do vertor 
	public Controler(int tam)
	{
		listaFormas = new ListaDupEnc();
		listaViews = new ListaDupEnc();
		
		destrutorFormas = new DestrutorFormas(this, 1.0F, true);
		geradorFormas = new GeradorFormas(this, 1.0F, true);
		
		new Thread(destrutorFormas).start();
		new Thread(geradorFormas).start();
	}
	
	public void adicionaView(FormaGeometricaView view)
	{
		listaViews.insereFim(view);
	}

	public void removeView(FormaGeometricaView view)
	{
		int p = listaViews.consulta(view);
		if(p != -1)
		listaViews.remove(p);
	}
	
	protected void atualizaViews()
	{
		Iterador i = listaViews.inicio();
		FormaGeometricaView view;
		while((view = (FormaGeometricaView)i.proximo()) != null)
		{
			view.atualizar();
		}
	}
	
	public Ponto2D getViewsMaxSize()
	{
		Ponto2D p = new Ponto2D(0, 0);
		Iterador i = listaViews.inicio();
		FormaGeometricaView view;
		while((view = (FormaGeometricaView)i.proximo()) != null)
		{
			p = p.pontoMaximo(view.pontoMaximo());
		}
		return p;
	}
	
	public void gerarFormasPorSegundo(float formasPorSegundo)
	{
		geradorFormas.setFormasPorSegundo(formasPorSegundo);
	}
	public void destruirFormasPorSegundo(float formasPorSegundo)
	{
		destrutorFormas.setFormasPorSegundo(formasPorSegundo);
	}
	public void habilitarGeradorFormas()
	{
		if(geradorFormas.isSuspenso())
			geradorFormas.alternarSuspenso();
	}
	public void desabilitarGeradorFormas()
	{
		if(!geradorFormas.isSuspenso())
			geradorFormas.alternarSuspenso();
	}
	public void habilitarDestrutorFormas()
	{
		if(destrutorFormas.isSuspenso())
			destrutorFormas.alternarSuspenso();
	}
	public void desabilitarDestrutorFormas()
	{
		if(!destrutorFormas.isSuspenso())
			destrutorFormas.alternarSuspenso();
	}

	synchronized public void novaForma(FormaGeometrica2D forma)
	{
		listaFormas.insereFim(forma);
		
		minX = forma.limHorInf() < minX ? forma.limHorInf() : minX;
		minY = forma.limVerInf() < minY ? forma.limVerInf() : minY;
		
		maxX = forma.limHorSup() > maxX ? forma.limHorSup() : maxX;
		maxY = forma.limVerSup() > maxY ? forma.limVerSup() : maxY;

		atualizaViews();
	}
	
	synchronized public void novoPonto(int x, int y)
	{
		Ponto2D p = new Ponto2D(x, y);
		listaFormas.insereFim(p);
		
		minX = p.limHorInf() < minX ? p.limHorInf() : minX;
		minY = p.limVerInf() < minY ? p.limVerInf() : minY;
		
		maxX = p.limHorSup() > maxX ? p.limHorSup() : maxX;
		maxY = p.limVerSup() > maxY ? p.limVerSup() : maxY;

		atualizaViews();
	}
	
	synchronized public void novaLinha(int xi, int yi, int xf, int yf)
	{
		Linha2D l = new Linha2D(new Ponto2D(xi, yi), new Ponto2D(xf, yf));
		listaFormas.insereFim(l);

		minX = l.limHorInf() < minX ? l.limHorInf() : minX;
		minY = l.limVerInf() < minY ? l.limVerInf() : minY;
		
		maxX = l.limHorSup() > maxX ? l.limHorSup() : maxX;
		maxY = l.limVerSup() > maxY ? l.limVerSup() : maxY;

		atualizaViews();
	}

	synchronized public void novoRetangulo(int xi, int yi, int xf, int yf)
	{

		Retangulo r = new Retangulo(new Ponto2D(xi, yi), new Ponto2D(xf, yf));
		listaFormas.insereFim(r);
		
		minX = r.limHorInf() < minX ? r.limHorInf() : minX;
		minY = r.limVerInf() < minY ? r.limVerInf() : minY;
		
		maxX = r.limHorSup() > maxX ? r.limHorSup() : maxX;
		maxY = r.limVerSup() > maxY ? r.limVerSup() : maxY;
		
		atualizaViews();
	}
	
	synchronized public FormaGeometrica2D removeForma(int ind)
	{
		if(ind > listaFormas.tamanho() && listaFormas.tamanho() > 0)
			return null;
		
		FormaGeometrica2D forma = (FormaGeometrica2D) listaFormas.remove(ind);
		if(forma.limHorInf() == minX || forma.limHorSup() == maxX || forma.limVerInf() == minY || forma.limVerSup() == maxY)
		{		
			FormaGeometrica2D f = (FormaGeometrica2D) listaFormas.consulta(0);
			if(f != null)
			{
				minX = f.limHorInf();
				maxX = f.limHorSup();
				minY = f.limVerInf();
				maxY = f.limVerSup();
				Iterador i = listaFormas.inicio();
				while((f = (FormaGeometrica2D)i.proximo()) != null)
				{
					minX = f.limHorInf() < minX ? f.limHorInf() : minX;
					minY = f.limVerInf() < minY ? f.limVerInf() : minY;
					
					maxX = f.limHorSup() > maxX ? f.limHorSup() : maxX;
					maxY = f.limVerSup() > maxY ? f.limVerSup() : maxY;
				}
			}
		}
		
		atualizaViews();
		return forma;
	}

	synchronized public int getQtdFormas()
	{
		return listaFormas.tamanho();
	}
	
	synchronized public FormaGeometrica2D getFigura(int pos)
	{
		return (FormaGeometrica2D) listaFormas.consulta(pos);
	}

	synchronized public void lerArquivo(String ext)
	{
		FormaGeometricaArq arq = null;
		
		if(ext.compareTo("serial") == 0)
			arq = new FormaGeometricaArqSerial();
		if(ext.compareTo("txt") == 0)
			arq = new FormaGeometricaArqTexto();
		
		listaFormas = arq.lerListaFormas();
		
		atualizaViews();
	}
	
	synchronized public void salvarArquivo(String ext)
	{
		FormaGeometricaArq arq = null;
		
		if(ext.compareTo("serial") == 0)
			arq = new FormaGeometricaArqSerial();
		if(ext.compareTo("txt") == 0)
			arq = new FormaGeometricaArqTexto();

		arq.salvarListaFormas(listaFormas);
	}
		
	public int getMinX()
	{
		return minX;
	}
	public int getMaxX()
	{
		return maxX;
	}
	public int getMinY()
	{
		return minY;
	}
	public int getMaxY()
	{
		return maxY;
	}

	public Iterador getIteradorFormas() {
		return listaFormas.inicio();
	}
}