/**
 * 
 */
package edu.udc.psw.desenho.unitTests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.udc.psw.desenho.formas.Ponto2D;

/**
 * @author Miguel
 * 	Exemplo de teste unit�rio utilizando JUnit.
 *  Testes para a classe Ponto2D.
 *
 */
public class Ponto2DTest {
	
	Ponto2D p1, p2, p3, p4, p5;
	int x, y;
	
	public Ponto2DTest()
	{
		p1 = new Ponto2D();
		p2 = new Ponto2D(1, 1);
		p3 = new Ponto2D(3, 5);
		p4 = new Ponto2D(321654, 456123);
		p5 = new Ponto2D(3, 5);
		x = 7;
		y = 9;
	}

	/**
	 * @throws java.lang.Exception
	 * 
	 * M�todo chamado antes de iniciar os testes. Para realizar configura��es globais, 
	 * como conex�o a bases de dados, ou a servi�os remotos. 
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {		
	}

	/**
	 * @throws java.lang.Exception
	 * M�todo chamado ap�s a realiza��o de todos os testes, para realizar a libera��o 
	 * dos recursos alocados na inicializa��o dos testes, como conex�o a bases de dados
	 * ou servi�os remotos. 
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 * M�todo chamado imediatamente antes de cada um dos testes. Utilizado para configura��es
	 * necess�rias durante a execu��o dos testes, como configura��o de valores, aloca��o de
	 * recursos auxiliares, etc.
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 * M�todo chamado imediatamente ap�s a execu��o de cada teste, para libera��o de recursos 
	 * auxiliares, entre outros.
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link edu.udc.psw.model.Ponto2D#area()}.
	 * 
	 */
	@Test
	public void testAreaDeveriaSerZero() {
		assertEquals("�rea do ponto deveria ser 0", 0F, p1.area(), 0.000001F); 		
		assertEquals("�rea do ponto deveria ser 0", 0F, p2.area(), 0.000001F); 		
		assertEquals("�rea do ponto deveria ser 0", 0F, p3.area(), 0.000001F); 		
		assertEquals("�rea do ponto deveria ser 0", 0F, p4.area(), 0.000001F); 		
	}

	/**
	 * Test method for {@link edu.udc.psw.model.Ponto2D#tamHorizontal()}.
	 */
	@Test
	public void testTamHorizontalDeveriaSerZero() {
		assertEquals("O tamanho horizontal do ponto deveria ser 0", 0F, p1.tamHorizontal(), 0.000001F); 		
		assertEquals("O tamanho horizontal do ponto deveria ser 0", 0F, p2.tamHorizontal(), 0.000001F); 		
		assertEquals("O tamanho horizontal do ponto deveria ser 0", 0F, p3.tamHorizontal(), 0.000001F); 		
		assertEquals("O tamanho horizontal do ponto deveria ser 0", 0F, p4.tamHorizontal(), 0.000001F); 		
	}

	/**
	 * Test method for {@link edu.udc.psw.model.Ponto2D#tamVertical()}.
	 */
	@Test
	public void testTamVerticalDeveriaSerZero() {
		assertEquals("O tamanho vertical do ponto deveria ser 0", 0F, p1.tamVertical(), 0.000001F); 		
		assertEquals("O tamanho vertical do ponto deveria ser 0", 0F, p2.tamVertical(), 0.000001F); 		
		assertEquals("O tamanho vertical do ponto deveria ser 0", 0F, p3.tamVertical(), 0.000001F); 		
		assertEquals("O tamanho vertical do ponto deveria ser 0", 0F, p4.tamVertical(), 0.000001F); 		
	}

	/**
	 * Test method for {@link edu.udc.psw.model.Ponto2D#perimetro()}.
	 */
	@Test
	public void testPerimetroDeveriaSerZero() {
		assertEquals("O per�metro do ponto deveria ser 0", 0F, p1.perimetro(), 0.000001F); 		
		assertEquals("O per�metro do ponto deveria ser 0", 0F, p2.perimetro(), 0.000001F); 		
		assertEquals("O per�metro do ponto deveria ser 0", 0F, p3.perimetro(), 0.000001F); 		
		assertEquals("O per�metro do ponto deveria ser 0", 0F, p4.perimetro(), 0.000001F); 		
	}

	/**
	 * Test method for {@link edu.udc.psw.model.Ponto2D#centro()}.
	 */
	@Test
	public void testCentroDeveriaSerEleMesmo() {
		assertEquals("O centro do ponto deveria ser ele mesmo", p1, p1.centro()); 		
		assertEquals("O centro do ponto deveria ser ele mesmo", p2, p2.centro()); 		
		assertEquals("O centro do ponto deveria ser ele mesmo", p3, p3.centro()); 		
		assertEquals("O centro do ponto deveria ser ele mesmo", p4, p4.centro()); 		
	}

	/**
	 * Test method for {@link edu.udc.psw.model.Ponto2D#clone()}.
	 */
	@Test
	public void testClone() {
		Ponto2D p = p2.clone();
		assertFalse("M�todo clone() n�o pode retornar uma referencia ao mesmo objeto.", p == p2);
		assertTrue("M�todo clone() deve retornar um ponto com as mesmas coordenadas x e y.", 
				p.getX() == p2.getX() && p.getY() == p2.getY());
	}

	/**
	 * Test method for {@link edu.udc.psw.model.Ponto2D#toString()}.
	 */
	@Test
	public void testToString() {
		assertEquals("A representa��o de um ponto deve ser da forma: (x; y)", 
				String.format("(%d; %d)", 0, 0), p1.toString()); 		
		assertEquals("A representa��o de um ponto deve ser da forma: (x; y)", 
				String.format("(%d; %d)", 1, 1), p2.toString()); 		
		assertEquals("A representa��o de um ponto deve ser da forma: (x; y)", 
				String.format("(%d; %d)", 3, 5), p3.toString()); 		
		assertEquals("A representa��o de um ponto deve ser da forma: (x; y)", 
				String.format("(%d; %d)", 321654, 456123), p4.toString()); 		
	}

	/**
	 * Test method for {@link edu.udc.psw.model.Ponto2D#equals(java.lang.Object)}.
	 */
	@Test
	public void testEqualsObject() {
		assertFalse("Compara��o entre pontos diferentes deveria ser falsa", p1.equals(p2));
		assertFalse("Compara��o entre referencias de objetos de classes distintas deveria ser falsa", 
				p1.equals(this));
		assertTrue("Compara��o entre as mesmas referencias deveria ser verdadeira", p1.equals(p1));
		assertTrue("Compara��o entre pontos com as mesmas coordenadas deveria ser verdadeira", p3.equals(p5));
	}

	/**
	 * Test method for {@link edu.udc.psw.model.Ponto2D#distancia(edu.udc.psw.model.Ponto2D)}.
	 */
	@Test
	public void testDistancia() {
		assertEquals("Dist�ncia do ponto at� ele mesmo deveria ser 0", 0F, p1.distancia(p1), 0.000001F); 
		assertEquals("Dist�ncia do ponto at� ele mesmo deveria ser 0", 0F, p2.distancia(p2), 0.000001F); 
		assertEquals("Dist�ncia do ponto at� ele mesmo deveria ser 0", 0F, p3.distancia(p3), 0.000001F); 
		assertEquals("Dist�ncia do ponto at� ele mesmo deveria ser 0", 0F, p4.distancia(p4), 0.000001F);
		
		assertTrue("As dist�ncias entre dois pontos deveriam ser iguais", 
				p1.distancia(p2) == p2.distancia(p1));
		assertTrue("As dist�ncias entre dois pontos deveriam ser iguais", 
				p1.distancia(p3) == p3.distancia(p1));
		assertTrue("As dist�ncias entre dois pontos deveriam ser iguais", p3.distancia(p4) == p4.distancia(p3));
		assertTrue("As dist�ncias entre dois pontos deveriam ser iguais", p1.distancia(p4) == p4.distancia(p1));

		assertEquals("Dist�ncia entre dois pontos iguais deveria ser 0",
				0F, p3.distancia(p5), 0.000001F); 		
		assertEquals("Dist�ncia entre dois pontos iguais deveria ser 0",
				0F, p5.distancia(p3), 0.000001F); 		
		
		// Estes asserts n�o s�o usuais, uma vez que reproduzem a formula utilizada no m�todo
		assertEquals("Dist�ncia de um ponto at� outro deveria ser ( (x1-x2)^2 + (y1-y2)^2 )^(1/2) -> " + p3 + p2,
				(float)Math.sqrt( (1-3)*(1-3) + (1-5)*(1-5) ), p3.distancia(p2), 0.000001F); 
		assertEquals("Dist�ncia de um ponto at� outro deveria ser ( (x1-x2)^2 + (y1-y2)^2 )^(1/2) -> " + p2 + p4,
				(float)Math.sqrt((1-321654)*(1-321654) + (1-456123)*(1-456123)), p2.distancia(p4), 0.000001F); 
	}

	/**
	 * Test method for {@link edu.udc.psw.model.Ponto2D#distHorizontal(edu.udc.psw.model.Ponto2D)}.
	 */
	@Test
	public void testDistHorizontal() {
		assertEquals("Dist�ncia horizontal do ponto at� ele mesmo deveria ser 0", 0F, p1.distHorizontal(p1), 0.000001F); 
		assertEquals("Dist�ncia horizontal do ponto at� ele mesmo deveria ser 0", 0F, p2.distHorizontal(p2), 0.000001F); 
		assertEquals("Dist�ncia horizontal do ponto at� ele mesmo deveria ser 0", 0F, p3.distHorizontal(p3), 0.000001F); 
		assertEquals("Dist�ncia horizontal do ponto at� ele mesmo deveria ser 0", 0F, p4.distHorizontal(p4), 0.000001F);
		
		assertTrue("As dist�ncias horizontais entre dois pontos deveriam ser iguais", 
				p1.distHorizontal(p2) == p2.distHorizontal(p1));
		assertTrue("As dist�ncias horizontais entre dois pontos deveriam ser iguais", 
				p1.distHorizontal(p3) == p3.distHorizontal(p1));
		assertTrue("As dist�ncias horizontais entre dois pontos deveriam ser iguais", 
				p1.distHorizontal(p4) == p4.distHorizontal(p1));
		assertTrue("As dist�ncias horizontais entre dois pontos deveriam ser iguais", 
				p3.distHorizontal(p4) == p4.distHorizontal(p3));

		assertEquals("Dist�ncia horizontal entre dois pontos iguais deveria ser 0",
				0F, p3.distHorizontal(p5), 0.000001F); 		
		
		// Estes asserts n�o s�o usuais, uma vez que reproduzem a formula utilizada no m�todo
		assertEquals("Dist�ncia horizontal de um ponto at� outro deveria ser a diferen�a entre suas coordenadas x",
				Math.abs(p1.getX() - p2.getX()), p1.distHorizontal(p2), 0.000001F); 
		assertEquals("Dist�ncia horizontal de um ponto at� outro deveria ser a diferen�a entre suas coordenadas x",
				Math.abs(p1.getX() - p3.getX()), p1.distHorizontal(p3), 0.000001F); 
		assertEquals("Dist�ncia horizontal de um ponto at� outro deveria ser a diferen�a entre suas coordenadas x",
				Math.abs(p1.getX() - p4.getX()), p1.distHorizontal(p4), 0.000001F); 
	}

	/**
	 * Test method for {@link edu.udc.psw.model.Ponto2D#distVertical(edu.udc.psw.model.Ponto2D)}.
	 */
	@Test
	public void testDistVertical() {
		assertEquals("Dist�ncia vertical do ponto at� ele mesmo deveria ser 0", 0F, p1.distVertical(p1), 0.000001F); 
		assertEquals("Dist�ncia vertical do ponto at� ele mesmo deveria ser 0", 0F, p2.distVertical(p2), 0.000001F); 
		assertEquals("Dist�ncia vertical do ponto at� ele mesmo deveria ser 0", 0F, p3.distVertical(p3), 0.000001F); 
		assertEquals("Dist�ncia vertical do ponto at� ele mesmo deveria ser 0", 0F, p4.distVertical(p4), 0.000001F);
		
		assertTrue("As dist�ncias verticais entre dois pontos deveriam ser iguais", 
				p1.distVertical(p2) == p2.distVertical(p1));
		assertTrue("As dist�ncias verticais entre dois pontos deveriam ser iguais", 
				p1.distVertical(p3) == p3.distVertical(p1));
		assertTrue("As dist�ncias verticais entre dois pontos deveriam ser iguais", 
				p1.distVertical(p4) == p4.distVertical(p1));
		assertTrue("As dist�ncias verticais entre dois pontos deveriam ser iguais", 
				p3.distVertical(p4) == p4.distVertical(p3));

		assertEquals("Dist�ncia vertical entre dois pontos iguais deveria ser 0",
				0F, p3.distVertical(p5), 0.000001F); 		
		
		// Estes asserts n�o s�o usuais, uma vez que reproduzem a formula utilizada no m�todo
		assertEquals("Dist�ncia vertical de um ponto at� outro deveria ser a diferen�a entre suas coordenadas y",
				Math.abs(p1.getY() - p2.getY()), p1.distVertical(p2), 0.000001F); 
		assertEquals("Dist�ncia vertical de um ponto at� outro deveria ser a diferen�a entre suas coordenadas y",
				Math.abs(p1.getY() - p3.getY()), p1.distVertical(p3), 0.000001F); 
		assertEquals("Dist�ncia vertical de um ponto at� outro deveria ser a diferen�a entre suas coordenadas y",
				Math.abs(p1.getY() - p4.getY()), p1.distVertical(p4), 0.000001F); 
	}

}
