package edu.udc.psw.desenho;

import java.util.Random;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import edu.udc.psw.desenho.formas.FormaGeometrica2D;

public class DestrutorFormas implements Runnable {
	private static Random geradorNum = new Random(); 
	private Lock lockObject; 
	private Condition suspend; // utilizado para suspender e retomar a thread a partir do controlardor
	private boolean suspended = false; // true se a thread for suspensa
	private boolean terminar = false; // true se a thread for finalizada
	
	private Controler controler;
	private long espera; // tempo em milisegundos entre as destrui��es de cada forma
	
	private int contador = 0;

	public DestrutorFormas(Controler controler, float formasPorSegundo, boolean pausa)
	{
		lockObject = new ReentrantLock( true );
		suspend = lockObject.newCondition(); // cria nova Condition
		this.controler = controler;
		
		suspended = pausa;
		
		// determina o tempo de espera pela quantidade de formas a destruir por segundo
		espera = Math.round(1 / formasPorSegundo) * 1000;
		System.out.println("Destrutor criado!");
	}
	
	public void setFormasPorSegundo(float formasPorSegundo)
	{
		espera = Math.round((1 / formasPorSegundo) * 1000);
		espera = espera < 65 ? 65 : espera;
		espera = espera > 5000 ? 5000 : espera;
	}
	
	@Override
	public void run() 
	{
		System.out.println("Destrutor iniciando execu��o!");
		while (!terminar) // execu��o; ser� terminado com uma chamada ao m�todo encerrar()
		{
			lockObject.lock(); // obt�m o bloqueio
			try {
				while (suspended) // faz loop at� n�o ser suspenso
					suspend.await(); // suspende a execu��o do thread					
			} 
			// se a thread foi interrompida durante a espera/pausa
			catch (InterruptedException e) {
				terminar = true; // finaliza a thread
				continue;
			} 
			finally {
				lockObject.unlock(); // desbloqueia o bloqueio
			}

			// destroi uma forma no controlardor
			destruirForma();
			
			try {
				// dorme pelo tempo de espera para destruir a pr�xima forma
				System.out.println("Destrutor esperando por " + espera + " milisegundos.");
	            Thread.sleep( espera );
			}
			// se a thread foi interrompida durante enquanto dormia
			catch (InterruptedException exception) {
				terminar = true; // finaliza a thread
			}
		}
		System.out.println("Destrutor finalizando execu��o!");
	}

	private void destruirForma() {
		if(controler.getQtdFormas() > 0)
		{
			int indiceParaDestruir = geradorNum.nextInt(controler.getQtdFormas());
			FormaGeometrica2D forma = controler.removeForma(indiceParaDestruir);
			contador ++;
			System.out.printf("%3d - %s\n", contador, forma.toString());
		}		
	}

	// altera o estado da thread - suspenso/em execu��o 
	public void alternarSuspenso() {
		suspended = !suspended; // alterna booleano que controla estado
		System.out.println(suspended ? "Destrutor suspenso!" : "Destrutor executando!");
		
		lockObject.lock(); // obt�m bloqueio
		try {
			if (!suspended) // se a thread foi retomada
				suspend.signal(); // retoma a thread
		} 
		finally {
			lockObject.unlock(); // libera o bloqueio
		} 
	} 

	public boolean isSuspenso()
	{
		return suspended;
	}
	
// m�todo chamada para encerrar a execu��o da thread
	public void encerrar()
	{
		terminar = !terminar;
		// se necess�rio, realizar aqui outras atividades para terminar corretamente a execu��o da thread
	}
}
