package edu.udc.psw.desenho.gui;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import edu.udc.psw.desenho.Controler;
import edu.udc.psw.desenho.gui.views.FormaGeometricaViewTabela;
import edu.udc.psw.desenho.gui.views.ViewDesenho;
import edu.udc.psw.desenho.gui.views.ViewDesenhoMouse;
import edu.udc.psw.desenho.gui.views.ViewTexto;
import edu.udc.psw.desenho.PrincipalGUIFrameApp;
import edu.udc.psw.desenho.formas.Linha2D;
import edu.udc.psw.desenho.formas.Ponto2D;
import edu.udc.psw.desenho.formas.Retangulo;
import edu.udc.psw.desenho.gui.dialogos.DialogoPonto;
import edu.udc.psw.desenho.gui.dialogos.DialogoPontos;

public class InterfaceGUIFrame extends JFrame {
	private static final long serialVersionUID = 1L;

	private JPanel contentPane;
	private Controler controler;
	private ViewTexto textArea;
	private JScrollPane scrollDesenho;
	private JScrollPane scrollTabela;
	private ViewDesenho viewDesenho;
	private FormaGeometricaViewTabela viewTabela;
	private JCheckBoxMenuItem habilitaMouse;
	
	public InterfaceGUIFrame(Controler controler) {
		super("Figuras - utilizando Barra de Menu");

		this.controler = controler;

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(3, 3));

		// Determina a opera��o padr�o ao se fechar a janela
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		setBounds(50, 50, 450, 250); // Posi��o e tamanho da janela

		JMenuBar bar = new JMenuBar(); // cria a barra de menus
		setJMenuBar(bar); // adiciona uma barra de menus ao aplicativo
		criarMenuArquivo(bar);
		criarMenuFigura(bar);
		criarMenuAjuda(bar);

		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.WEST);

		textArea = new ViewTexto(controler);
		scrollPane.setViewportView(textArea);
		textArea.setEditable(false);
		
		{
			JPanel tblDes = new JPanel();
			ButtonGroup btnGroup = new ButtonGroup();
			
			tblDes.setLayout(new BorderLayout(0, 0));
			
			JPanel rdBtnPanel = new JPanel();
			tblDes.add(rdBtnPanel, BorderLayout.NORTH);
			
			JRadioButton rdbtnTabela = new JRadioButton("Tabela");
			rdbtnTabela.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					if(((JRadioButton)e.getSource()).isSelected())
					{
						tblDes.remove(scrollDesenho);
						controler.removeView(viewDesenho);
						tblDes.add(scrollTabela, BorderLayout.CENTER);
						controler.adicionaView(viewTabela);
						tblDes.revalidate();
						tblDes.repaint();
					}
				}
			});
			btnGroup.add(rdbtnTabela);
			rdBtnPanel.add(rdbtnTabela);
			
			JRadioButton rdbtnDesenho = new JRadioButton("Desenho");
			rdbtnDesenho.addChangeListener(new ChangeListener() {
				public void stateChanged(ChangeEvent e) {
					if(((JRadioButton)e.getSource()).isSelected())
					{
						tblDes.remove(scrollTabela);
						controler.removeView(viewTabela);
						tblDes.add(scrollDesenho, BorderLayout.CENTER);
						controler.adicionaView(viewDesenho);
						tblDes.revalidate();
						tblDes.repaint();
					}
				}
			});
			btnGroup.add(rdbtnDesenho);
			rdBtnPanel.add(rdbtnDesenho);

			viewDesenho = new ViewDesenhoMouse(controler);
			scrollDesenho = new JScrollPane();
			scrollDesenho.setViewportView(viewDesenho);
			
			viewTabela = new FormaGeometricaViewTabela(controler);
			scrollTabela = new JScrollPane();
			scrollTabela.setViewportView(viewTabela);
			controler.adicionaView(viewTabela);
			tblDes.add(scrollTabela);
			
			rdbtnDesenho.setSelected(true);
			
			contentPane.add(tblDes, BorderLayout.CENTER);

		}
		controler.adicionaView(textArea);
	}
	
	private void criarMenuArquivo(JMenuBar bar)
	{

		JMenu menuArq = new JMenu("Arquivo"); // cria o menu Arquivo
		menuArq.setMnemonic('A'); // configura o mnem�nico como A

		JMenuItem itemArq = new JMenuItem("Ler arquivo serial");
		itemArq.setMnemonic('l');
		menuArq.add(itemArq); // adiciona o item sair ao menu Arquivo
		itemArq.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				controler.lerArquivo("serial");
			}
		});

		itemArq = new JMenuItem("Salvar arquivo serial");
		itemArq.setMnemonic('s');
		menuArq.add(itemArq); // adiciona o item sair ao menu Arquivo
		itemArq.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				controler.salvarArquivo("serial");
			}
		});
		
		itemArq = new JMenuItem("Ler arquivo texto");
		itemArq.setMnemonic('e');
		menuArq.add(itemArq); // adiciona o item sair ao menu Arquivo
		itemArq.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				controler.lerArquivo("txt");
			}
		});

		itemArq = new JMenuItem("Salvar arquivo texto");
		itemArq.setMnemonic('x');
		menuArq.add(itemArq); // adiciona o item sair ao menu Arquivo
		itemArq.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				controler.salvarArquivo("txt");
			}
		});

		JMenuItem itemSair = new JMenuItem("Sair");
		itemSair.setMnemonic('r');
		menuArq.add(itemSair); // adiciona o item sair ao menu Arquivo
		itemSair.addActionListener(new ActionListener() {
			// termina o aplicativo quando o usu�rio clica itemSair
			public void actionPerformed(ActionEvent event) {
				System.exit(0); // encerra o aplicativo
			}
		});
		bar.add(menuArq); // adiciona o menu Arquivo � barra de menus
	}

	private void criarMenuFigura(JMenuBar bar)
	{
		JMenu menu = new JMenu("Figura");
		menu.setMnemonic('F');
		JMenuItem item = new JMenuItem("Ponto...");
		item.setMnemonic('P');
		menu.add(item);
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if(habilitaMouse.isSelected())
					((ViewDesenhoMouse)viewDesenho).novaForma(new Ponto2D());
				else
					cmdNovoPonto();
			}
		});
		item = new JMenuItem("Linha...");
		item.setMnemonic('L');
		menu.add(item);
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if(habilitaMouse.isSelected())
					((ViewDesenhoMouse)viewDesenho).novaForma(new Linha2D());
				else
					cmdNovaLinha();
			}
		});
		item = new JMenuItem("Retangulo...");
		item.setMnemonic('R');
		menu.add(item);
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				if(habilitaMouse.isSelected())
					((ViewDesenhoMouse)viewDesenho).novaForma(new Retangulo());
				else
					cmdNovoRetangulo();
			}
		});
		
		menu.addSeparator();
		
		habilitaMouse = new JCheckBoxMenuItem("Habilita mouse");
		habilitaMouse.setMnemonic('m');
		menu.add(habilitaMouse);
		habilitaMouse.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				// Se o mouse est� habilitado, utiliza ViewDesenhoMouse
				if(e.getStateChange() == ItemEvent.SELECTED)
				{
					controler.removeView(viewDesenho);
					viewDesenho = new ViewDesenhoMouse(controler);
					scrollDesenho.setViewportView(viewDesenho);
					controler.adicionaView(viewDesenho);
				}
				// Caso contr�rio, utiliza ViewDesenho
				else
				{
					controler.removeView(viewDesenho);
					viewDesenho = new ViewDesenho(controler);
					scrollDesenho.setViewportView(viewDesenho);
					controler.adicionaView(viewDesenho);
				}				
			}			
		});
		
		menu.addSeparator();

		JCheckBoxMenuItem habilitaGerador = new JCheckBoxMenuItem("Habilita gerador formas");
		habilitaGerador.setMnemonic('H');
		habilitaGerador.setSelected(false);
		menu.add(habilitaGerador);
		habilitaGerador.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED)
					controler.habilitarGeradorFormas();
				else
					controler.desabilitarGeradorFormas();			
			}			
		});
		item = new JMenuItem("Gerar figuras / s");
		item.setMnemonic('G');
		menu.add(item);
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				String str = JOptionPane.showInputDialog("Quantidade de formas a gerar por segundo?");
				str = str.trim();
				str.replace(',', '.');
				if (str.matches("[+]?[0-9]*\\.?[0-9]+"))
				{
					float qtd = Float.parseFloat(str);
					controler.gerarFormasPorSegundo(qtd);
				}
				else
					JOptionPane.showMessageDialog(null, "Valor informado incorreto!\n Deve ser um n�mero em poto flutuante!", "Entrada Invalida", JOptionPane.ERROR_MESSAGE);
			}
		});

		JCheckBoxMenuItem habilitaDestrutor = new JCheckBoxMenuItem("Habilita destrutor formas");
		habilitaDestrutor.setMnemonic('d');
		habilitaDestrutor.setSelected(false);
		menu.add(habilitaDestrutor);
		habilitaDestrutor.addItemListener(new ItemListener(){
			@Override
			public void itemStateChanged(ItemEvent e) {
				if(e.getStateChange() == ItemEvent.SELECTED)
					controler.habilitarDestrutorFormas();
				else
					controler.desabilitarDestrutorFormas();			
			}			
		});
		item = new JMenuItem("Destruir figuras / s");
		item.setMnemonic('t');
		menu.add(item);
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				String str = JOptionPane.showInputDialog("Quantidade de formas a destruir por segundo?");
				str = str.trim();
				str.replace(',', '.');
				if (str.matches("[+]?[0-9]*\\.?[0-9]+"))
				{
					float qtd = Float.parseFloat(str);
					controler.destruirFormasPorSegundo(qtd);
				}
				else
					JOptionPane.showMessageDialog(null, "Valor informado incorreto!\n Deve ser um n�mero em poto flutuante!", "Entrada Invalida", JOptionPane.ERROR_MESSAGE);
			}
		});

		bar.add(menu);
	}
	
	private void criarMenuAjuda(JMenuBar bar)
	{
		JMenu menu = new JMenu("Ajuda");
		menu.setMnemonic('j');
		JMenuItem item = new JMenuItem("Sobre...");
		item.setMnemonic('S');
		menu.add(item);
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				JOptionPane.showMessageDialog(InterfaceGUIFrame.this,
						"Aplica��o Java exemplificando o uso de Frames e Menus!", 
						"Sobre", JOptionPane.PLAIN_MESSAGE);
			}
		});
		
		menu.addSeparator();

		item = new JMenuItem("Informa��es...");
		item.setMnemonic('I');
		menu.add(item);
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent event) {
				PrincipalGUIFrameApp.getTheApp().getFrameInfo().setVisible(true);
			}
		});
		
		bar.add(menu);
	}
	
	public void lacoInterface() {
		// Torna a janela visivel
		setVisible(true);
	}
	
	private void cmdNovoPonto()
	{
		DialogoPonto dlg = new DialogoPonto("Ler um ponto");
		dlg.setVisible(true);
		if(dlg.getResult() == DialogoPonto.OK)
		{
			controler.novoPonto(dlg.getX(), dlg.getY());
		}
	}
	private void cmdNovaLinha()
	{
		DialogoPontos dlg = new DialogoPontos("Ler uma Linha");
		dlg.setVisible(true);
		if(dlg.getResult() == DialogoPontos.OK)
		{
			controler.novaLinha(dlg.getX1(), dlg.getY1(), dlg.getX2(), dlg.getY2());
		}
	}
	private void cmdNovoRetangulo()
	{
		DialogoPontos dlg = new DialogoPontos("Ler um Retangulo");
		dlg.setVisible(true);
		if(dlg.getResult() == DialogoPontos.OK)
		{
			controler.novoRetangulo(dlg.getX1(), dlg.getY1(), dlg.getX2(), dlg.getY2());
		}
	}
	
}
