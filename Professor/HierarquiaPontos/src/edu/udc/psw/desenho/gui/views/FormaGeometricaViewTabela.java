package edu.udc.psw.desenho.gui.views;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.table.AbstractTableModel;

import edu.udc.psw.desenho.Controler;
import edu.udc.psw.desenho.formas.FormaGeometrica2D;
import edu.udc.psw.desenho.formas.Ponto2D;

import javax.swing.JTable;
import java.awt.BorderLayout;

public class FormaGeometricaViewTabela extends JPanel implements FormaGeometricaView {
	private static final long serialVersionUID = 1L;
	//private Controler controler;
	private JTable table;
	private FormaGeometricaTableModel tblModel;

	public FormaGeometricaViewTabela(Controler controler) {
		//this.controler = controler;
		setLayout(new BorderLayout(0, 0));

		tblModel = new FormaGeometricaTableModel(controler);
		table = new JTable();
		table.setModel(tblModel);
		add( new JScrollPane( table ), BorderLayout.CENTER );
	}

	@Override
	public void atualizar() {
		tblModel.atualizar();          
	}
	@Override
	public Ponto2D pontoMaximo()
	{
		return new Ponto2D(0,0);
	}
}

class FormaGeometricaTableModel extends AbstractTableModel {
	private static final long serialVersionUID = 1L;
	private final String columnNames[] = new String[] { "#", "Tipo", "Pontos", "Centro", "�rea", "Perimetro", "Tam. H.",
			"Tam. V." };
	private final Class<?> columnTypes[] = new Class[] { Integer.class, String.class, String.class, String.class, Float.class,
			Float.class, Integer.class, Integer.class };
	
	private Controler controler;
	
	public FormaGeometricaTableModel(Controler controler)
	{
		this.controler = controler;
	}
	
	public void atualizar()
	{
        // notifica a JTable de que os dados da tabela foram alterados
		fireTableDataChanged();
	}

	@Override
	public Class<?> getColumnClass(int columnIndex) {
		return columnTypes[columnIndex];
	}

	@Override
	public String getColumnName(int column) {
		return columnNames[column];
	}

	@Override
	public int getRowCount() {

		return controler.getQtdFormas();
	}

	@Override
	public int getColumnCount() {

		return 8;
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		FormaGeometrica2D forma = controler.getFigura(rowIndex);
		switch (columnIndex) {
		case 0:
			return rowIndex;
		case 1:
			return forma.getClass().getSimpleName();
		case 2:
			return forma.toString();
		case 3:
			return forma.centro().toString();
		case 4:
			return forma.area();
		case 5:
			return forma.perimetro();
		case 6:
			return forma.tamHorizontal();
		case 7:
			return forma.tamVertical();
		}
		return null;
	}
}
