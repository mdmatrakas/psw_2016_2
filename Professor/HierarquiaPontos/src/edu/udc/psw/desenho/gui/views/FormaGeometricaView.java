package edu.udc.psw.desenho.gui.views;

import edu.udc.psw.desenho.formas.Ponto2D;

public interface FormaGeometricaView {
	public void atualizar();
	public Ponto2D pontoMaximo();
}
