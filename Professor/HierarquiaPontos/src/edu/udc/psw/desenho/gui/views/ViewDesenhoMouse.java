package edu.udc.psw.desenho.gui.views;

import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import edu.udc.psw.desenho.Controler;
import edu.udc.psw.desenho.formas.FormaGeometrica2D;
import edu.udc.psw.desenho.formas.ManipuladorForma;

public class ViewDesenhoMouse extends ViewDesenho implements MouseMotionListener, MouseListener {
	private static final long serialVersionUID = 1L;
	private FormaGeometrica2D forma;
	private ManipuladorForma manipulador;

	public ViewDesenhoMouse(Controler controler) {
		super(controler);
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
	}
	
	public void novaForma(FormaGeometrica2D forma)
	{
		this.forma = forma;
		manipulador = forma.getManipulador();
	}

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(manipulador == null)
			return;
		manipulador.downClick(e.getX(), e.getY());
		getParent().revalidate();
		repaint();
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		if(manipulador == null)
			return;
		manipulador.upClick(e.getX(), e.getY());
		controler.novaForma(forma);
		forma = null;
		manipulador = null;
		getParent().revalidate();
		repaint();
	}

	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		if(manipulador == null)
			return;
		manipulador.drag(e.getX(), e.getY());
		getParent().revalidate();
		repaint();
	}

	@Override
	public void mouseMoved(MouseEvent e) {

	}

	@Override
	public void paint(Graphics g)
	{
		super.paint(g);
		
		if(forma != null)
			forma.desenhar(g);
	}

}
