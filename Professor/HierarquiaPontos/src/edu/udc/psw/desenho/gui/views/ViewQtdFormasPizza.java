package edu.udc.psw.desenho.gui.views;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JPanel;

import edu.udc.psw.desenho.Controler;
import edu.udc.psw.desenho.util.Iterador;
import edu.udc.psw.desenho.formas.FabricaFormas;
import edu.udc.psw.desenho.formas.FormaGeometrica2D;
import edu.udc.psw.desenho.formas.Ponto2D;

public class ViewQtdFormasPizza extends JPanel implements FormaGeometricaView {
	private static final long serialVersionUID = 1L;
	// Define as cores �ndigo e violeta
	final Color		VIOLET	= new Color(128, 0, 128);
	final Color		INDIGO	= new Color(75, 0, 130);

	private Controler controler;
	
	private int qtdFormas[];
	private int totalFormas;
	private Color	cores[]	= { Color.BLUE, Color.GREEN, Color.YELLOW, Color.ORANGE, Color.RED, VIOLET, INDIGO  };
	
	public ViewQtdFormasPizza(Controler controler)
	{
		this.controler = controler;
		qtdFormas = new int[FabricaFormas.qtdFormas()];
		for(int i = 0; i < qtdFormas.length; i ++)
			qtdFormas[i] = 0;
		totalFormas = 0;
	}
	
	@Override
	public void atualizar() {
		for(int i = 0; i < qtdFormas.length; i ++)
			qtdFormas[i] = 0;
		totalFormas = 0;
		
		Iterador it = controler.getIteradorFormas();
		FormaGeometrica2D forma;
		while((forma = (FormaGeometrica2D)it.proximo()) != null)
		{
			qtdFormas[FabricaFormas.tipoForma(forma.getClass())]++;
			totalFormas++;
		}

		if(getParent() != null)
			getParent().revalidate();
		repaint();
	}

	@Override
	public Ponto2D pontoMaximo() {
		return new Ponto2D(0, 0);
	}

	@Override
	public void paint(Graphics g)
	{
		super.paint(g);
		if(totalFormas == 0)
			return;
		int raio = ((getWidth() < getHeight() ? getWidth() : getHeight()) / 2) - 10;

		// centro do painel
		int centerX = getWidth() / 2;
		int centerY = getHeight() / 2;

		int ini = 0;
		int fim = 0;
		// desenha arcos preenchidos com o mais externo
		for (int i = 0; i < qtdFormas.length; i++)
		{
			fim = ini + (360 * qtdFormas[i]) / totalFormas;
			// configura a cor para o arco atual
			g.setColor(cores[i]);
			g.fillArc(centerX - raio, centerY - raio, raio * 2, raio * 2, ini, fim-ini);
			ini = fim;
		}
	}
}
