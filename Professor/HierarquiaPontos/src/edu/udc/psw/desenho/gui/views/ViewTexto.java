package edu.udc.psw.desenho.gui.views;

import javax.swing.JTextArea;

import edu.udc.psw.desenho.Controler;
import edu.udc.psw.desenho.formas.Ponto2D;

public class ViewTexto extends JTextArea implements FormaGeometricaView {
	private static final long serialVersionUID = 1L;
	private Controler controler;
	
	public ViewTexto(Controler controler)
	{
		super(8,8);
		this.controler = controler;
	}

	@Override
	public void atualizar() {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < controler.getQtdFormas(); i++) {
			buf.append(controler.getFigura(i));
			buf.append("\n");
		}
		setText(buf.toString());
	}		
	@Override
	public Ponto2D pontoMaximo()
	{
		return new Ponto2D(0,0);
	}
}
