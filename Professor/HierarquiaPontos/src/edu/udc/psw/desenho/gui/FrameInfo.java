package edu.udc.psw.desenho.gui;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;

import edu.udc.psw.desenho.Controler;
import edu.udc.psw.desenho.gui.views.ViewQtdFormasPizza;
import edu.udc.psw.desenho.PrincipalGUIFrameApp;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class FrameInfo extends JFrame {
	private static final long serialVersionUID = 1L;
	private ViewQtdFormasPizza contentPane;
	
	@SuppressWarnings("unused")
	private Controler controler;


	/**
	 * Create the frame.
	 */
	public FrameInfo(Controler controler) {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent e) {
				PrincipalGUIFrameApp.getTheApp().frameInfoDispose();
				controler.removeView(contentPane);
			}
		});
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new ViewQtdFormasPizza(controler);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		controler.adicionaView(contentPane);
		
		this.controler = controler;
	}

}
