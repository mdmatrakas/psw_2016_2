package edu.udc.psw.desenho.gui.views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;
import edu.udc.psw.desenho.Controler;
import edu.udc.psw.desenho.gui.views.FormaGeometricaView;
import edu.udc.psw.desenho.formas.Ponto2D;;

public class ViewDesenho extends JPanel implements FormaGeometricaView {
	private static final long serialVersionUID = 1L;
	protected Controler controler;

	public ViewDesenho(Controler controler)
	{
		super();
		
		this.controler = controler;
		setBackground(new Color(255,255,255));
	}
	@Override
	public void atualizar() {
		setPreferredSize(new Dimension(controler.getMaxX(), controler.getMaxY()));
		if(getParent() != null)
			getParent().revalidate();
		repaint();
	}
	@Override
	public Ponto2D pontoMaximo()
	{
		Dimension d = this.getSize();
		return new Ponto2D(d.width, d.height);
	}
	
	@Override
	public void paint(Graphics g)
	{
		super.paint(g);
		for (int i = 0; i < controler.getQtdFormas(); i++) 
			controler.getFigura(i).desenhar(g);
	}

}
