package edu.udc.psw.desenho;

import edu.udc.psw.desenho.Controler;
import edu.udc.psw.desenho.ui.UsrInterfaceDialogos;

public class PrincipalUIDialogosComMVC_V02 {
	public static void main(String[] args) {
		Controler c = new Controler(15);
		UsrInterfaceDialogos gui = new UsrInterfaceDialogos(c);		
		gui.lacoInterface();
	}
}
