package edu.udc.psw.desenho.ui;

import java.util.Scanner;

import edu.udc.psw.desenho.Controler;

public class UsrInterfaceConsole {
	private static final String menu[] = {
			"1 - Ponto", 
			"2 - Linha", 
			"3 - Retangulo",
			"4 - Mostrar todos",
			"5 - Sair"};
	
	private Controler controler;
	private Scanner sc;
	
	public UsrInterfaceConsole(Controler c)
	{
		sc = new Scanner(System.in);
		controler = c;
	}
	
	private void mostraMenu()
	{
		for(int i = 0; i < menu.length; i++)
		{
			System.out.println(menu[i]);
		}
	}
	
	private int opcaoMenu()
	{
		System.out.println("Digite a op��o");
		int r = sc.nextInt();
		return r;
	}
	
	private void trataOpcao(int op)
	{
		int xi;
		int xf;
		int yi;
		int yf;
		switch(op)
		{
		case 1:
			System.out.println("Digite a coordenada x");
			xi = sc.nextInt();
			System.out.println("Digite a coordenada y");
			yi = sc.nextInt();
			controler.novoPonto(xi, yi);
			break;
		case 2:
			System.out.println("Digite a primeira coordenada x");
			xi = sc.nextInt();
			System.out.println("Digite a primeira coordenada y");
			yi = sc.nextInt();
			System.out.println("Digite a segunda coordenada x");
			xf = sc.nextInt();
			System.out.println("Digite a segunda coordenada y");
			yf = sc.nextInt();
			controler.novaLinha(xi, yi, xf, yf);
			break;
		case 3:
			System.out.println("Digite a primeira coordenada x");
			xi = sc.nextInt();
			System.out.println("Digite a primeira coordenada y");
			yi = sc.nextInt();
			System.out.println("Digite a segunda coordenada x");
			xf = sc.nextInt();
			System.out.println("Digite a segunda coordenada y");
			yf = sc.nextInt();
			controler.novoRetangulo(xi, yi, xf, yf);
			break;
		case 4:
			for(int i = 0; i < controler.getQtdFormas(); i++)
			{
				System.out.println(controler.getFigura(i));
			}
		default:
				
		}
	}
	
	public void lacoInterface()
	{
		int o = 5;
		do
		{
			mostraMenu();
			o = opcaoMenu();
			trataOpcao(o);
		}while(o != 5);
	}
}
