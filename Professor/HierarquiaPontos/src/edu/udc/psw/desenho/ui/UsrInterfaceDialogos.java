package edu.udc.psw.desenho.ui;

import javax.swing.JOptionPane;

import edu.udc.psw.desenho.Controler;

public class UsrInterfaceDialogos {
	private static final String menu[] = {
			"1 - Ponto", 
			"2 - Linha", 
			"3 - Retangulo",
			"4 - Mostrar todos",
			"5 - Sair"};
	
	private Controler controler;
	
	public UsrInterfaceDialogos(Controler c)
	{
		controler = c;
	}
	
	private int opcaoMenu()
	{
		StringBuffer buf = new StringBuffer();
		buf.append("Digite uma op��o:");
		buf.append("\n");
		for(int i = 0; i < menu.length; i++)
		{
			buf.append(menu[i]);
			buf.append("\n");
		}
		String res = JOptionPane.showInputDialog(null, buf.toString());
		if(res != null && !res.equals(""))
			return Integer.parseInt(res);
		else return 0;
	}
	
	private void trataOpcao(int op)
	{
		int xi;
		int xf;
		int yi;
		int yf;
		String res;
		switch(op)
		{
		case 1:
			res = JOptionPane.showInputDialog(null, "Digite a coordenada x:");
			xi = Integer.parseInt(res);
			res = JOptionPane.showInputDialog(null, "Digite a coordenada y:");
			yi = Integer.parseInt(res);
			controler.novoPonto(xi, yi);
			break;
		case 2:
			res = JOptionPane.showInputDialog(null, "Digite a primeira coordenada x:");
			xi = Integer.parseInt(res);
			res = JOptionPane.showInputDialog(null, "Digite a primeira coordenada y:");
			yi = Integer.parseInt(res);
			res = JOptionPane.showInputDialog(null, "Digite a segunda coordenada x:");
			xf = Integer.parseInt(res);
			res = JOptionPane.showInputDialog(null, "Digite a segunda coordenada y:");
			yf = Integer.parseInt(res);
			controler.novaLinha(xi, yi, xf, yf);
			break;
		case 3:
			res = JOptionPane.showInputDialog(null, "Digite a primeira coordenada x:");
			xi = Integer.parseInt(res);
			res = JOptionPane.showInputDialog(null, "Digite a primeira coordenada y:");
			yi = Integer.parseInt(res);
			res = JOptionPane.showInputDialog(null, "Digite a segunda coordenada x:");
			xf = Integer.parseInt(res);
			res = JOptionPane.showInputDialog(null, "Digite a segunda coordenada y:");
			yf = Integer.parseInt(res);
			controler.novoRetangulo(xi, yi, xf, yf);
			break;
		case 4:
			{
				StringBuffer buf = new StringBuffer();
				int c= 1;
				for(int i = 0; i < controler.getQtdFormas(); i++, c++)
				{
					buf.append(controler.getFigura(i));
					buf.append("\n");
					if(c % 3 == 0){
						JOptionPane.showMessageDialog(null, buf.toString());
						c = 0;
						buf.delete(0, buf.length());
					}
				}
				if(c != 1){
					JOptionPane.showMessageDialog(null, buf.toString());
					c = 0;
					buf.delete(0, buf.length());
				}
			}			
		}
	}
	
	public void lacoInterface()
	{
		int o = 5;
		do
		{
			o = opcaoMenu();
			trataOpcao(o);
		}while(o != 5);
	}

}
