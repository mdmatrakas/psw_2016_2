package edu.udc.psw.desenho;

import edu.udc.psw.desenho.Controler;
import edu.udc.psw.desenho.gui.FrameInfo;
import edu.udc.psw.desenho.gui.InterfaceGUIFrame;

public class PrincipalGUIFrameApp {
	private static PrincipalGUIFrameApp theApp;

	private Controler c = null;
	private InterfaceGUIFrame gui = null;
	
	private FrameInfo frameInfo = null;

	public static void main(String[] args) {
		theApp = getTheApp();
		theApp.gui.lacoInterface();
	}
	
	public static PrincipalGUIFrameApp getTheApp() {
		if(theApp == null)
			theApp = new PrincipalGUIFrameApp();
		return theApp;
	}

	private PrincipalGUIFrameApp()
	{
		c = new Controler(0);
		gui = new InterfaceGUIFrame(c);	
	}

	public Controler getControler() {
		return c;
	}

	public InterfaceGUIFrame getGui() {
		return gui;
	}

	public FrameInfo getFrameInfo() {
		System.out.println("FrameInfo solicitado!");
		if(frameInfo == null)
		{
			System.out.println("FrameInfo criado!");
			frameInfo = new FrameInfo(c);
		}
		return frameInfo;
	}
	
	public void frameInfoDispose() {
		frameInfo = null;
		System.out.println("FrameInfo descartado!");
	}

}
