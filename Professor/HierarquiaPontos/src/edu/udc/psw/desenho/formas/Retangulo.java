package edu.udc.psw.desenho.formas;

import java.awt.Graphics;
import java.io.Serializable;

public class Retangulo implements FormaGeometrica2D, Serializable {
	// Padr�o para o serialVersion a->aplica��o, p->pacote, c->classe : apppccc
	static final long serialVersionUID = 1002003L;
	private Ponto2D a;
	private Ponto2D b;
	
	public Retangulo(Ponto2D a, Ponto2D b) {
		this.a = a.clone();
		this.b = b.clone();
	}
	
	public Retangulo()
	{
		a = new Ponto2D(0, 0);
		b = new Ponto2D(1, 1);
	}
	public static Retangulo ParseRetangulo(String str)
	{
		str = str.trim();
		if(str.matches("\\{\\([0-9]+; [0-9]+\\)\\([0-9]+; [0-9]+\\)\\}"))
		{
			String p = str.substring(1,str.indexOf(')')+1);
			Ponto2D p1 = Ponto2D.ParsePonto2D(p);
			p = str.substring(str.indexOf(')')+1, str.length()-1);
			Ponto2D p2 = Ponto2D.ParsePonto2D(p);
			
			return new Retangulo(p1, p2);
		}
		else
			return null;
	}
	public Ponto2D getA() {
		return a;
	}
	public void setA(Ponto2D a) {
		if(!a.equals(b))
			this.a = a.clone();
	}
	public Ponto2D getB() {
		return b;
	}
	public void setB(Ponto2D b) {
		if(!b.equals(a))
		this.b = b.clone();
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		return new Retangulo(a, b);
	}

	@Override
	public String toString() {
		return String.format("{%s%s}",a,b);
	}
	
	@Override
	public String toUIDString() {
		return String.format("%d {%s%s}", serialVersionUID, a,b);
	}

	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(this.getClass() != obj.getClass())
			return false;
		if(this == obj)
			return true;
//		if(a.equals(((Retangulo)obj).a) && b.equals(((Retangulo)obj).b))
//			return true;
		if(centro().equals( ((Retangulo)obj).centro()) && 
				tamHorizontal() == ((Retangulo)obj).tamHorizontal() &&
				tamVertical() == ((Retangulo)obj).tamVertical())
			return true;
		return false;
	}
	@Override
	public float area() {
		return a.distHorizontal(b) * a.distVertical(b);
	}

	@Override
	public float tamHorizontal() {
		return a.distHorizontal(b);
	}

	@Override
	public float tamVertical() {
		return a.distVertical(b);
	}

	@Override
	public float perimetro() {
		return 2 * (a.distHorizontal(b) + a.distVertical(b));
	}

	@Override
	public Ponto2D centro() {
		return new Ponto2D((a.getX()+b.getX())/2, (a.getY()+b.getY())/2);
	}
	@Override
	public void desenhar(Graphics g) {
		g.drawRect(a.getX() < b.getX() ? a.getX() : b.getX(),
				a.getY() < b.getY() ? a.getY() : b.getY(), 
				a.getX() < b.getX() ? b.getX() - a.getX() : a.getX() - b.getX(), 
				a.getY() < b.getY() ? b.getY() - a.getY() : a.getY() - b.getY());
	}
	@Override
	public int limHorInf() {
		return a.getX() < b.getX() ? a.getX() : b.getX();
	}
	@Override
	public int limHorSup() {
		return a.getX() > b.getX() ? a.getX() : b.getX();
	}
	@Override
	public int limVerInf() {
		return a.getY() < b.getY() ? a.getY() : b.getY();
	}
	@Override
	public int limVerSup() {
		return a.getY() > b.getY() ? a.getY() : b.getY();
	}

	@Override
	public ManipuladorForma getManipulador() {
		return (ManipuladorForma) new ManipuladorRetangulo(this);
	}

}

class ManipuladorRetangulo implements ManipuladorForma {
	private Retangulo retangulo;
	
	public ManipuladorRetangulo(Retangulo retangulo)
	{
		this.retangulo = retangulo;
	}

	@Override
	public void downClick(int x, int y) {
		retangulo.getA().setX(x);
		retangulo.getA().setY(y);
		retangulo.getB().setX(x);
		retangulo.getB().setY(y);
	}

	@Override
	public void upClick(int x, int y) {
	}

	@Override
	public void drag(int x, int y) {
		retangulo.getB().setX(x);
		retangulo.getB().setY(y);
	}

}
