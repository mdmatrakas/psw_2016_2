package edu.udc.psw.desenho.formas.persistencia;

import edu.udc.psw.desenho.util.ListaDupEnc;

public interface FormaGeometricaArq {
	public ListaDupEnc lerListaFormas();
	public void salvarListaFormas(ListaDupEnc lista);
}
