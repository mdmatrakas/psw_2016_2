package edu.udc.psw.desenho.formas;

public interface ManipuladorForma {
	public void downClick(int x, int y);
	public void upClick(int x, int y);
	public void drag(int x, int y);
}
