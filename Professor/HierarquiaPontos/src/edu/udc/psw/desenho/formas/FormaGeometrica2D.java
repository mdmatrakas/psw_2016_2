package edu.udc.psw.desenho.formas;

import java.awt.Graphics;

import edu.udc.psw.desenho.formas.Ponto2D;

public interface FormaGeometrica2D {
	
	public float area();
	public float tamHorizontal();
	public float tamVertical();
	public float perimetro();
	public Ponto2D centro();
	public void desenhar(Graphics g);
	
	public int limHorInf();
	public int limHorSup();
	public int limVerInf();
	public int limVerSup();
	
	public String toUIDString();
	
	public ManipuladorForma getManipulador();
}
