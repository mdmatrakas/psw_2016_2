package edu.udc.psw.desenho.formas;

import java.awt.Graphics;
import java.io.Serializable;

public class Ponto2D implements FormaGeometrica2D, Serializable {
	// Padr�o para o serialVersion a->aplica��o, p->pacote, c->classe : apppccc
	static final long serialVersionUID = 1002001L;
	private int x;
	private int y;

	public Ponto2D() {
		this.x = 0;
		this.y = 0;
	}
	public Ponto2D(int x, int y) {
		this.x = x;
		this.y = y;
	}
	
	public static Ponto2D ParsePonto2D(String str)
	{
		str = str.trim();
		if(str.matches("\\([0-9]+; [0-9]+\\)"))
		{
			String n = str.substring(1,str.indexOf(';'));
			int x = Integer.parseInt(n);
			n = str.substring(str.indexOf(';') + 2, str.indexOf(')'));
			int y = Integer.parseInt(n);
			
			return new Ponto2D(x, y);
		}
		else
			return null;
	}
	
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	@Override
	public Ponto2D clone() {
		return new Ponto2D(x, y);
	}
	@Override
	public String toString() {
		return String.format("(%d; %d)", x, y);
	}
	@Override
	public String toUIDString() {
		return String.format("%d (%d; %d)", serialVersionUID, x, y);
	}
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(this.getClass() != obj.getClass())
			return false;
		if(this == obj)
			return true;
		if(this.x == ((Ponto2D)obj).x && this.y == ((Ponto2D)obj).y)
			return true;
		return false;
	}
	public float distancia(Ponto2D p)
	{
		return (float) Math.sqrt( ((x - p.x)*(x - p.x)) + ((y - p.y)*(y - p.y)) );
	}
	public int distHorizontal(Ponto2D p)
	{
		return Math.abs(x - p.x);
	}
	public Ponto2D pontoMaximo(Ponto2D p)
	{
		return new Ponto2D(x > p.x ? x : p.x, y > p.y ? y : p.y);
	}
	public int distVertical(Ponto2D p)
	{
		return Math.abs(y - p.y);
	}
	@Override
	public float area() {
		return 0;
	}
	@Override
	public float tamHorizontal() {
		return 0;
	}
	@Override
	public float tamVertical() {
		return 0;
	}
	@Override
	public float perimetro() {
		return 0;
	}
	@Override
	public Ponto2D centro() {
		return this;
	}
	@Override
	public void desenhar(Graphics g) {
		g.drawLine(x, y, x, y);
	}
	@Override
	public int limHorInf() {
		return x;
	}
	@Override
	public int limHorSup() {
		return x;
	}
	@Override
	public int limVerInf() {
		return y;
	}
	@Override
	public int limVerSup() {
		return y;
	}
	@Override
	public ManipuladorForma getManipulador() {
		return (ManipuladorForma) new ManipuladorPonto(this);
	}
}

class ManipuladorPonto implements ManipuladorForma {
	private Ponto2D ponto;

	public ManipuladorPonto(Ponto2D ponto) {
		this.ponto = ponto;
	}

	@Override
	public void downClick(int x, int y) {
		ponto.setX(x);
		ponto.setY(y);
	}

	@Override
	public void upClick(int x, int y) {
	}

	@Override
	public void drag(int x, int y) {
		ponto.setX(x);
		ponto.setY(y);
	}

}
