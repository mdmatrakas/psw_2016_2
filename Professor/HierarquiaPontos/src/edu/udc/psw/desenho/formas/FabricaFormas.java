package edu.udc.psw.desenho.formas;

import java.util.Random;

public class FabricaFormas {
	private static Random geradorNum = new Random();
	
	// N�o pode ser instanciada
	private FabricaFormas() {}

	// retorna a quantidade de formas distintas implementadas
	public static int qtdFormas()
	{
		return 3;
	}
	// retorna o tipo de cada forma, pelo seu indice
	public static Class<?> tipoForma(int i)
	{
		switch(i)
		{
		case 0: 
			return Ponto2D.class;
		case 1: 
			return Linha2D.class;
		case 2: 
			return Retangulo.class;
		}
		return null;
	}
	// retorna o indice de cada forma, pela sua classe
	public static int tipoForma(Class<?> c)
	{
		if(c.equals(Ponto2D.class))
			return 0;
		if(c.equals(Linha2D.class))
			return 1;
		if(c.equals(Retangulo.class))
			return 2;
		return -1;
	}
	// constroi uma forma geometrica a partir de seu identificador e da string com seus par�metros
	public static FormaGeometrica2D instanciarForma(long uid, String param)
	{
		// n�o pode ser usado o valor uid em um switch devido ser do tipo long, apenas int � permitido
		if(uid == Ponto2D.serialVersionUID)
			return Ponto2D.ParsePonto2D(param);
		if(uid == Linha2D.serialVersionUID)
			return Linha2D.ParseLinha2D(param);
		if(uid == Retangulo.serialVersionUID)
			return Retangulo.ParseRetangulo(param);
		
		return null;
	}
	// constroi uma forma geometrica aleatoria, no intervalo indicado
	public static FormaGeometrica2D criaFormaAleatoria(int maxX, int maxY)
	{
		int i = geradorNum.nextInt(qtdFormas());
		maxX = maxX <= 1 ? 100 : maxX;
		maxY = maxY <= 1 ? 100 : maxY;
		switch(i)
		{
		case 0: // cria um ponto
			return new Ponto2D(geradorNum.nextInt(maxX), geradorNum.nextInt(maxY));
		case 1: // cria uma linha
		{
			Ponto2D p1 = new Ponto2D(geradorNum.nextInt(maxX), geradorNum.nextInt(maxY));
			Ponto2D p2 = new Ponto2D(geradorNum.nextInt(maxX), geradorNum.nextInt(maxY));
			return new Linha2D(p1, p2);
		}
		case 2: // cria um retangulo
		{
			Ponto2D p1 = new Ponto2D(geradorNum.nextInt(maxX), geradorNum.nextInt(maxY));
			Ponto2D p2 = new Ponto2D(geradorNum.nextInt(maxX), geradorNum.nextInt(maxY));
			return new Retangulo(p1, p2);
		}
		}
		return null;
	}
}
