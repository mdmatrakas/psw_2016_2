package edu.udc.psw.desenho.formas.persistencia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import edu.udc.psw.desenho.util.ListaDupEnc;

public class FormaGeometricaArqSerial implements FormaGeometricaArq{

	private String fileName;
	
	public FormaGeometricaArqSerial()
	{
		fileName = "formas.serial";
	}
	
	public ListaDupEnc lerListaFormas()
	{
		ListaDupEnc listaFormas = null;
		try
		{
			File file = new File(fileName);

			if (!file.exists())
			{
				listaFormas = new ListaDupEnc();
			}
			else
			{
				FileInputStream fis = new FileInputStream(file);
				ObjectInputStream ois = new ObjectInputStream(fis);
				listaFormas = (ListaDupEnc)ois.readObject(); 
				ois.close();
				fis.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		catch (ClassNotFoundException e)
		{
			e.printStackTrace();
		}
		return listaFormas;
	}
	
	public void salvarListaFormas(ListaDupEnc listaFormas)
	{
		try
		{
			File file = new File(fileName);
			FileOutputStream fos;

			fos = new FileOutputStream(file);

			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(listaFormas);
			oos.close();
			fos.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
