package edu.udc.psw.desenho.formas.persistencia;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import edu.udc.psw.desenho.util.Iterador;
import edu.udc.psw.desenho.util.ListaDupEnc;
import edu.udc.psw.desenho.formas.FabricaFormas;
import edu.udc.psw.desenho.formas.FormaGeometrica2D;

public class FormaGeometricaArqTexto implements FormaGeometricaArq{
	private String fileName;
	
	public FormaGeometricaArqTexto()
	{
		fileName = "formas.txt";
	}
	
	public ListaDupEnc lerListaFormas()
	{
		ListaDupEnc listaFormas = new ListaDupEnc();
		try
		{
			File file = new File(fileName);

			if (!file.exists())
			{
				return listaFormas;
			}
			else
			{
				Scanner sc = new Scanner(file);
				while(sc.hasNext())
				{
					long uid = sc.nextLong();
					String line = sc.nextLine();
					listaFormas.insereFim(FabricaFormas.instanciarForma(uid, line));
				}
				sc.close();
			}
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		return listaFormas;
	}
	
	public void salvarListaFormas(ListaDupEnc listaFormas)
	{
		try
		{
			File file = new File(fileName);
			FileWriter fw;

			fw = new FileWriter(file);

			FormaGeometrica2D forma;
			Iterador it = listaFormas.inicio();
			while((forma = (FormaGeometrica2D)it.proximo())!=null)
				fw.write(forma.toUIDString() + "\n");

			fw.close();
		}
		catch (FileNotFoundException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
}
