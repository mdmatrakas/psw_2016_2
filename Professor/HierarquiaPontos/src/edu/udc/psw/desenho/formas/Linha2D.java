package edu.udc.psw.desenho.formas;

import java.awt.Graphics;
import java.io.Serializable;

public class Linha2D implements FormaGeometrica2D, Serializable {
	// Padr�o para o serialVersion a->aplica��o, p->pacote, c->classe : apppccc
	static final long serialVersionUID = 1002002L;
	private Ponto2D a;
	private Ponto2D b;
	
	public Linha2D(Ponto2D a, Ponto2D b) {
		if(a != null && b != null)
		{
			if(!a.equals(b))
			{
				this.a = a.clone();
				this.b = b.clone();
			}
			else
			{
				this.a = a.clone();
				this.b = new Ponto2D(0, 0);
				if(a.equals(b))
				{
					b.setX(1);
					b.setY(1);
				}
			}
		}
	}
	
	public static Linha2D ParseLinha2D(String str)
	{
		str = str.trim();
		if(str.matches("\\[\\([0-9]+; [0-9]+\\)\\([0-9]+; [0-9]+\\)\\]"))
		{
			String p = str.substring(1,str.indexOf(')')+1);
			Ponto2D p1 = Ponto2D.ParsePonto2D(p);
			p = str.substring(str.indexOf(')')+1, str.length()-1);
			Ponto2D p2 = Ponto2D.ParsePonto2D(p);
			
			return new Linha2D(p1, p2);
		}
		else
			return null;
	}
	
	public Linha2D() {
		this.a = new Ponto2D(0,0);
		this.b = new Ponto2D(1,1);
	}
	public Ponto2D getA() {
		return a;
	}
	public void setA(Ponto2D a) {
		if(!a.equals(b))
			this.a = a.clone();
	}
	public Ponto2D getB() {
		return b;
	}
	public void setB(Ponto2D b) {
		if(!b.equals(a))
		this.b = b.clone();
	}
	@Override
	protected Linha2D clone() {
		return new Linha2D(this.a, this.b);
	}
	@Override
	public String toString() {
		return String.format("[%s%s]", a, b);
	}
	@Override
	public String toUIDString() {
		return String.format("%d [%s%s]", serialVersionUID, a, b);
	}
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(this.getClass() != obj.getClass())
			return false;
		if(this == obj)
			return true;
		if(a.equals(((Linha2D)obj).a) && b.equals(((Linha2D)obj).b))
			return true;
		return false;
	}
	@Override
	public float area() {
		return 0;
	}
	@Override
	public float tamHorizontal() {
		return a.distHorizontal(b);
	}
	@Override
	public float tamVertical() {
		return a.distVertical(b);
	}
	@Override
	public float perimetro() {
		return 0;
	}
	@Override
	public Ponto2D centro() {
		return new Ponto2D( (a.getX() + b.getX()) / 2, 
				(a.getY() + b.getY()) / 2 );
	}
	@Override
	public void desenhar(Graphics g) {
		g.drawLine(a.getX(), a.getY(), b.getX(), b.getY());
	}
	@Override
	public int limHorInf() {
		return a.getX() < b.getX() ? a.getX() : b.getX();
	}
	@Override
	public int limHorSup() {
		return a.getX() > b.getX() ? a.getX() : b.getX();
	}
	@Override
	public int limVerInf() {
		return a.getY() < b.getY() ? a.getY() : b.getY();
	}
	@Override
	public int limVerSup() {
		return a.getY() > b.getY() ? a.getY() : b.getY();
	}

	@Override
	public ManipuladorForma getManipulador() {
		return (ManipuladorForma) new ManipuladorLinha(this);
	}

}

class ManipuladorLinha implements ManipuladorForma {
	private Linha2D linha;
	
	public ManipuladorLinha(Linha2D linha)
	{
		this.linha = linha;
	}

	@Override
	public void downClick(int x, int y) {
		linha.getA().setX(x);
		linha.getA().setY(y);
		linha.getB().setX(x);
		linha.getB().setY(y);
	}

	@Override
	public void upClick(int x, int y) {
	}

	@Override
	public void drag(int x, int y) {
		linha.getB().setX(x);
		linha.getB().setY(y);
	}

}
