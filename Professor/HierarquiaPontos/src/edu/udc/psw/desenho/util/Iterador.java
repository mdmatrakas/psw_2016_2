package edu.udc.psw.desenho.util;

public interface Iterador {
	public Object dado();
	public Object proximo();
	public Object anterior();
	public boolean temProximo();
	public boolean temAnterior();
	public int insereAntes(Object obj);
	public int insereDepois(Object obj);
}
