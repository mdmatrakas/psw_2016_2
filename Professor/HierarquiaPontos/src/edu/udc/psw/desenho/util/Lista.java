package edu.udc.psw.desenho.util;

public interface Lista {
	public int tamanho();
	public int numTotalAcessos();
	public int insere(Object obj, int pos);
	public Object remove(int pos);
	public Object consulta(int pos);
	
	public Iterador inicio();
	public Iterador fim();
}
