package edu.udc.psw.exercicio_1_11;

public class Data extends Object {
	private int dia;
	private int mes;
	private int ano;
	
	public void mostraData()
	{
		System.out.printf("%02d/%02d/%4d\n", dia, mes, ano);
	}
	
	public String toString()
	{
		return String.format("%02d/%02d/%4d", dia, mes, ano);
	}

	/**
	 * 
	 * @return Negativo se obj < this, 
	 * zero se iguais, 
	 * positivo se obj maior que this
	 */
	public int comparaData(Data obj)
	{
		if(obj.ano < this.ano)
			return -1;
		if(obj.ano > this.ano)
			return 1;
		
		if(obj.mes < this.mes)
			return -1;
		if(obj.mes > this.mes)
			return 1;

		if(obj.dia < this.dia)
			return -1;
		if(obj.dia > this.dia)
			return 1;
		
		return 0;
}
	
	/**
	 * @return the dia
	 */
	public int getDia() {
		return dia;
	}

	/**
	 * @param dia
	 *            the dia to set
	 */
	public void setDia(int dia) {
		this.dia = dia;
	}

	/**
	 * @return the mes
	 */
	public int getMes() {
		return mes;
	}

	/**
	 * @param mes
	 *            the mes to set
	 */
	public void setMes(int mes) {
		this.mes = mes;
	}

	/**
	 * @return the ano
	 */
	public int getAno() {
		return ano;
	}

	/**
	 * @param ano
	 *            the ano to set
	 */
	public void setAno(int ano) {
		this.ano = ano;
	}
}
