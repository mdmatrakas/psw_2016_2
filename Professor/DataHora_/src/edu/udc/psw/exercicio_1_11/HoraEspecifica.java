package edu.udc.psw.exercicio_1_11;

public class HoraEspecifica extends Data {
	private int hora;
	private int minuto;
	
	public int comparaHoraEspecifica(HoraEspecifica obj)
	{
		int comp = comparaData(obj);
		if(comp == 0)
		{
			if(obj.hora < this.hora)
				return -1;
			if(obj.hora > this.hora)
				return 1;

			if(obj.minuto < this.minuto)
				return -1;
			if(obj.minuto > this.minuto)
				return 1;
			
			return 0;			
		}
		return comp;
	}
	
	public int getHora() {
		return hora;
	}
	public void setHora(int hora) {
		this.hora = hora;
	}
	public int getMinuto() {
		return minuto;
	}
	public void setMinuto(int minuto) {
		this.minuto = minuto;
	}

}
