package edu.udc.psw.exercicio_1_11;

public class DataHora {
	// composição
	private Hora hora;
	private Data data;
	
	public int comparaDataHora(DataHora obj)
	{
		int comp = data.comparaData(obj.data);
		if(comp == 0)
			return hora.comparaHora(obj.hora);
		return comp;
	}
	
	@Override
	public String toString() {
		return String.format(data + " - " + hora);
	}

	/**
	 * @return the hora
	 */
	public Hora getHora() {
		return hora;
	}
	/**
	 * @param hora the hora to set
	 */
	public void setHora(Hora hora) {
		this.hora = hora;
	}
	/**
	 * @return the data
	 */
	public Data getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(Data data) {
		this.data = data;
	}

}
