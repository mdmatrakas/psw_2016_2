package edu.udc.psw.exercicio_1_11;

public class Hora {
	private int hora;
	private int minuto;
	
	public Hora(int hora, int minuto)
	{
		setHora(hora);
		setMinuto(minuto);
	}
	
	public Hora(int hora)
	{
		setHora(hora);
		setMinuto(0);
	}
	
	public Hora()
	{
		hora = 0;
		minuto = 0;
	}
	
	public void mostraHora()
	{
		System.out.printf("%02d:%02d", hora, minuto);
	}
	
	
	
	@Override
	protected Hora clone() {
		Hora novaHora = new Hora(hora, minuto);
		return novaHora;
	}



	/**
	 * 
	 * @return Negativo se obj < this, 
	 * zero se iguais, 
	 * positivo se obj maior que this
	 */
	public int comparaHora(Hora obj)
	{
		if(obj.hora < this.hora)
			return -1;
		if(obj.hora > this.hora)
			return 1;

		if(obj.minuto < this.minuto)
			return -1;
		if(obj.minuto > this.minuto)
			return 1;
		
		return 0;
			
	}

	@Override
	public String toString() {
		return String.format("%d:%d", hora, minuto);
	}



	public int getHora() {
		return hora;
	}

	public void setHora(int hora) {
		if(hora < 0 || hora >= 24)
			this.hora = 0;
		else
			this.hora = hora;
	}

	public int getMinuto() {
		return minuto;
	}

	public void setMinuto(int minuto) {
		if(minuto < 0 || minuto >= 60)
			this.minuto = 0;
		else
			this.minuto = minuto;
	}
}
