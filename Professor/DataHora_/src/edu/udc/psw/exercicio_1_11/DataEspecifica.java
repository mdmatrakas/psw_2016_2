package edu.udc.psw.exercicio_1_11;

public class DataEspecifica extends Hora {
	public DataEspecifica(int hora, int minuto) {
		super(hora, minuto);
	}
	private int dia;
	private int mes;
	private int ano;
	
	public String toString()
	{
		return String.format("%02d/%02d/%4d - %02d:%02d", dia, mes, ano, 
				getHora(), getMinuto());
	}
	
	public int comparaDataEspecifica(DataEspecifica obj)
	{
		if(obj.ano < this.ano)
			return -1;
		if(obj.ano > this.ano)
			return 1;
		
		if(obj.mes < this.mes)
			return -1;
		if(obj.mes > this.mes)
			return 1;

		if(obj.dia < this.dia)
			return -1;
		if(obj.dia > this.dia)
			return 1;

		return comparaHora(obj);
	}
	public int getDia() {
		return dia;
	}
	public void setDia(int dia) {
		this.dia = dia;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
}
