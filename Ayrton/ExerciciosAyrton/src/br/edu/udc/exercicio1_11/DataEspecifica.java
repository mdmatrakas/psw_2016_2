package br.edu.udc.exercicio1_11;

public class DataEspecifica extends Data {
	public DataEspecifica(int dia, int mes, int ano) {
		super(dia, mes, ano);
	}

	private int hora;
	private int minuto;

	public String toString() {
		return String.format("%2d/%2d/%4d - %2d:%2dhrs", getDia(), getMes(), getAno(), hora, minuto);
	}

	public int comparaDataEspecifica(DataEspecifica obj) {

		int comp = comparaData(obj);
		if (comp == 0) {
			if (obj.hora < this.hora)
				return -1;
			if (obj.hora > this.hora)
				return 1;
			if (obj.minuto < this.minuto)
				return -1;
			if (obj.minuto > this.minuto)
				return 1;
			return 0;
		}
		return comp;
	}

	public int getHora() {
		return hora;
	}

	public void setHora(int hora) {
		if (hora < 0 || hora > 24)
			this.hora = 0;
		else
			this.hora = hora;
	}

	public int getMinuto() {
		return minuto;
	}

	public void setMinuto(int minuto) {
		if (minuto < 0 || minuto > 60)
			this.minuto = 0;
		else
			this.minuto = minuto;
	}

}
