package br.edu.udc.exercicio1_11;

public class Principal {

	public static void main(String[] args) {
		Hora hora = new Hora();
		Hora novaHora = new Hora();
		Data novaData = new Data();
		hora.setHora(5);
		hora.setMinuto(45);
		novaHora.setHora(5);
		novaHora.setMinuto(46);

		Data data = new Data();
		data.setDia(13);
		data.setMes(9);
		data.setAno(2016);
		novaData.setDia(14);
		novaData.setMes(9);
		novaData.setAno(2016);
		
		DataHora dataHora = new DataHora();
		DataHora novaDataHora = new DataHora();
		dataHora.setData(data);
		novaDataHora.setData(novaData);
		dataHora.setHora(hora);
		novaDataHora.setHora(novaHora);
		
		
		System.out.println(dataHora.toString());
		System.out.printf("\n");
		System.out.println(novaDataHora.toString());
		System.out.printf("\n");
		System.out.print(dataHora.comparaDataHora(novaDataHora));
		System.out.printf("\n");
		System.out.print(novaDataHora.comparaDataHora(dataHora));

	//	data.mostraData();

	//	hora.mostraHora();

	//	System.out.print(hora.comparaHora(novaHora));
	//	System.out.print(data.comparaData(novaData));
		
	}
}
