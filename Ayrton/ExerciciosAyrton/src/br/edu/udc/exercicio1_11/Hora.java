package br.edu.udc.exercicio1_11;

public class Hora {
	private int hora;
	private int minuto;
	
	public int getHora(){
		return hora;
	}
	public void setHora(int hora){
		if(hora < 0 ||hora>24)
			this.hora = 0;
		else
			this.hora = hora;
	}
	public int getMinuto(){
		return minuto;
	}
	public void setMinuto(int minuto){
		if(minuto<0||minuto>60)
			this.minuto = 0;
		else
			this.minuto = minuto;
	}
	
	
	public void mostraHora(){
		System.out.printf("%02d:%02dhrs",getHora(),getMinuto());
	}
	public Hora clone(){
		Hora novaHora = new Hora();
		return novaHora;
	}
	
	public int comparaHora(Hora hora){
		if(this.getHora() > hora.getHora())
			return -1;
		if(this.getHora() < hora.getHora())
			return 1;
		if(this.getMinuto() > hora.getMinuto())
			return -1;
		if(this.getMinuto() < hora.getMinuto())
			return 1;
		return 0;
	}
	
	
	
	@Override
	public String toString(){
		return String.format("%02d:%02dhrs",hora, minuto);
	}
	
	public Hora(int hora,int minuto){
		setHora(hora);
		setMinuto(minuto);
	}
	public Hora(int hora){
		setHora(hora);
		setMinuto(0);
	}
	public Hora(){
		setHora(0);
		setHora(0);
	}
	
}
