package br.edu.udc.exercicio1_11;

public class Data {
	private int ano;
	private int mes;
	private int dia;

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		if (mes <= 0 || mes > 12)
			this.setMes(1);
		else
			this.mes = mes;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		if (dia <= 0 || dia > 31)
			this.setDia(1);
		else
			this.dia = dia;
	}

	public void mostraData() {
		System.out.printf("%02d/%02d/%4d; ", getDia(), getMes(), getAno());
	}
	public String toString(){
		return String.format("%02d/%02d/%4d",getDia(),getMes(),getAno());
	}
	public int comparaData(Data data){
		if(this.getAno()> data.getAno())
			return -1;
		if(this.getAno()< data.getAno())
			return 1;
		if(this.getMes()> data.getMes())
			return -1;
		if(this.getMes()< data.getMes())
			return 1;
		if(this.getDia()> data.getDia())
			return -1;
		if(this.getDia()< data.getDia())
			return 1;
		return 0;
	}
	public Data(){
		setAno(2016);
		setMes(9);
		setDia(14);
	}
	public Data(int dia){
		setAno(2016);
		setMes(9);
		setDia(dia);
	}
	public Data(int dia, int mes){
		setAno(2016);
		setMes(mes);
		setDia(dia);
	}
	public Data(int dia, int mes, int ano){
		setAno(ano);
		setMes(mes);
		setDia(dia);
	}
}
