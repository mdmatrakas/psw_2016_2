# Faculdades Anglo-Americano de Foz do Iguaçu #
# Ciência da Computação #

## Projeto de Software I - 2016 ##

Exemplos de sala de aula para a disciplina de Projeto de Software I - Faculdade Anglo-Americano de Foz do Iguaçu, Professor Miguel Matrakas.

Grande parte dos exemplos são baseados nos códigos do livro Java como Programar, Deitel, Ed Pearson.

### Objetivo do repositório ###

* Exemplos de sala de aula para a disciplina de Projeto de Software I
* Acompanhamento do desenvolvimento de trabalhos acadêmicos
* Entrega de trabalhos práticos

### Como configurar e utilizar ###

Para o desenvolvimento dos exemplos e trabalhos está sendo utilizada a IDE Eclipse Luna.

* Instale os plug-ins de conexão com repositórios GIT no Eclipse
* Configure o repositório remoto para apontar para este projeto no Bitbucket (https://bitbucket.org/mdmatrakas/psw_2016_2)
* Crie um branch com o seu nome a partir do branch "master"
* Ao criar os seus projetos, faço em uma pasta com o seu nome, para não ocorrer conflitos com os projetos dos colegas (os projetos podem ter os mesmos nomes, o que causará problemas ao se unificar os branchs de desenvolvimento)

### Responsável ###

* Miguel Matrakas