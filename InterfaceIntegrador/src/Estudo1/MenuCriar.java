package Estudo1;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.JSpinner;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.LineBorder;
import java.awt.Color;

public class MenuCriar extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JPanel panel_1;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MenuCriar frame = new MenuCriar();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MenuCriar() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 320, 409);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		JLabel lblInstruesDeCpu = new JLabel("Instru\u00E7\u00F5es CPU:");
		JSpinner instrucaoCPU = new JSpinner();

		panel_1 = new JPanel();
		panel_1.setBackground(new Color(255, 255, 255));
		panel_1.setBorder(new LineBorder(new Color(0, 0, 0)));

		JLabel lblInstruoEs = new JLabel("Instru\u00E7\u00E3o E/S 1:");
		JSpinner instrucaoES1 = new JSpinner();
		// Valor
		JLabel lblInstruoEs_2 = new JLabel("Instru\u00E7\u00E3o E/S 2:");
		JSpinner instrucaoES2 = new JSpinner();

		JLabel lblInstruoEs_1 = new JLabel("Instru\u00E7\u00E3o E/S 3:");
		JSpinner instrucaoES3 = new JSpinner();
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);

		JButton btnCriar = new JButton("Criar");
		btnCriar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				int cmpIES1 = (int) instrucaoES1.getValue(); // Envia o valor do
																// campo ES1
				int cmpIES2 = (int) instrucaoES2.getValue(); // Envia o valor do
																// campo ES2
				int cmpIES3 = (int) instrucaoES3.getValue(); // Envia o valor do
																// campo ES3

				// Chamar m�todo de criar
			}
		});
		JLabel jlabelQtdeProcesso = new JLabel("Qtd. Processos:");

		JSpinner qtdeProcesso = new JSpinner();
		// Enviar a Qtde de Processos

		JLabel lblPrioridade = new JLabel("Prioridade:");

		JLabel lblTeste = new JLabel("Selecione uma cor:");

		JButton btnCor = new JButton("Cor");
		btnCor.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Color color = JColorChooser.showDialog(MenuCriar.this, "Escolher a color", new Color(0, 0, 0));
				panel_1.setBackground(color);

			}
		});

		JComboBox<?> comboBox = new JComboBox<Object>();
		comboBox.setModel(new DefaultComboBoxModel(new String[] { "ALTA", "M\u00C9DIA", "BAIXA" }));

		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Tratar bot�o cancelar
			}

		});
		// Daqui pra baixo � apenas a parte de interface feita pelo Windows
		// Builder.
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
				gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel.createSequentialGroup().addGap(10)
						.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING).addGroup(gl_panel
								.createSequentialGroup().addGroup(gl_panel
										.createParallelGroup(Alignment.LEADING).addGroup(
												gl_panel.createSequentialGroup().addGap(17)
														.addGroup(gl_panel
																.createParallelGroup(Alignment.TRAILING, false)
																.addComponent(lblInstruoEs_2, GroupLayout.DEFAULT_SIZE,
																		90, Short.MAX_VALUE)
																.addComponent(lblInstruoEs_1, GroupLayout.DEFAULT_SIZE,
																		GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addComponent(lblInstruoEs, GroupLayout.DEFAULT_SIZE,
																		GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
																.addGroup(Alignment.LEADING,
																		gl_panel.createParallelGroup(Alignment.TRAILING)
																				.addComponent(lblPrioridade)
																				.addComponent(lblInstruesDeCpu)
																				.addComponent(jlabelQtdeProcesso))))
										.addGroup(gl_panel.createSequentialGroup()
												.addPreferredGap(ComponentPlacement.RELATED).addComponent(btnCancelar)))
								.addGap(18))
								.addGroup(gl_panel.createSequentialGroup().addComponent(lblTeste)
										.addPreferredGap(ComponentPlacement.UNRELATED)))
						.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING).addGroup(gl_panel
								.createSequentialGroup().addGroup(
										gl_panel.createParallelGroup(Alignment.LEADING)
												.addGroup(
														gl_panel.createSequentialGroup()
																.addComponent(btnCor).addPreferredGap(
																		ComponentPlacement.UNRELATED)
																.addComponent(panel_1,
																		GroupLayout.PREFERRED_SIZE, 32,
																		GroupLayout.PREFERRED_SIZE))
												.addGroup(gl_panel.createSequentialGroup().addGap(10).addGroup(gl_panel
														.createParallelGroup(Alignment.TRAILING, false)
														.addGroup(gl_panel.createSequentialGroup()
																.addComponent(instrucaoES1, GroupLayout.DEFAULT_SIZE,
																		47, Short.MAX_VALUE)
																.addPreferredGap(ComponentPlacement.RELATED))
														.addComponent(instrucaoES2).addComponent(instrucaoES3)))
												.addGroup(gl_panel.createSequentialGroup().addGap(10)
														.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
																.addComponent(comboBox, GroupLayout.PREFERRED_SIZE,
																		GroupLayout.DEFAULT_SIZE,
																		GroupLayout.PREFERRED_SIZE)
																.addComponent(qtdeProcesso, GroupLayout.PREFERRED_SIZE,
																		44, GroupLayout.PREFERRED_SIZE)
																.addComponent(instrucaoCPU, GroupLayout.PREFERRED_SIZE,
																		44, GroupLayout.PREFERRED_SIZE))))
								.addGap(54))
								.addGroup(gl_panel.createSequentialGroup().addComponent(btnCriar,
										GroupLayout.PREFERRED_SIZE, 81, GroupLayout.PREFERRED_SIZE)
										.addContainerGap()))));
		gl_panel.setVerticalGroup(gl_panel.createParallelGroup(Alignment.LEADING).addGroup(gl_panel
				.createSequentialGroup().addContainerGap()
				.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(qtdeProcesso, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
								GroupLayout.PREFERRED_SIZE)
						.addComponent(jlabelQtdeProcesso))
				.addPreferredGap(ComponentPlacement.RELATED)
				.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING).addGroup(gl_panel.createSequentialGroup()
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE)
								.addComponent(lblPrioridade))
						.addPreferredGap(ComponentPlacement.RELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblInstruesDeCpu)
								.addComponent(instrucaoCPU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.UNRELATED)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblInstruoEs)
								.addComponent(instrucaoES1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addGap(17)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblInstruoEs_2)
								.addComponent(instrucaoES2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addGap(17)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblInstruoEs_1)
								.addComponent(instrucaoES3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
										GroupLayout.PREFERRED_SIZE))
						.addPreferredGap(ComponentPlacement.RELATED, 59, Short.MAX_VALUE)
						.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(lblTeste)
								.addComponent(btnCor)))
						.addComponent(panel_1, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))
				.addGap(37).addGroup(gl_panel.createParallelGroup(Alignment.BASELINE).addComponent(btnCancelar)
						.addComponent(btnCriar))
				.addGap(24)));
		panel.setLayout(gl_panel);
	}
}
