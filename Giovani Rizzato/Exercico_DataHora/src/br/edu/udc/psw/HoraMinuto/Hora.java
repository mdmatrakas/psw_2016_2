package br.edu.udc.psw.HoraMinuto;

public class Hora {
	
	private int hora;
	private int minuto;
	
	//HORA
	public int getHora() {
		return hora;
	}
	public void setHora(int hora) {
		this.hora = hora;
	}
	
	//MINUTO
	public int getMinuto() {
		return minuto;
	}
	public void setMinuto(int minuto) {
		this.minuto = minuto;
	}
	
	
}
