package br.edu.udc.psw.HoraMinuto;

public class Data {
	
	private int dia;
	private int mes;
	private int ano;
	
	//DIA
	public int getDia() {
		return dia;
	}
	public void setDia(int dia) {
		this.dia = dia;
	}
	
	//MES
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	
	//ANO
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}
}
