package br.edu.udc.psw.listaencadeada;

public class Pilha extends ListaEncadeada {
	
	@Override
	public Object remove(Object identificador){
		
		No removido = buscaNaLista(identificador);
		
		//Apenas efetivamente remove, se o elemento que for procurado para remo��o, for o mais recente a ser colocado.
		if(removido.equals(cabeca.getProximo())){
			removeDaLista(removido);
			return removido.getInformacao();
		}
		
		return null;
	}
}
