package br.edu.udc.psw.listaencadeada;

public class No {
	
	private No proximo;
	private No anterior;
	
	private Object identificador;
	private Object informacao;
	
	public No(No next, No previous, Object id, Object information){
		
		this.proximo = next;
		this.anterior = previous;
		
		//Unica chance que o utilizador tem chance de modificar essas informa��es,
		//ap�s estas informa��es forem recebidas, nenhuma outra fun��o poder� modifica-las.
		this.informacao = information;
		
		//Este objeto deve ter uma fu��o equals implementada, pois ser� utilizada na busca do objeto informa��o atravez dele.
		this.identificador = id;
	}
	
	//PROXIMO
	public No getProximo() {
		return this.proximo;
	}
	public void setProximo(No next) {
		this.proximo = next;
	}
	
	//ANTERIOR
	public No getAnterior() {
		return this.anterior;
	}
	public void setAnterior(No previous) {
		this.anterior = previous;
	}
	
	//INFORMACAO
	//N�o possui set, para proteger a informa��o que j� foi alocada no contrutor,
	//ficando intencionalmente impossivel de ser modificada.
	public Object getInformacao() {
		return this.informacao;
	}
	
	public Object getIdentificador() {
		return this.identificador;
	}
	
	@Override
	public boolean equals(Object obj){
		if(this == obj)
			return true;
		else
			return false;
	}
}
