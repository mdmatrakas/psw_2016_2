package br.edu.udc.psw.listaencadeada;

public class Fila extends  ListaEncadeada {
	
	@Override
	public Object remove(Object identificador){
		
		No removido = buscaNaLista(identificador);
		
		//Apenas efetivamente remove, se o elemento que for procurado para remo��o, for o mais antigo a ser colocado.
		if(removido.getProximo() == null){//ultimo elemento da lista
			removeDaLista(removido);
			return removido.getInformacao();
		}
		
		return null;
	}
}
