package br.edu.udc.psw.listaencadeada;

public class ListaEncadeada {
	
	protected No cabeca;
	
	//CONTRUTOR
	public ListaEncadeada(){
		this.cabeca = new No(null, null, null, null);
		//pois neste instante n�o existe lista, porem � necessario o cabeca para o bom funcionamento da lista,
		//mesmo que s� tenha lixo neste momento.
	}
	
	//ADICIONA
	public void adiciona(Object objAdicionado, Object idAdicionado){
		
		No noAdicionado = new No(this.cabeca.getProximo(), this.cabeca, idAdicionado, objAdicionado);
		this.cabeca.setProximo(noAdicionado);
	}
	
	//BUSCA
	public Object busca(Object indetificador){
		
		No procurado = buscaNaLista(indetificador);
		
		if(procurado != null)
			return procurado.getInformacao();
		else
			return null;//elemento n�o encontrado na lista.
	}
	
	protected No buscaNaLista(Object identificador){
		
		No cursor = this.cabeca;
		
		while(cursor != null){
			if(identificador.equals(cursor.getIdentificador()))
				return cursor;
			
			cursor = cursor.getProximo();
		}
		
		return null;
	}
	
	
	//REMOVE
	public Object remove(Object identificador){
		
		No removido = buscaNaLista(identificador);
		removeDaLista(removido);
		return null;
	}
	
	//Esta fun��o faz a roaloca��o de ponteioros para que seja feito a exclus�o atravez do garbege colector,
	//ou seja, o elemento que ir� ser excluido � apenas isolado por est fun��o, colocando a exclus�o de memorio a cargo do garbege colector.
	protected void removeDaLista(No removido){
		
		No anterior = removido.getAnterior();
		No proximo = removido.getProximo();
		
		anterior.setProximo(removido.getProximo());
		
		if(proximo != null)//se ele n�o estiver no final da lista
			proximo.setAnterior(removido.getAnterior());
	}
}
