package edu.udc.psw.aula03_01;

public class Idade {
	private int anosDeIdade;

	public int getAnosDeIdade() {
		return anosDeIdade;
	}

	public void setAnosDeIdade(int anosDeIdade) {
		if(anosDeIdade < 0)
			return;
		this.anosDeIdade = anosDeIdade;
	}
	
	
}
