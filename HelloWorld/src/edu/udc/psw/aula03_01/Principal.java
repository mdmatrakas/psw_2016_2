package edu.udc.psw.aula03_01;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Idade idade = new Idade();
		
		idade.setAnosDeIdade(23);
		
		System.out.printf("O valor da idade �: %d\n", idade.getAnosDeIdade());

		idade.setAnosDeIdade(-23);
		
		System.out.printf("O valor da idade �: %d\n", idade.getAnosDeIdade());
		
		System.out.println("Qual � a sua idade?");
		
		Scanner sc = new Scanner(System.in);
		idade.setAnosDeIdade( sc.nextInt() );
		System.out.printf("O valor da idade �: %d\n", idade.getAnosDeIdade());
	}

}
