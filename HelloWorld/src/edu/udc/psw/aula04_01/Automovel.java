package edu.udc.psw.aula04_01;

public class Automovel {
	private String modelo;
	private Idade idade;
	
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public Idade getIdade() {
		return idade;
	}
	public void setIdade(Idade idade) {
		this.idade = idade;
	}
	public int getValorIdade()
	{
		return this.idade.getAnosDeIdade();
	}
}
