package edu.udc.psw.aula04_01;

public class Caminhao extends Automovel {
	private int capacidadeDeCarga;

	public int getCapacidadeDeCarga() {
		return capacidadeDeCarga;
	}

	public void setCapacidadeDeCarga(int capacidadeDeCarga) {
		if(capacidadeDeCarga >= 2000 && capacidadeDeCarga < 27000)
			this.capacidadeDeCarga = capacidadeDeCarga;
		else
			this.capacidadeDeCarga = 2000;
	}

}
