package edu.udc.psw.aula04_01;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Idade idade = new Idade();
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Qual � a sua idade?");
		idade.setAnosDeIdade(sc.nextInt());
		
		if(idade.getAnosDeIdade() >= 18 )
			System.out.printf("Voc� � maior de idade\n");
		else
			System.out.printf("Voc� � menor de idade\n");
		
		Automovel auto = new Automovel();
		
		System.out.println("Qual � o modelo do seu automovel?");
		auto.setModelo(sc.next());
		System.out.println("Qual � a idade do seu automovel?");
		Idade idadeAuto = new Idade();
		idadeAuto.setAnosDeIdade(sc.nextInt());
		auto.setIdade(idadeAuto);
		
		System.out.printf("O seu %s tem %d anos!\n", auto.getModelo(), 
				auto.getIdade().getAnosDeIdade());
		
		Caminhao caminhao = new Caminhao();
		System.out.println("Qual � o modelo do seu caminhao?");
		caminhao.setModelo(sc.next());
		System.out.println("Qual � a idade do seu caminhao?");
		Idade idadeCam = new Idade();
		idadeCam.setAnosDeIdade(sc.nextInt());
		caminhao.setIdade(idadeCam);
		System.out.println("Qual � a capacidade de carga do seu caminhao?");
		caminhao.setCapacidadeDeCarga(sc.nextInt());
		
		System.out.printf("O seu %s tem %d anos e %d Kg de capacidade de carga!\n", 
				caminhao.getModelo(), caminhao.getIdade().getAnosDeIdade(), 
				caminhao.getCapacidadeDeCarga());
		
		
		sc.close();
	}

}
