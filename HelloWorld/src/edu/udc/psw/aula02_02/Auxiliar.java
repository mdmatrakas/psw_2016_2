package edu.udc.psw.aula02_02;

public class Auxiliar {
	private int valor;
	
	public int getValor() {
		return valor;
	}

	public void incrementa()
	{
		valor += 5;
	}
	
	public void decrementa()
	{
		valor -= 3;
	}
}
