package a09_05_PainelLinhas;

import javax.swing.JFrame;

/**
 * Aplicativo de teste para exibir um PainelDesenho.
 * @author MDMatrakas
 *
 */
public class TestePainelDesenho
{
   public static void main( String args[] )
   {
      PainelDesenho painel = new PainelDesenho();      
      JFrame app = new JFrame();
      
      app.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      app.add( painel );
      app.setSize( 300, 300 );
      app.setVisible( true );
   }
} 