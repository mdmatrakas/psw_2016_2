package a09_05_PainelLinhas;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;
import javax.swing.JPanel;

/**
 * Programa que utiliza a classe Linha para desenhar linhas aleat�rias.
 * @author MDMatrakas
 *
 */
@SuppressWarnings("serial")
public class PainelDesenho extends JPanel
{
   private Random rand = new Random();   
   private Linha linhas[]; // array de linhas

   // construtor, cria um painel com formas aleat�rias
   public PainelDesenho()
   {
      setBackground( Color.WHITE );
      
      linhas = new Linha[ 5 + rand.nextInt( 5 ) ];

      // cria linhas
      for ( int count = 0; count < linhas.length; count++ )
      {
         // gera coordenadas aleat�rias
         int x1 = rand.nextInt( 300 );
         int y1 = rand.nextInt( 300 );
         int x2 = rand.nextInt( 300 );
         int y2 = rand.nextInt( 300 );
         
         // gera uma cor aleat�ria
         Color color = new Color( rand.nextInt( 256 ), 
            rand.nextInt( 256 ), rand.nextInt( 256 ) );
         
         // adiciona a linha � lista de linhas a ser exibida
         linhas[ count ] = new Linha( x1, y1, x2, y2, color );
      } 
   } 

   // para cada array de forma, desenha as formas individuais
   public void paintComponent( Graphics g )
   {
      super.paintComponent( g );
      
      // desenha as linhas
      for ( Linha line : linhas )
         line.draw( g );
   } 
} 
