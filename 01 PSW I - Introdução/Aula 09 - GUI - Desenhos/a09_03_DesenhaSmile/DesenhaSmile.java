package a09_03_DesenhaSmile;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Demonstra formas preenchidas.
 * 
 * @author Matrakas
 * 
 */
@SuppressWarnings("serial")
public class DesenhaSmile extends JPanel
{
	public static void main(String args[])
	{
		DesenhaSmile painel = new DesenhaSmile();
		JFrame app = new JFrame();

		app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		app.add(painel);
		app.setSize(230, 250);
		app.setVisible(true);
	}

	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);

		// desenha o rosto
		g.setColor(Color.YELLOW);
		g.fillOval(10, 10, 200, 200);

		// desenha os olhos
		g.setColor(Color.BLACK);
		g.fillOval(55, 65, 30, 30);
		g.fillOval(135, 65, 30, 30);

		// desenha a boca
		g.fillOval(50, 110, 120, 60);

		// "retoca" a boca para criar um sorriso
		g.setColor(Color.YELLOW);
		g.fillRect(50, 110, 120, 30);
		g.fillOval(50, 120, 120, 40);
	}
}