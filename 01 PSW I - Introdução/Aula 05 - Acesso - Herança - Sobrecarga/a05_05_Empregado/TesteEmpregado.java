package a05_05_Empregado;

/**
 * Demonstra��o de composi��o.
 * 
 * @author Matrakas
 */
public class TesteEmpregado
{
	public static void main(String args[])
	{
		Data nascimento = new Data(7, 24, 1949);
		Data contratacao = new Data(3, 12, 1988);
		Empregado empregado = new Empregado("Jo�o", "Maria", 
				nascimento, contratacao);

		System.out.println(empregado);
	
		// teste 1
		// contratacao.setAno(2000);
		// System.out.println(empregado);
		
		// teste 2
		// contratacao = empregado.getDataContratacao();
		// contratacao.setAno(2000);
		// System.out.println(empregado);
		
	}
}

/*******************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 ******************************************************************/
