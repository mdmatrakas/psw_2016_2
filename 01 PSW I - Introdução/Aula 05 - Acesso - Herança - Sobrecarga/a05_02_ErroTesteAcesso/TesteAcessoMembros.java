package a05_02_ErroTesteAcesso;

import a05_01_1_Hora.Hora;

/**
 * Classse para demonstrar que membros privados da classe Hora1 n�o s�o 
 * acess�veis.
 * 
 * @author Matrakas
 * 
 */
public class TesteAcessoMembros
{
	public static void main(String args[])
	{
		@SuppressWarnings("unused")
		Hora hora = new Hora(); // cria e inicializa um objeto Hora1
/*
		hora.hora = 7; // erro: hora tem acesso privado em Hora1
		hora.minuto = 15; // erro: minuto tem acesso privado em Hora1
		hora.segundo = 30; // erro: segundo tem acesso privado em in Hora1
*/	
	}
}

/*******************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 ******************************************************************/
