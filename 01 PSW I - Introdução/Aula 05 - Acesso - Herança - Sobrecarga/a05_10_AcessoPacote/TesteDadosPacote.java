package a05_10_AcessoPacote;

/**
 * Membros de acesso de pacote de uma classe permanecem acess�veis a outras classes no mesmo pacote.
 * 
 * @author Matrakas
 */
public class TesteDadosPacote
{
	public static void main(String args[])
	{
		DadosPacote dadoPacote = new DadosPacote();

		// gera sa�da da representa��o String de dadoPacote
		System.out.printf("Depois de instanciar:\n%s\n", dadoPacote);

		// muda os dados de acesso de pacote no objeto DadosPacote
		dadoPacote.numero = 77;
		dadoPacote.string = "Tchau";

		// gera sa�da da representa��o String de dadoPacote
		System.out.printf("\nDepois demudar os valores:\n%s\n", dadoPacote);
	}
}

/**
 * Classe com vari�veis de inst�ncia de acesso de pacote
 * 
 * @author Matrakas
 * 
 */
class DadosPacote
{
	int		numero; // vari�vel de inst�ncia de acesso de pacote
	String	string; // vari�vel de inst�ncia de acesso de pacote

	/**
	 * Inicializa��o padr�o do objeto
	 */
	public DadosPacote()
	{
		numero = 0;
		string = "Ol�";
	}

	/**
	 * Representa��o String do objeto DadosPacote
	 */
	public String toString()
	{
		return String.format("N�mero: %d; String: %s", numero, string);
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
