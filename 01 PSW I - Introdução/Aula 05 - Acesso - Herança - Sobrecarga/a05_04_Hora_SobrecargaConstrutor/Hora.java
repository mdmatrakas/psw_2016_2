package a05_04_Hora_SobrecargaConstrutor;

/**
 * Declara��o da classe Hora com construtores sobrecarregados.
 * 
 * @author Matrakas
 * 
 */
public class Hora
{
	private int	hora;		// 0 - 23
	private int	minuto;	// 0 - 59
	private int	segundo;	// 0 - 59

	/**
	 * construtor sem argumento Hora : inicializa cada vari�vel de inst�ncia com 
	 * zero; assegura que objetos Hora iniciam em um estado consistente
	 * 
	 */
	public Hora()
	{
		this(0, 0, 0); // invoca o construtor Hora2 com tr�s argumentos
	}

	/**
	 * Construtor Hora: hora fornecida, minuto e segundo padronizados para 0.
	 * 
	 * @param h
	 *            Indica o valor da hora. Deve estar entre 0 e 24.
	 */
	public Hora(int h)
	{
		this(h, 0, 0); // invoca o construtor Hora2 com tr�s argumentos
	}

	/**
	 * Construtor Hora: hora e minuto fornecidos, segundo padronizado para 0.
	 * 
	 * @param h
	 *            Indica o valor da hora. Deve estar entre 0 e 23.
	 * @param m
	 *            Indica o valor de minuto. Deve estar entre 0 e 59.
	 */
	public Hora(int h, int m)
	{
		this(h, m, 0); // invoca o construtor Hora com tr�s argumentos
	}

	/**
	 * Construtor Hora: hora, minuto e segundo fornecidos.
	 * 
	 * @param h
	 *            Indica o valor da hora. Deve estar entre 0 e 23.
	 * @param m
	 *            Indica o valor de minuto. Deve estar entre 0 e 59.
	 * @param s
	 *            Indica o valor de segundo. Deve estar entre 0 e 59.
	 */
	public Hora(int h, int m, int s)
	{
		setHora(h, m, s); // invoca setHora para validar a data/hora
	}

	/**
	 * Construtor Hora: outro objeto Hora fornecido.
	 * 
	 * @param h
	 *            Objeto tipo Hora2 que ser� copiado.
	 */
	public Hora(Hora h)
	{
		// invoca o construtor de tr�s argumentos Hora
		this(h.getHora(), h.getMinuto(), h.getSegundo());
	}

	// M�todos set

	/**
	 * Configura um novo valor de hora usando UTC; assegura que os dados 
	 * permane�am consistentes configurando valores inv�lidos como zero
	 * @param h
	 *            Indica o valor da hora. Deve estar entre 0 e 23.
	 * @param m
	 *            Indica o valor de minuto. Deve estar entre 0 e 59.
	 * @param s
	 *            Indica o valor de segundo. Deve estar entre 0 e 59.
	 */
	public void setHora(int h, int m, int s)
	{
		setHora(h); // configura hora
		setMinuto(m); // configura minuto
		setSegundo(s); // configura segundo
	}

	/**
	 *  Valida e configura a hora.
	 * @param h Novo valor de hora.
	 */
	public void setHora(int h)
	{
		hora = ((h >= 0 && h < 24) ? h : 0);
	}

	/**
	 *  Valida e configura os minutos.
	 * @param m Novo valor de minuto.
	 */
	public void setMinuto(int m)
	{
		minuto = ((m >= 0 && m < 60) ? m : 0);
	}

	/**
	 *  Valida e configura os segundos.
	 * @param s Novo valor de segundo.
	 */
	public void setSegundo(int s)
	{
		segundo = ((s >= 0 && s < 60) ? s : 0);
	}

	// M�todos get
	
	/**
	 *  Obt�m valor da hora
	 * @return O valor da hora.
	 */
	public int getHora()
	{
		return hora;
	}

	/**
	 *  Obt�m valor dos minutos.
	 * @return O valor de minuto.
	 */
	public int getMinuto()
	{
		return minuto;
	}

	/**
	 *  Obt�m valor dos segundos.
	 * @return O valor de segundo.
	 */
	public int getSegundo()
	{
		return segundo;
	}

	/**
	 *  Converte em String no formato de hora universal (HH:MM:SS)
	 * @return Uma String com a representa��o da hora em formato universal.
	 */
	public String toStringUniversal()
	{
		return String.format("%02d:%02d:%02d", getHora(), getMinuto(),
				getSegundo());
	}

	/**
	 *  Converte em String no formato padr�o de hora (H:MM:SS AM ou PM)
	 * @return Uma String com a representa��o da hora em formato padr�o.
	 */
	public String toString()
	{
		return String.format("%d:%02d:%02d %s", 
				((getHora() == 0 || getHora() == 12) ? 12 : getHora() % 12)
				, getMinuto(), getSegundo(), (getHora() < 12 ? "AM" : "PM"));
	}
}

/*******************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 ******************************************************************/
