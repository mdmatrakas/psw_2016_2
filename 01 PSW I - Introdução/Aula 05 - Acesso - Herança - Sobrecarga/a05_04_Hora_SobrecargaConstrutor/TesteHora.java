package a05_04_Hora_SobrecargaConstrutor;

/**
 * Construtores sobrecarregados utilizados para inicializar objetos Hora.
 * 
 * @author Matrakas
 * 
 */
public class TesteHora
{
	public static void main(String args[])
	{
		Hora t1 = new Hora(); // 00:00:00
		Hora t2 = new Hora(2); // 02:00:00
		Hora t3 = new Hora(21, 34); // 21:34:00
		Hora t4 = new Hora(12, 25, 42); // 12:25:42
		Hora t5 = new Hora(27, 74, 99); // 00:00:00
		Hora t6 = new Hora(t4); // 12:25:42

		System.out.println("Constru�do com:");
		System.out.println("t1: todos os par�metros por padr�o");
		System.out.printf("   %s\n", t1.toStringUniversal());
		System.out.printf("   %s\n", t1.toString());

		System.out.println("t2: hora especificada; minuto e segundo por padr�o");
		System.out.printf("   %s\n", t2.toStringUniversal());
		System.out.printf("   %s\n", t2.toString());

		System.out.println("t3: hora e minuto especificados; segundo padr�o");
		System.out.printf("   %s\n", t3.toStringUniversal());
		System.out.printf("   %s\n", t3.toString());

		System.out.println("t4: hora, minuto e segundo especificados");
		System.out.printf("   %s\n", t4.toStringUniversal());
		System.out.printf("   %s\n", t4.toString());

		System.out.println("t5: todos os valores especificados com valores inv�lidos");
		System.out.printf("   %s\n", t5.toStringUniversal());
		System.out.printf("   %s\n", t5.toString());

		System.out.println("t6: Hora especificado com objeto t4");
		System.out.printf("   %s\n", t6.toStringUniversal());
		System.out.printf("   %s\n", t6.toString());
	}
}

/*******************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 ******************************************************************/
