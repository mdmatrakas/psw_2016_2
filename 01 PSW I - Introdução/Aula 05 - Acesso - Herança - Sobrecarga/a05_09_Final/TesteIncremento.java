package a05_09_Final;

/**
 * Vari�vel final inicializada com um argumento de construtor.
 * 
 * @author Matrakas
 */
public class TesteIncremento
{
	public static void main(String args[])
	{
		Incremento valor = new Incremento(5);

		System.out.printf("Antes de incrementar: %s\n\n", valor);

		for (int i = 1; i <= 3; i++)
		{
			valor.SomaIncrementoEmTotal();
			System.out.printf("Depois de incrementar %d: %s\n", i, valor);
		}
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/