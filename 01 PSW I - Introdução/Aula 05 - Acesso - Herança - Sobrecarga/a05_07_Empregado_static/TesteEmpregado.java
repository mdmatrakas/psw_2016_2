package a05_07_Empregado_static;

/**
 * Demonstra��o do membro static.
 * 
 * @author Matrakas
 */
public class TesteEmpregado
{
	@SuppressWarnings("static-access")
	public static void main(String args[])
	{
		// Mostra que a contagem � 0 antes de criar Emplregado
		System.out.printf("Empregados antes da instancia��o: %d\n", 
				Empregado.getCont());

		// Cria dois Emplregados; a contagem deve ser 2
		Empregado e1 = new Empregado("Susan", "Baker");
		Empregado e2 = new Empregado("Bob", "Blue");

		// Mostra que a contagem � 2 depois de criar dois Emplregados
		System.out.println("\nEmpregados depois da instancia��o: ");
		System.out.printf("via e1.getCount(): %d\n", e1.getCont());
		System.out.printf("via e2.getCount(): %d\n", e2.getCont());
		System.out.printf("via Empregado.getCont(): %d\n", Empregado.getCont());

		// Obt�m nomes de Emplregado
		System.out.printf("\nEmpregado 1: %s %s\nEmpregado 2: %s %s\n\n", 
				e1.getPrimeiroNome(), e1.getUltimoNome(), e2.getPrimeiroNome(), 
				e2.getUltimoNome());

		// Nesse exemplo, h� somente uma refer�ncia a cada Employee,
		// assim as duas instru��es a seguir fazem com que a JVM marque cada
		// objeto Emplregado para coleta de lixo
		e1 = null;
		e2 = null;

		System.gc(); // pede que a coleta de lixo ocorra agora

		// Mostra a contagem de Empregado depois de chamar o coletor de lixo; 
		// contagem exibida pode ser 0, 1 ou 2 com base na execu��o do coletor 
		// de lixo imediata e n�mero de objetos Empregados coletados
		System.out.printf("\nEmpregados depois de chamar System.gc(): %d\n",
				Empregado.getCont());
	}
}

/*******************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 ******************************************************************/
