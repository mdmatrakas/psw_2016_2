package a05_11_SobrecargaMetodo;

/**
 * Aplicativo para testar a classe MethodOverload.
 * 
 * @author Matrakas
 */
public class TesteSobrecargaDeMetodos
{
	public static void main(String args[])
	{
		SobrecargaDeMetodos methodOverload = new SobrecargaDeMetodos();
		methodOverload.testeMetodosSobrecarregados();
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
