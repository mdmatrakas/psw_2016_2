package a05_01_2_Hora;

import a05_01_1_Hora.Hora;

/**
 *  Demonstra��o do uso de um objeto do tipo Hora, definido em outro pacote, 
 *  utilizado em um aplicativo.
 * @author Matrakas
 *
 */
public class HoraTestePacote 
{
   public static void main( String args[] )
   {
	      // cria e inicializa um objeto Hora
	      Hora hora = new Hora(); // invoca o construtor Hora

	      // gera sa�da de representa��es de string da hora
	      System.out.print( "Hora universal inicial �: " );
	      System.out.println(hora.toStringUniversal());
	      System.out.print( "Hora padr�o inicial �: " );
	      System.out.println(hora.toString());
	      System.out.println(); // gera sa�da de uma linha em branco

	      // altera a hora e gera sa�da da data/hora atualizada
	      hora.setHora( 13, 27, 6 );
	      System.out.print( "Hora univesal depois de setHora �: " );
	      System.out.println(hora.toStringUniversal());
	      System.out.print( "Hora padr�o depois de setHora �: " );
	      System.out.println(hora.toString());
	      System.out.println(); // gera sa�da de uma linha em branco

	      // configura hora com valores inv�lidos; gera sa�da da data/hora atualizada
	      hora.setHora( 99, 99, 99 );
	      System.out.println( "Depois de tentar valores inv�lidos:" );
	      System.out.print( "Hora universal: " );
	      System.out.println(hora.toStringUniversal());
	      System.out.print( "Hora padr�o: " );
	      System.out.println(hora.toString());
	   }
	} 

/*******************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 ******************************************************************/