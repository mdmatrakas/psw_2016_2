package a05_06_Enumeracao;

/**
 * Declarando um tipo enum com um construtor e campos de inst�ncia expl�citos e m�todos de acesso
 * para esses campos
 * 
 * @author Matrakas
 */
public enum Livro
{
	// declara constantes do tipo enum
	JHTP6("Java Como programar 6e", "2005"),
	CHTP4("C Como Programar 4e", "2004"), 
	IW3HTP3("Internet & World Wide Web Como Programar 3e", "2004"), 
	CPPHTP4("C++ Como Programar 4e", "2003"), 
	VBHTP2("Visual Basic .NET Como Programar 2e", "2002"), 
	CSHARPHTP("C# Como Programar", "2002");

	// campos de inst�ncia
	/**
	 * T�tulo do livro
	 */
	private final String	titulo;
	/**
	 * Ano dos direitos autorais
	 */
	private final String	anoDireitos;

	/**
	 * Construtor enum
	 * 
	 * @param tituloLivro
	 *            String com o t�tulo do livro
	 * @param ano
	 *            Inteiro com o ano dos direitos autorais
	 */
	Livro(String tituloLivro, String ano)
	{
		titulo = tituloLivro;
		anoDireitos = ano;
	}

	/**
	 * M�todo de acesso para t�tulo de campo
	 * 
	 * @return O titulo do livro
	 */
	public String getTitulo()
	{
		return titulo;
	}

	/**
	 * M�todo de acesso para o campo anoDireitos
	 * 
	 * @return O ano dos direitos autorais
	 */
	public String getAnoDireitos()
	{
		return anoDireitos;
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
