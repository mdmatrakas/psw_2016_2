package a07_01_DialogoMensagem;

import javax.swing.JOptionPane; // importa classe JOptionPane

import a06_02_EmpregadoHierarquia.EmpregadoAssalariado;

/**
 * Imprimindo m�ltiplas linhas em uma caixa de di�logo, a partir de um objeto, utilizando uma
 * chamada implicita a toString().
 * 
 * @author Matrakas
 */
public class Dialogo03_MensagemDeObjeto
{
	/**
	 * M�todo de entrada da aplica��o de exemplo de uso de caixas de dialogo.
	 * 
	 * @param args
	 *            N�o utilizado.
	 */
	public static void main(String[] args)
	{
		EmpregadoAssalariado empregado = new EmpregadoAssalariado
			("Jos�", "Silva", "111.111.111-11", 1220.3);

		// exibe um di�logo com a string referenta ao objeto
		JOptionPane.showMessageDialog(null, empregado); // chamada implicita a toString()
	}
}
