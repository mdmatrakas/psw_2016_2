package a03_12_ModificadorZero__traduzir;
// Fig. 28.19: ZeroFlagTest.java
// Imprimindo com flags 0 (zero) preenche com zeros � esquerda.

public class ZeroFlagTest 
{
   public static void main( String args[] )
   { 
      System.out.printf("%+09d\n", 452 );
      System.out.printf("%09d\n", 452 ); 
      System.out.printf("% 9d\n", 452 );
   } // fim de main
} // fim da classe ZeroFlagTest


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
