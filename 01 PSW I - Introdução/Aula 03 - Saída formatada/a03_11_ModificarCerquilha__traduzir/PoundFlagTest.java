package a03_11_ModificarCerquilha__traduzir;
// Fig. 28.18: PoundFlagTest.java
// Utilizando o flag # com os caracteres de convers�o o e x.

public class PoundFlagTest 
{
   public static void main( String args[] )
   { 
      int c = 31;      // inicializa c
   
      System.out.printf( "%o\n", c );
      System.out.printf( "%x\n", c );
      System.out.printf( "%#o\n", c );
      System.out.printf( "%#x\n", c );
   } // fim de main
} // fim da classe PoundFlagTest


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/