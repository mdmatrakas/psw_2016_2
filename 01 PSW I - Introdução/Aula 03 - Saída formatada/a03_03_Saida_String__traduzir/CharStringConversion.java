package a03_03_Saida_String__traduzir;
// Fig. 28.5: CharStringConversion.java
// Utilizando caractere e caracteres de convers�o de string.

public class CharStringConversion 
{
   public static void main( String args[] )
   { 
      char character = 'A';  // inicializa char
      String string = "This is also a string";  // Objeto String
      Integer integer = 1234;  // inicializa inteiro (autoboxing)

      System.out.printf("%c\n", character);
      System.out.printf("%s\n", "This is a string");
      System.out.printf("%s\n", string);
      System.out.printf("%S\n", string);
      System.out.printf("%s\n", string);
      System.out.printf("%s\n", integer); // chamada impl�cita a toString
   } // fim de main
} // fim da classe CharStringConversion


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/