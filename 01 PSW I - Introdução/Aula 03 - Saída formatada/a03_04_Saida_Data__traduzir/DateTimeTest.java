package a03_04_Saida_Data__traduzir;
// Fig. 28.9: DateTimeTest.java
// Formatando datas e horas com o caractere de convers�o t e T.
import java.util.Calendar;

public class DateTimeTest
{
   public static void main( String args[] ) 
   {
      // obt�m a data e hora atual
      Calendar dateTime = Calendar.getInstance();

      // imprimindo com caracteres de convers�o para composi��es de data/hora
      System.out.printf("%tc\n", dateTime);
      System.out.printf("%tF\n", dateTime);
      System.out.printf("%tD\n", dateTime);
      System.out.printf("%tr\n", dateTime);
      System.out.printf("%tT\n", dateTime);

      // imprimindo com caracteres de convers�o para data
      System.out.printf("%1$tA, %1$tB %1$td, %1$tY\n", dateTime);
      System.out.printf("%1$TA, %1$TB %1$Td, %1$TY\n", dateTime);
      System.out.printf("%1$ta, %1$tb %1$te, %1$ty\n", dateTime);

      // imprimindo com caracteres de convers�o para hora
      System.out.printf("%1$tH:%1$tM:%1$tS\n", dateTime);
     // System.out.printf("%1$tZ %1$tI:%1$tM:%1$tS %tP", dateTime);
   } // fim de main
} // fim da classe DateTimeTest 


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/