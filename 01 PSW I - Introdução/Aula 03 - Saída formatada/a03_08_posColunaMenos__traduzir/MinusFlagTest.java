package a03_08_posColunaMenos__traduzir;
// Fig. 28.15: MinusFlagTest.java
// Valores para o alinhamento � direita e � esquerda.

public class MinusFlagTest 
{
public static void main( String args[] )
   { 
      System.out.println( "Columns:" );
      System.out.println( "0123456789012345678901234567890123456789\n" );
      System.out.printf ( "%10s%10d%10c%10f\n\n", "hello", 7, 'a', 1.23 );
      System.out.printf(                                   
         "%-10s%-10d%-10c%-10f\n", "hello", 7, 'a', 1.23 );
   } // fim de main
} // fim da classe MinusFlagTest

