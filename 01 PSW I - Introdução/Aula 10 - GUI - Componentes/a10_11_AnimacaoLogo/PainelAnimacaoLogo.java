package a10_11_AnimacaoLogo;

// Anima��o de uma s�rie de imagens.
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Graphics;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

@SuppressWarnings("serial")
public class PainelAnimacaoLogo extends JPanel 
{
   private final static String NOME_IMAGEM = "deitel"; // nome b�sico de imagem
   protected ImageIcon imagens[]; // array de imagens
   private final int TOTAL_IMAGES = 30; // n�mero de imagens
   private int imagemAtual = 0; // �ndice de imagem atual
   private final int TEMPO_ANIMACAO = 50; // retardo em milissegundos 
   private int largura; // largura da imagem
   private int altura; // altura da imagem

   private Timer temporizador; // O temporizador guia a anima��o

   // construtor inicializa PainelAnimacaoLogo carregando imagens
   public PainelAnimacaoLogo()
   {
      imagens = new ImageIcon[ TOTAL_IMAGES ];

      // carrega 30 imagens
      for ( int count = 0; count < imagens.length; count++ )
      {
    	 String str = "images/" + NOME_IMAGEM + count + ".gif";   
         imagens[ count ] = new ImageIcon( getClass().getResource(str) );
      }
 
      // esse exemplo assume que todas as imagens t�m a mesma largura e altura
      largura = imagens[ 0 ].getIconWidth();   // obt�m a largura de �cone
      altura = imagens[ 0 ].getIconHeight(); // obt�m a altura de �cone
   } 

   // exibe a imagem atual
   public void paintComponent( Graphics g )
   {
      super.paintComponent( g ); // chama a superclasse paintComponent

      imagens[ imagemAtual ].paintIcon( this, g, 0, 0 );
      
      // configura a pr�xima imagem a ser desenhada apenas se o timer estiver executando
      if (temporizador.isRunning())  
         imagemAtual = ( imagemAtual + 1 ) % TOTAL_IMAGES;
   }

   // inicia a anima��o ou reinicia se a janela for reexibida
   public void iniciaAnimacao()
   {
      if ( temporizador == null ) 
      {
         imagemAtual = 0; // exibe a primeira imagem

         // cria o temporizador                                     
         temporizador = new Timer( TEMPO_ANIMACAO, new OuvinteTempo() );
         temporizador.start(); // inicia o timer
      } 
      else // temporizador j� existe, reinicia anima��o
      {
         if ( ! temporizador.isRunning())
            temporizador.restart();
      } 
   }

   // p�ra o timer de anima��o 
   public void paraAnimacao()
   {
      temporizador.stop();
   }

   // retorna o tamanho m�nimo de anima��o
   public Dimension getMinimumSize()  
   {                                  
      return getPreferredSize();      
   }      

   // retorna tamanho preferido da anima��o    
   public Dimension getPreferredSize()      
   {                                        
      return new Dimension( largura, altura );
   }       

   // classe interna para tratar eventos de a��o do Temporizador
   private class OuvinteTempo implements ActionListener 
   {
      // responde ao evento do Temporizador
      public void actionPerformed( ActionEvent actionEvent )
      {
         repaint(); // pinta o animador novamente
      }
   }
} 

