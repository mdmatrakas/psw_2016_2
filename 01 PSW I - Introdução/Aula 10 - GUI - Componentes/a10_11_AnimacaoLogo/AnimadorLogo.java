package a10_11_AnimacaoLogo;

// Anima��o de uma s�rie de imagens.
import javax.swing.JFrame;

public class AnimadorLogo 
{
   // executa a anima��o em um JFrame
   public static void main( String args[] )
   {
      PainelAnimacaoLogo animation = new PainelAnimacaoLogo(); 

      JFrame window = new JFrame( "Teste de anima��o" ); // configura a janela
      window.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      window.add( animation ); // adiciona painel ao frame

      window.pack();  // aumenta a janela apenas o suficiente para sua GUI
      window.setVisible( true );   // exibe a janela 

      animation.iniciaAnimacao();  // inicia a anima��o
   } 
} 
