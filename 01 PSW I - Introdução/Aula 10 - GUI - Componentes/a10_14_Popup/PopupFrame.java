package a10_14_Popup;
// Fig. 22.7: PopupFrame.java
// Demonstrando JPopupMenus.
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.ButtonGroup;

public class PopupFrame extends JFrame 
{
   private JRadioButtonMenuItem items[]; // cont�m itens para cores
   private final Color colorValues[] = 
      { Color.BLUE, Color.YELLOW, Color.RED }; // cores a serem utilizadas
   private JPopupMenu popupMenu; // permite que o usu�rio selecione a cor

   // construtor sem argumento configure a GUI
   public PopupFrame()
   {
      super( "Using JPopupMenus" );

      ItemHandler handler = new ItemHandler(); // handler para itens de menu
      String colors[] = { "Blue", "Yellow", "Red" }; // array de cores

      ButtonGroup colorGroup = new ButtonGroup(); // gerencia itens de cor
      popupMenu = new JPopupMenu(); // cria menu pop-up
      items = new JRadioButtonMenuItem[ 3 ]; // itens para selecionar cor

      // cria item de menu, adiciona-o ao menu pop-up, permite tratamento de eventos
      for ( int count = 0; count < items.length; count++ ) 
      {
         items[ count ] = new JRadioButtonMenuItem( colors[ count ] );
         popupMenu.add( items[ count ] ); // adiciona o item ao menu pop-up
         colorGroup.add( items[ count ] ); // adiciona o item ao grupo de bot�es
         items[ count ].addActionListener( handler ); // adiciona handler
      } // fim do for

      setBackground( Color.WHITE ); // configura o fundo como branco

      // declara um MouseListener para a janela a fim de exibir o menu pop-up
      addMouseListener(

         new MouseAdapter() // classe interna an�nima
         {  
            // trata eventos de pressionamento do mouse
            public void mousePressed( MouseEvent event )
            { 
               checkForTriggerEvent( event ); // verifica o acionamento
            } // fim do m�todo mousePressed

            // trata eventos de libera��o de bot�o do mouse
            public void mouseReleased( MouseEvent event )
            { 
               checkForTriggerEvent( event ); // verifica o acionamento
            } // fim do m�todo mouseReleased

            // determina se o evento deve acionar o menu de pop-up
            private void checkForTriggerEvent( MouseEvent event )
            {
               if ( event.isPopupTrigger() )                            
                  popupMenu.show(                                       
                     event.getComponent(), event.getX(), event.getY() );
            } // fim do m�todo checkForTriggerEvent
         } // fim da classe interna an�nima
      ); // fim da chamada para addMouseListener
   } // fim do construtor PopupFrame

   // classe interna privada para tratar eventos de item de menu
   private class ItemHandler implements ActionListener 
   {
      // processa sele��es de itens de menu
      public void actionPerformed( ActionEvent event )
      {
         // determina qual item de menu foi selecionado
         for ( int i = 0; i < items.length; i++ )
         {
            if ( event.getSource() == items[ i ] ) 
            {
               getContentPane().setBackground( colorValues[ i ] );
               return;
            } // fim do if
         } // fim do for
      } // fim do m�todo actionPerformed
   } // fim da classe interna privada ItemHandler
} // fim da classe PopupFrame


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/