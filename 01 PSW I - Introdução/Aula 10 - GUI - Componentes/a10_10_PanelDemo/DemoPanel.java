package a10_10_PanelDemo;

import javax.swing.JFrame;

/**
 * Testando PanelFrame.
 * @author MDMatrakas
 *
 */
@SuppressWarnings("serial")
public class DemoPanel extends JFrame 
{
   public static void main( String args[] )
   { 
      FramePanel panel = new FramePanel(); 
      panel.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      panel.setSize( 450, 200 ); // configura o tamanho do frame
      panel.setVisible( true ); // exibe o frame
   } 
} 