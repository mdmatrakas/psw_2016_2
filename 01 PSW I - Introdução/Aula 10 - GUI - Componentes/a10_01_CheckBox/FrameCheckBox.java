package a10_01_CheckBox;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JCheckBox;

/**
 * Demonstrar o uso das classes JCheckBox e Font.
 * @author MDMatrakas
 *
 */
@SuppressWarnings("serial")
public class FrameCheckBox extends JFrame
{
	private JTextField	campoTexto;			// exibe o texto na altera��o de fontes
	private JCheckBox	checkBoxNegrito;	// para selecionar/desselecionar negrito
	private JCheckBox	checkBoxItalico;	// para selecionar/desselecionar it�lico

	public static void main(String args[])
	{
		FrameCheckBox f = new FrameCheckBox();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(300, 150); // configura o tamanho do frame
		f.setVisible(true); // exibe o frame
	} 
	// construtor FrameCheckBox adiciona JCheckBoxes ao JFrame
	public FrameCheckBox()
	{
		super("Testando Caixas de sele��o");
		setLayout(new FlowLayout()); // configura o layout de frame
		// configura JTextField e sua fonte
		campoTexto = new JTextField("Observe a fonte...", 20);
		campoTexto.setFont(new Font("Serif", Font.PLAIN, 14));
		add(campoTexto); 
		checkBoxNegrito = new JCheckBox("Negrito"); // cria caixa de sele��o p/ negrito
		checkBoxItalico = new JCheckBox("It�lico"); // cria it�lico
		add(checkBoxNegrito); // adiciona caixa de sele��o de negrito ao JFrame
		add(checkBoxItalico); // adiciona caixa de sele��o de it�lico ao JFrame
		// ouvintes para os JCheckBoxes
		OuvinteCheckBox ouvinte = new OuvinteCheckBox();
		checkBoxNegrito.addItemListener(ouvinte);
		checkBoxItalico.addItemListener(ouvinte);
	} 

	// classe interna private para tratamento de evento ItemListener
	private class OuvinteCheckBox implements ItemListener
	{
		private int	valNegrito	= Font.PLAIN;	// controla o estilo de fonte negrito
		private int	valItalic	= Font.PLAIN;	// controla o estilo de fonte it�lico

		// responde aos eventos de caixa de sele��o
		public void itemStateChanged(ItemEvent event)
		{
			// processa eventos da caixa de sele��o de negrito
			if (event.getSource() == checkBoxNegrito)
				valNegrito = checkBoxNegrito.isSelected() ? Font.BOLD : Font.PLAIN;

			// processa eventos da caixa de sele��o de it�lico
			if (event.getSource() == checkBoxItalico)
				valItalic = checkBoxItalico.isSelected() ? Font.ITALIC : Font.PLAIN;
			/*
			 * if(checkBoxItalico.isSelected()) 
			 * 		valItalic = Font.ITALIC; 
			 * else
			 * 		valItalic = Font.PLAIN;
			 */

			// configura a fonte do campo de texto
			campoTexto.setFont(new Font("Serif", valNegrito + valItalic, 14));
		} 
	} 
} 
