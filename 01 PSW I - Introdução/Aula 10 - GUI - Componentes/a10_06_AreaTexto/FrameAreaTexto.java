package a10_06_AreaTexto;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.Box;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JScrollPane;

/**
 * Copiando texto selecionado de uma �rea de texto para outra.
 * 
 * @author MDMatrakas
 * 
 */
@SuppressWarnings("serial")
public class FrameAreaTexto extends JFrame
{
	private JTextArea	areaTexto1; // exibe a string demo
	private JTextArea	areaTexto2; // texto destacado � copiado aqui
	private JButton		botaoCopia; // come�a a copiar o texto

	public static void main(String args[])
	{
		FrameAreaTexto f = new FrameAreaTexto();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(425, 200); // configura o tamanho do frame
		f.setVisible(true); // exibe o frame
	}

	// construtor sem argumento
	public FrameAreaTexto()
	{
		super("Demo de Area de Texto");
		Box box = Box.createHorizontalBox(); // cria box
		String demo = "Esta � uma string de demonstra��o\n"
				+ "para ilustrar a c�pia \nde uma �rea de texto \n"
				+ "para outra usando um evento externo\n";

		areaTexto1 = new JTextArea(demo, 10, 15); // cria textarea1
		box.add(new JScrollPane(areaTexto1)); // adiciona scrollpane

		botaoCopia = new JButton("Copia >>>"); // cria bot�o de c�pia
		box.add(botaoCopia); // adiciona o bot�o de c�pia � box
		botaoCopia.addActionListener(

		new ActionListener() // classe interna an�nima
				{
					// configura texto em areaTexto2 como texto selecionado de areaTexto1
					public void actionPerformed(ActionEvent event)
					{
						areaTexto2.setText(areaTexto1.getSelectedText());
					} 
				}); 

		areaTexto2 = new JTextArea(10, 15); // cria segundo textarea
		areaTexto2.setEditable(false); // desativa a edi��o
		box.add(new JScrollPane(areaTexto2)); // adiciona scrollpane

		add(box); 
	} 
} 