package a10_15_LookAndFeel;
// Fig. 22.9: LookAndFeelFrame.java
// Alterando a apar�ncia e comportamento.
import java.awt.GridLayout;
import java.awt.BorderLayout;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;

public class LookAndFeelFrame extends JFrame 
{
   // nomes de string das apar�ncias e comportamentos
   private final String strings[] = { "Metal", "Motif", "Windows" };
   private UIManager.LookAndFeelInfo looks[]; // apar�ncia e comportamentos
   private JRadioButton radio[]; // bot�es de op��o para selecionar a apar�ncia e comportamento
   private ButtonGroup group; // grupo para bot�es de op��o 
   private JButton button; // exibe a apar�ncia do bot�o
   private JLabel label; // exibe a apar�ncia do r�tulo
   private JComboBox comboBox; // exibe a apar�ncia da caixa de combina��o

   // configura a GUI
   public LookAndFeelFrame()
   {
      super( "Look and Feel Demo" );

      JPanel northPanel = new JPanel(); // cria o painel North
      northPanel.setLayout( new GridLayout( 3, 1, 0, 5 ) );

      label = new JLabel( "This is a Metal look-and-feel",
         SwingConstants.CENTER ); // cria o r�tulo
      northPanel.add( label ); // adiciona o r�tulo ao painel

      button = new JButton( "JButton" ); // cria o bot�o
      northPanel.add( button ); // adiciona bot�o ao painel

      comboBox = new JComboBox( strings ); // cria a caixa de combina��o
      northPanel.add( comboBox ); // adiciona a caixa de combina��o ao painel
     
      // cria um array para bot�es de op��o
      radio = new JRadioButton[ strings.length ];

      JPanel southPanel = new JPanel(); // cria o painel South
      southPanel.setLayout( new GridLayout( 1, radio.length ) );

      group = new ButtonGroup(); // grupo de bot�es para a apar�ncia e comportamento
      ItemHandler handler = new ItemHandler(); // handler da apar�ncia e comportamento

      for ( int count = 0; count < radio.length; count++ ) 
      {
         radio[ count ] = new JRadioButton( strings[ count ] );
         radio[ count ].addItemListener( handler ); // adiciona handler
         group.add( radio[ count ] ); // adiciona bot�es de op��o ao grupo
         southPanel.add( radio[ count ] ); // adiciona bot�es de op��o ao painel
      } // fim do for

      add( northPanel, BorderLayout.NORTH ); // adiciona o painel North
      add( southPanel, BorderLayout.SOUTH ); // adiciona o painel South

      // obt�m as informa��es sobre a apar�ncia e comportamento instaladas
      looks = UIManager.getInstalledLookAndFeels();
      radio[ 0 ].setSelected( true ); // configura a sele��o padr�o
   } // fim do construtor LookAndFeelFrame

   // utiliza UIManager para alterar a apar�ncia e comportamento da GUI
   private void changeTheLookAndFeel( int value )
   {
      try // muda a apar�ncia e comportamento
      {
         // configura a apar�ncia e comportamento para esse aplicativo                 
         UIManager.setLookAndFeel( looks[ value ].getClassName() );

         // atualiza os componentes nesse aplicativo     
         SwingUtilities.updateComponentTreeUI( this );
      } // fim do try
      catch ( Exception exception ) 
      {
         exception.printStackTrace();
      } // fim do catch
   } // fim do m�todo changeTheLookAndFeel 

   // classe interna private para tratar eventos de bot�o de op��o
   private class ItemHandler implements ItemListener 
   {
      // processa a sele��o de apar�ncia e comportamento feita pelo usu�rio
      public void itemStateChanged( ItemEvent event )
      {
         for ( int count = 0; count < radio.length; count++ )
         {
            if ( radio[ count ].isSelected() ) 
            {
               label.setText( String.format( "This is a %s look-and-feel", 
                  strings[ count ] ) );
               comboBox.setSelectedIndex( count ); // configura o �ndice da caixa de combina��o
               changeTheLookAndFeel( count ); // muda a apar�ncia e comportamento
            } // fim do if
         } // fim do for
      } // fim do m�todo itemStateChanged
   } // fim da classe interna privada ItemHandler
} // fim da classe LookAndFeelFrame


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
