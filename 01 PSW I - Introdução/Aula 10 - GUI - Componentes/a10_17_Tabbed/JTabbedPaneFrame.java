package a10_17_Tabbed;
// Fig. 22.13: JTabbedPaneFrame.java
// Demonstrando o JTabbedPane.
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class JTabbedPaneFrame extends JFrame  
{
   // configura a GUI
   public JTabbedPaneFrame()
   {
      super( "JTabbedPane Demo " );

      JTabbedPane tabbedPane = new JTabbedPane(); // cria o JTabbedPane

      // configura o pane11 e o adiciona ao JTabbedPane
      JLabel label1 = new JLabel( "panel one", SwingConstants.CENTER );
      JPanel panel1 = new JPanel(); // cria o primeiro painel
      panel1.add( label1 ); // adiciona o r�tulo ao painel
      tabbedPane.addTab( "Tab One", null, panel1, "First Panel" );
      
      // configura o panel2 e o adiciona a JTabbedPane
      JLabel label2 = new JLabel( "panel two", SwingConstants.CENTER );
      JPanel panel2 = new JPanel(); // cria o segundo painel
      panel2.setBackground( Color.YELLOW ); // configura o fundo como amarelo
      panel2.add( label2 ); // adiciona o r�tulo ao painel
      tabbedPane.addTab( "Tab Two", null, panel2, "Second Panel" );

      // configura o panel3 e o adiciona a JTabbedPane
      JLabel label3 = new JLabel( "panel three" );
      JPanel panel3 = new JPanel(); // cria o terceiro painel
      panel3.setLayout( new BorderLayout() ); // utilize o borderlayout
      panel3.add( new JButton( "North" ), BorderLayout.NORTH );
      panel3.add( new JButton( "West" ), BorderLayout.WEST );
      panel3.add( new JButton( "East" ), BorderLayout.EAST );
      panel3.add( new JButton( "South" ), BorderLayout.SOUTH );
      panel3.add( label3, BorderLayout.CENTER );
      tabbedPane.addTab( "Tab Three", null, panel3, "Third Panel" );

      add( tabbedPane ); // adiciona o JTabbedPane ao quadro
   } // fim do construtor JTabbedPaneFrame
} // fim da classe JTabbedPaneFrame


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/