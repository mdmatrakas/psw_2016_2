package a10_04_Lista;
 
import java.awt.FlowLayout;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.ListSelectionModel;

/**
 * Selecionando as cores de uma JList. Demosntrando o uso das Classes JList e JScrollPane.
 * @author MDMatrakas
 *
 */
@SuppressWarnings("serial")
public class FrameLista extends JFrame
{
	private JList<String>			listaCores;		// lista para exibir cores
	private final String	nomesCores[]	= { "Preto", "Azul", "Ciano", "Cinza escuro", "Cinza",
			"Verde", "Cinza claro", "Magenta", "Laranja", "Rosa", "Vermelho", "Branco", "Amarelo" };
	private final Color		colors[]		= { Color.BLACK, Color.BLUE, Color.CYAN,
			Color.DARK_GRAY, Color.GRAY, Color.GREEN, Color.LIGHT_GRAY, Color.MAGENTA,
			Color.ORANGE, Color.PINK, Color.RED, Color.WHITE, Color.YELLOW };
	
	public static void main(String args[])
	{
		FrameLista f = new FrameLista();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(350, 150); // configura o tamanho do frame
		f.setVisible(true); // exibe o frame
	}
	
	// construtor FrameLista adiciona JScrollPane que cont�m JList ao JFrame
	public FrameLista()
	{
		super("Testando JList.");
		setLayout(new FlowLayout()); // configura o layout de frame
		
		listaCores = new JList<String>(nomesCores); // cria com colorNames
		listaCores.setVisibleRowCount(5); // exibe cinco linhas de uma vez
		
		// n�o permite m�ltiplas sele��es
		listaCores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		// adiciona um JScrollPane que cont�m JList ao frame
		add(new JScrollPane(listaCores));
		
		listaCores.addListSelectionListener(new ListSelectionListener() // classe interna an�nima
				{
					// trata eventos de sele��o de lista
					public void valueChanged(ListSelectionEvent event)
					{
						getContentPane().setBackground(colors[listaCores.getSelectedIndex()]);
					} 
				} 
				); 
	} 
} 