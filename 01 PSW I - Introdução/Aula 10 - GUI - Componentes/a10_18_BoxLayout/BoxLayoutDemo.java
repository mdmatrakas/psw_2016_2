package a10_18_BoxLayout;
// Fig. 22.17: BoxLayoutDemo.java
// Demonstrando BoxLayout.
import javax.swing.JFrame;

public class BoxLayoutDemo
{
   public static void main( String args[] )
   { 
      BoxLayoutFrame boxLayoutFrame = new BoxLayoutFrame();  
      boxLayoutFrame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      boxLayoutFrame.setSize( 400, 220 ); // configura o tamanho do frame
      boxLayoutFrame.setVisible( true ); // exibe o frame
   } // fim de main
} // fim da classe BoxLayoutDemo


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/