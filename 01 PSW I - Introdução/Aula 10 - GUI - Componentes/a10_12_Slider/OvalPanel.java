package a10_12_Slider;
// Fig. 22.2: OvalPanel.java
// Uma classe personalizada de JPanel.
import java.awt.Graphics;
import java.awt.Dimension;
import javax.swing.JPanel;

public class OvalPanel extends JPanel 
{
   private int diameter = 10; // di�metro padr�o de 10

   // desenha uma oval do di�metro especificado
   public void paintComponent( Graphics g )
   {
      super.paintComponent( g );

      g.fillOval( 10, 10, diameter, diameter ); // desenha um c�rculo 
   } // fim do m�todo paintComponent

   // valida e configura o di�metro e ent�o repinta
   public void setDiameter( int newDiameter )
   {
      // se di�metro inv�lido, assume o padr�o de 10
      diameter = ( newDiameter >= 0 ? newDiameter : 10 );
      repaint(); // repinta o painel
   } // fim do m�todo setDiameter

   // utilizado pelo gerenciador de layout para determinar o tamanho preferido
   public Dimension getPreferredSize()
   {
      return new Dimension( 200, 200 );
   } // fim do m�todo getPreferredSize         

   // utilizado pelo gerenciador de layout para determinar o tamanho m�nimo
   public Dimension getMinimumSize()                  
   {                                                  
      return getPreferredSize();                      
   } // fim do m�todo getMinimumSize     
} // fim da classe OvalPanel


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/