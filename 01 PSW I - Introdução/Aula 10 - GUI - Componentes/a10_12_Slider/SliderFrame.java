package a10_12_Slider;
// Fig. 22.3: SliderFrame.java
// Utilizando JSliders para dimensionar uma oval.
import java.awt.BorderLayout;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JSlider;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;   

public class SliderFrame extends JFrame 
{
   private JSlider diameterJSlider; // controle deslizante para selecionar o di�metro
   private OvalPanel myPanel; // painel para desenhar um c�rculo

   // construtor sem argumento
   public SliderFrame() 
   {
      super( "Slider Demo" );

      myPanel = new OvalPanel(); // cria o painel para desenhar um c�rculo
      myPanel.setBackground( Color.YELLOW ); // configura o fundo como amarelo

      // configura o JSlider para controlar o valor do di�metro                       
      diameterJSlider =                                                 
         new JSlider( SwingConstants.HORIZONTAL, 0, 200, 10 );          
      diameterJSlider.setMajorTickSpacing( 10 ); // cria uma marca de medida a cada 10
      diameterJSlider.setPaintTicks( true ); // pinta as marcas de medida no controle deslizante   

      // registra o ouvinte de evento do JSlider                         
      diameterJSlider.addChangeListener(                         
                                                                 
         new ChangeListener() // classe interna an�nima
         {                                                       
            // trata da altera��o de valor do controle deslizante                     
            public void stateChanged( ChangeEvent e )            
            {                                                    
               myPanel.setDiameter( diameterJSlider.getValue() );
            } // fim do m�todo stateChanged                         
         } // fim da classe interna an�nima
      ); // fim da chamada a addChangeListener                        

      add( diameterJSlider, BorderLayout.SOUTH ); // adiciona um controle deslizante ao quadro
      add( myPanel, BorderLayout.CENTER ); // adiciona painel ao frame
   } // fim do construtor de SliderFrame
} // fim da classe SliderFrame


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/