package a10_05_ListaMultiSel;

import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

/**
 * Copiando itens de uma Lista para outra.
 * 
 * @author MDMatrakas
 * 
 */
@SuppressWarnings("serial")
public class ListaMultiSelecao extends JFrame
{
	@SuppressWarnings("rawtypes")
	private JList	listaCores;	// lista para armazenar nomes de cor
	@SuppressWarnings("rawtypes")
	private JList	listaCopia;	// lista para copiar nomes de cor em
	private JButton	botaoCopia;	// bot�o para copiar nomes selecionados
	private final String	NomesCores[]	= { "Preto", "Azul", "Ciano",
			"Cinza escuro", "Cinza", "Verde", "Cinza claro", "Magenta",
			"Laranja", "Rosa", "Vermelho", "Branco", "Amarelo" };

	public static void main(String args[])
	{
		ListaMultiSelecao f = new ListaMultiSelecao();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(350, 150); // configura o tamanho do frame
		f.setVisible(true); // exibe o frame
	}

	// construtor ListaMultiSelecao
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public ListaMultiSelecao()
	{
		super("Listas com sele��o multipla");
		setLayout(new FlowLayout()); // configura o layout de frame

		listaCores = new JList(NomesCores); // armazena nomes de todas as cores
		listaCores.setVisibleRowCount(5); // mostra cinco linhas
		listaCores.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
		add(new JScrollPane(listaCores)); // adiciona lista com scrollpane

		botaoCopia = new JButton("Copia >>>"); // cria bot�o de c�pia
		botaoCopia.addActionListener(

		new ActionListener() // classe interna an�nima
				{
					// trata evento de bot�o
					@SuppressWarnings("deprecation")
					public void actionPerformed(ActionEvent event)
					{
						// coloca valores selecionados na listaCopia
						listaCopia.setListData(listaCores.getSelectedValues());
					}
				});

		add(botaoCopia); // adiciona bot�o de c�pia ao JFrame

		listaCopia = new JList(); // cria lista para armazenar nomes de cor copiados
		listaCopia.setVisibleRowCount(5); // mostra 5 linhas
		listaCopia.setFixedCellWidth(100); // configura a largura
		listaCopia.setFixedCellHeight(15); // configura a altura
		listaCopia.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		add(new JScrollPane(listaCopia)); 
	} 
} 