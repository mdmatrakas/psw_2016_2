package a10_02_RadioButton;

import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.ButtonGroup;

/**
 * Criando bot�es de op��o utilizando ButtonGroup e JRadioButton.
 * @author MDMatrakas
 *
 */
@SuppressWarnings("serial")
public class FrameBotaoRadio extends JFrame
{
	private JTextField		campoTexto;				// utilizado para exibir altera��es de fonte
	private Font			fontSimples;			// fonte para texto simples
	private Font			fontNegrito;			// fonte para texto em negrito
	private Font			fontItalico;			// fonte para texto em it�lico
	private Font			fontNegritoItalico;		// fonte para texto em negrito e it�lico
	private JRadioButton	botaoSimples;			// seleciona texto simples
	private JRadioButton	botaoNegrito;			// seleciona texto em negrito
	private JRadioButton	botaoItalico;			// seleciona texto em it�lico
	private JRadioButton	botaoNegritoItalico; 	// negrito e it�lico
	private ButtonGroup		grupoBotoes;			// buttongroup para armazenar bot�es de op��o
													
	public static void main(String args[])
	{
		FrameBotaoRadio f = new FrameBotaoRadio();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(350, 150); // configura o tamanho do frame
		f.setVisible(true); // exibe o frame
	} 
	
	// construtor FrameBotaoRadio adiciona JRadioButton ao JFrame
	public FrameBotaoRadio()
	{
		super("Testando bot�es de R�dio.");
		setLayout(new FlowLayout()); // configura o layout de frame
		
		campoTexto = new JTextField("Observe a fonte ...", 25);
		add(campoTexto); // adiciona textField ao JFrame
		
		// cria bot�es de op��o
		botaoSimples = new JRadioButton("Simples", true);
		botaoNegrito = new JRadioButton("Negrito", false);
		botaoItalico = new JRadioButton("It�lico", false);
		botaoNegritoItalico = new JRadioButton("Negrito/It�lico", false);
		add(botaoSimples); // adiciona bot�o no estilo simples ao JFrame
		add(botaoNegrito); // adiciona bot�o de negrito ao JFrame
		add(botaoItalico); // adiciona bot�o de it�lico ao JFrame
		add(botaoNegritoItalico); // adiciona bot�o de negrito e it�lico
		
		// cria relacionamento l�gico entre JRadioButtons
		grupoBotoes = new ButtonGroup(); // cria ButtonGroup
		grupoBotoes.add(botaoSimples); // adiciona simples ao grupo
		grupoBotoes.add(botaoNegrito); // adiciona negrito ao grupo
		grupoBotoes.add(botaoItalico); // adiciona it�lico ao grupo
		grupoBotoes.add(botaoNegritoItalico); // adiciona negrito e it�lico
		
		// cria objetos do tipo fonte
		fontSimples = new Font("Serif", Font.PLAIN, 14);
		fontNegrito = new Font("Serif", Font.BOLD, 14);
		fontItalico = new Font("Serif", Font.ITALIC, 14);
		fontNegritoItalico = new Font("Serif", Font.BOLD + Font.ITALIC, 14);
		campoTexto.setFont(fontSimples); // configura a fonte inicial como simples
		
		// registra eventos para JRadioButtons
		botaoSimples.addItemListener(new OuvinteBotaoRadio(fontSimples));
		botaoNegrito.addItemListener(new OuvinteBotaoRadio(fontNegrito));
		botaoItalico.addItemListener(new OuvinteBotaoRadio(fontItalico));
		botaoNegritoItalico.addItemListener(new OuvinteBotaoRadio(fontNegritoItalico));
	} 
	
	// classe interna private para tratar eventos de bot�o de r�dio
	private class OuvinteBotaoRadio implements ItemListener
	{
		private Font	font;	// fonte associada com esse listener
								
		public OuvinteBotaoRadio(Font f)
		{
			font = f; // configura a fonte desse listener
		} 
		
		// trata eventos de bot�o de op��o
		public void itemStateChanged(ItemEvent event)
		{
			campoTexto.setFont(font); // configura fonte de textField
		} 
	} 
} 
