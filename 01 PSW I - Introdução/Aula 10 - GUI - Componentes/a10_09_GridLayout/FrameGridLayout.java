package a10_09_GridLayout;
 
import java.awt.GridLayout;
import java.awt.Container;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JButton;

/**
 * Demonstrando GridLayout.
 * @author MDMatrakas
 *
 */
@SuppressWarnings("serial")
public class FrameGridLayout extends JFrame implements ActionListener 
{
	/**
	 * Vetor de bot�es
	 */
   private JButton buttons[]; 
   /**
    * Vetor de nomes dos bot�es
    */
   private final String names[] = 
      { "um", "dois", "tr�s", "quatro", "cinco", "seis" };
   /**
    * alterna entre dois layouts
    */
   private boolean toggle = true; 
   /**
    * cont�iner do frame
    */
   private Container container; 
   /**
    * primeiro gridlayout
    */
   private GridLayout gridLayout1; 
   /**
    * segundo gridlayout
    */
   private GridLayout gridLayout2; 

   /**
    *  construtor sem argumento
    */
   public FrameGridLayout()
   {
      super( "Demonstra��o do GridLayout" );
      gridLayout1 = new GridLayout( 2, 3 ); // 2 por 3; nenhuma lacuna
      gridLayout2 = new GridLayout( 3, 2 ); // 3 por 2; nenhuma lacuna
      container = getContentPane();
      getContentPane().setLayout(gridLayout1);
      buttons = new JButton[ names.length ]; // cria vetor de JButtons

      for ( int cont = 0; cont < names.length; cont++ )
      {
         buttons[ cont ] = new JButton( names[ cont ] );
         buttons[ cont ].addActionListener( this ); // registra o listener
         getContentPane().add( buttons[ cont ] ); // adiciona o bot�o ao JFrame
      } 
   } 

   /**
    *  trata eventos de bot�o alternando entre layouts
    */
   public void actionPerformed( ActionEvent event )
   { 
      if ( toggle )
         container.setLayout( gridLayout2 ); // configura layout como segundo
      else
         container.setLayout( gridLayout1 ); // configura layout como primeiro

      toggle = !toggle; // alterna para valor oposto
      container.validate(); // refaz o layout do cont�iner
   } 
} 