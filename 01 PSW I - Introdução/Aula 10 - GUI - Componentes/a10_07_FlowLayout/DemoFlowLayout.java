package a10_07_FlowLayout;

import javax.swing.JFrame;

/**
 * Testando FlowLayoutFrame.
 * @author MDMatrakas
 *
 */
public class DemoFlowLayout
{
   public static void main( String args[] )
   { 
      FrameFlowLayout flow = new FrameFlowLayout(); 
      flow.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      flow.setSize( 300, 75 ); // configura o tamanho do frame
      flow.setVisible( true ); // exibe o frame
   } 
} 
