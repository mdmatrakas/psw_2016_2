package a10_07_FlowLayout;

import java.awt.FlowLayout;
import java.awt.Container;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JButton;

/**
 * Demonstrando os alinhamentos de FlowLayout.
 * @author MDMatrakas
 *
 */
@SuppressWarnings("serial")
public class FrameFlowLayout extends JFrame 
{
	/**
	 * bot�o para configurar alinhamento � esquerda
	 */
   private JButton botaoEsq; 
   /**
    * bot�o para configurar alinhamento centralizado
    */
   private JButton botaoCentro; 
   /**
    * bot�o para configurar alinhamento � direita
    */
   private JButton botaoDir; 
   /**
    * objeto de layout
    */
   private FlowLayout layout; 
   /**
    * cont�iner para configurar layout
    */
   private Container container; 
   
   /**
    *  configura GUI e registra listeners de bot�o
    */
   public FrameFlowLayout()
   {
      super( "Demonstra��o do FlowLayout" );

      layout = new FlowLayout(); // cria FlowLayout
      container = getContentPane(); // obt�m cont�iner para layout
      setLayout( layout ); // configura o layout de frame

      // configura botaoEsq e registra listener
      botaoEsq = new JButton( "Esquerda" ); // cria bot�o Esq
      add( botaoEsq ); // adiciona o bot�o Esq ao frame
      botaoEsq.addActionListener(

    		  /**
    		   * classe interna an�nima
    		   */
         new ActionListener() 
         {  
            // processa o evento botaoEsq
            public void actionPerformed( ActionEvent event )
            {
               layout.setAlignment( FlowLayout.LEFT );

               // realinha os componentes anexados
               layout.layoutContainer( container );
            } 
         } 
      ); 

      // configura botaoCentro e registra o listener
      botaoCentro = new JButton( "Centro" ); // cria bot�o Centro
      add( botaoCentro ); // adiciona bot�o Centro ao frame
      botaoCentro.addActionListener(

         new ActionListener() // classe interna an�nima
         { 
            // processa evento botaoCentro
            public void actionPerformed( ActionEvent event )
            {
               layout.setAlignment( FlowLayout.CENTER );

               // realinha os componentes anexados
               layout.layoutContainer( container );
            } 
         } 
      ); 
      
      // configura botaoDir e registra o listener
      botaoDir = new JButton( "Direita" ); // cria bot�o Direita
      add( botaoDir ); // adiciona bot�o Direita ao frame
      botaoDir.addActionListener(

         new ActionListener() // classe interna an�nima
         {  
            // processo evento botaoDir
            public void actionPerformed( ActionEvent event )
            {
               layout.setAlignment( FlowLayout.RIGHT );

               // realinha os componentes anexados
               layout.layoutContainer( container );
            } 
         } 
      ); 
   } 
} 