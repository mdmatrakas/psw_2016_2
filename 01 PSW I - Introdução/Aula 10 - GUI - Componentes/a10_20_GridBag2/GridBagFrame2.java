package a10_20_GridBag2;
// Fig. 22.23: GridBagFrame2.java
// Demonstrando as constantes GridBagLayout.
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Component;
import javax.swing.JFrame;
import javax.swing.JComboBox;
import javax.swing.JTextField;
import javax.swing.JList;
import javax.swing.JButton;

public class GridBagFrame2 extends JFrame 
{ 
   private GridBagLayout layout; // layout desse quadro                
   private GridBagConstraints constraints; // restri��es desse layout
    
   // configura a GUI
   public GridBagFrame2()
   {
      super( "GridBagLayout" );
      layout = new GridBagLayout();           
      setLayout( layout ); // configura o layout de frame
      constraints = new GridBagConstraints(); // instancia restri��es

      // cria componentes GUI
      String metals[] = { "Copper", "Aluminum", "Silver" };
      JComboBox comboBox = new JComboBox( metals );

      JTextField textField = new JTextField( "TextField" );

      String fonts[] = { "Serif", "Monospaced" };
      JList list = new JList( fonts );

      String names[] = { "zero", "one", "two", "three", "four" };
      JButton buttons[] = new JButton[ names.length ];

      for ( int count = 0; count < buttons.length; count++ )
         buttons[ count ] = new JButton( names[ count ] );

      // define restri��es dos componentes GUI para textField
      constraints.weightx = 1;                             
      constraints.weighty = 1;                             
      constraints.fill = GridBagConstraints.BOTH;          
      constraints.gridwidth = GridBagConstraints.REMAINDER;
      addComponent( textField );                           

      // buttons[0] -- weightx e weighty s�o 1: fill � BOTH
      constraints.gridwidth = 1;   
      addComponent( buttons[ 0 ] );

      // buttons[1] -- weightx e weighty s�o 1: fill � BOTH
      constraints.gridwidth = GridBagConstraints.RELATIVE;
      addComponent( buttons[ 1 ] );                       

      // buttons[2] -- weightx e weighty s�o 1: fill � BOTH
      constraints.gridwidth = GridBagConstraints.REMAINDER;
      addComponent( buttons[ 2 ] );                        

      // comboBox -- weightx � 1: fill � BOTH
      constraints.weighty = 0;                             
      constraints.gridwidth = GridBagConstraints.REMAINDER;
      addComponent( comboBox );                            
      
      // buttons[3] -- weightx � 1: fill � BOTH
      constraints.weighty = 1;                             
      constraints.gridwidth = GridBagConstraints.REMAINDER;
      addComponent( buttons[ 3 ] );                        

      // buttons[4] -- weightx e weighty s�o 1: fill � BOTH
      constraints.gridwidth = GridBagConstraints.RELATIVE;
      addComponent( buttons[ 4 ] );                       

      // list -- weightx e weighty s�o 1: fill � BOTH
      constraints.gridwidth = GridBagConstraints.REMAINDER;
      addComponent( list );                                
   } // fim do construtor GridBagFrame2

   // adiciona um componente ao cont�iner
   private void addComponent( Component component ) 
   {
      layout.setConstraints( component, constraints );
      add( component ); // adiciona componente
   } // fim do m�todo addComponent
} // fim da classe GridBagFrame2


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/
