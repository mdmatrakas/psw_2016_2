package a10_19_GridBag;
// Fig. 22.21: GridbagFrame.java
// Demonstrando GridBagLayout
import java.awt.GridBagLayout;     
import java.awt.GridBagConstraints;
import java.awt.Component;
import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;

public class GridBagFrame extends JFrame 
{ 
   private GridBagLayout layout; // layout desse quadro                
   private GridBagConstraints constraints; // restri��es desse layout
    
   // configura a GUI
   public GridBagFrame()
   {
      super( "GridBagLayout" );
      layout = new GridBagLayout();                                     
      setLayout( layout ); // configura o layout de frame
      constraints = new GridBagConstraints(); // instancia restri��es

      // cria componentes GUI
      JTextArea textArea1 = new JTextArea( "TextArea1", 5, 10 );
      JTextArea textArea2 = new JTextArea( "TextArea2", 2, 2 );

      String names[] = { "Iron", "Steel", "Brass" };
      JComboBox comboBox = new JComboBox( names );

      JTextField textField = new JTextField( "TextField" );
      JButton button1 = new JButton( "Button 1" );
      JButton button2 = new JButton( "Button 2" );
      JButton button3 = new JButton( "Button 3" );

      // weightx e weighty para textArea1 s�o 0: o padr�o
      // anchor para todos os componentes CENTER: o padr�o
      constraints.fill = GridBagConstraints.BOTH;
      addComponent( textArea1, 0, 0, 1, 3 );     
       
      // weightx e weighty para button1 s�o 0: o padr�o
      constraints.fill = GridBagConstraints.HORIZONTAL;
      addComponent( button1, 0, 1, 2, 1 );             
      
      // weightx e weighty para comboBox s�o 0: o padr�o
      // fill � HORIZONTAL
      addComponent( comboBox, 2, 1, 2, 1 );

      // button2
      constraints.weightx = 1000;  // pode crescer na largura
      constraints.weighty = 1;     // pode crescer na altura
      constraints.fill = GridBagConstraints.BOTH;    
      addComponent( button2, 1, 1, 1, 1 );           
       
      // preenchimento � BOTH para button3
      constraints.weightx = 0;            
      constraints.weighty = 0;            
      addComponent( button3, 1, 2, 1, 1 );
       
      // weightx e weighty para textField s�o 0, preenchimento � BOTH
      addComponent( textField, 3, 0, 2, 1 );

      // weightx e weighty para textArea2 s�o 0, preenchimento � BOTH
      addComponent( textArea2, 3, 2, 1, 1 );
   } // fim do construtor GridBagFrame

   // m�todo para configurar restri��es em
   private void addComponent( Component component,
      int row, int column, int width, int height )
   {
      constraints.gridx = column; // configura gridx                           
      constraints.gridy = row; // configura gridy                              
      constraints.gridwidth = width; // configura gridwidth                    
      constraints.gridheight = height; // configure gridheight                 
      layout.setConstraints( component, constraints ); // configura constraints
      add( component ); // adiciona componente
   } // fim do m�todo addComponent
} // fim da classe GridBagFrame


/**************************************************************************
 * (C) Copyright 1992-2005 by Deitel & Associates, Inc. and               *
 * Pearson Education, Inc. All Rights Reserved.                           *
 *                                                                        *
 * DISCLAIMER: The authors and publisher of this book have used their     *
 * best efforts in preparing the book. These efforts include the          *
 * development, research, and testing of the theories and programs        *
 * to determine their effectiveness. The authors and publisher make       *
 * no warranty of any kind, expressed or implied, with regard to these    *
 * programs or to the documentation contained in these books. The authors *
 * and publisher shall not be liable in any event for incidental or       *
 * consequential damages in connection with, or arising out of, the       *
 * furnishing, performance, or use of these programs.                     *
 *************************************************************************/