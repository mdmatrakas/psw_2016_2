package a10_08_BorderLayout;

import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JButton;

/**
 * Demonstrando BorderLayout.
 * 
 * @author MDMatrakas
 * 
 */
@SuppressWarnings("serial")
public class FrameBorderLayout extends JFrame implements ActionListener
{
	/**
	 * vetor de bot�es para ocultar partes
	 */
	private JButton			botoes[];
	private final String	nomes[]	= { "Esconde Norte", "Esconde Sul",
			"Escode Leste", "Esconde Oeste", "Esconde Centro" };
	/**
	 * objeto borderlayout
	 */
	private BorderLayout	layout;

	/**
	 * configura GUI e tratamento de evento
	 */
	public FrameBorderLayout()
	{
		super("BorderLayout Demo");

		layout = new BorderLayout(5, 5); // espa�os de 5 pixels
		setLayout(layout); // configura o layout de frame
		botoes = new JButton[nomes.length]; // configura o tamanho do vetor

		// cria JButtons e registra ouvintes para eles
		for (int count = 0; count < nomes.length; count++)
		{
			botoes[count] = new JButton(nomes[count]);
			botoes[count].addActionListener(this);
		}

		add(botoes[0], BorderLayout.NORTH); // adiciona bot�o para o norte
		add(botoes[1], BorderLayout.SOUTH); // adiciona bot�o para o sul
		add(botoes[2], BorderLayout.EAST); // adiciona bot�o para o leste
		add(botoes[3], BorderLayout.WEST); // adiciona bot�o para o oeste
		add(botoes[4], BorderLayout.CENTER); // adiciona bot�o para o centro
	}

	/**
	 * trata os eventos de bot�o
	 */
	public void actionPerformed(ActionEvent event)
	{
		// verifica a origem de evento e o painel de conte�do de layout correspondentemente
		for (JButton button : botoes)
		{
			if (event.getSource() == button)
				button.setVisible(false); // oculta o bot�o clicado
			else
				button.setVisible(true); // mostra outros bot�es
		}
		layout.layoutContainer(getContentPane()); // painel de conte�do de layout
	}
}
