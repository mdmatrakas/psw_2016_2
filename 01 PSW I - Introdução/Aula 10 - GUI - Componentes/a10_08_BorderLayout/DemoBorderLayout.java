package a10_08_BorderLayout;

import javax.swing.JFrame;

/**
 * Testando BorderLayoutFrame.
 * @author MDMatrakas
 *
 */
public class DemoBorderLayout 
{
   public static void main( String args[] )
   { 
      FrameBorderLayout border = new FrameBorderLayout(); 
      border.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
      border.setSize( 300, 200 ); // configura o tamanho do frame
      border.setVisible( true ); // exibe o frame
   } 
} 