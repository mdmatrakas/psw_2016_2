package a01_07_DiarioClasse_MetodoAcesso;

// construtor DiarioClasse utilizado para especificar o nome do curso na
// hora em que cada objeto DiarioClasse � criado.
public class TesteDiarioClasse
{
	// m�todo main inicia a execu��o do programa
	public static void main(String args[])
	{
		// cria objeto DiarioClasse
		DiarioClasse diarioClasse1 = new DiarioClasse("CS101 Introdu��o a programa��o Java");
		DiarioClasse diarioClasse2 = new DiarioClasse("CS102 Estruturas de dados em Java");

		// exibe valor inicial de nomeCurso para cada DiarioClasse
		System.out.printf("O nome do curso diarioClasse1 �: %s\n", diarioClasse1.getNomeCurso());
		System.out.printf("O nome do curso diarioClasse2 �: %s\n", diarioClasse2.getNomeCurso());
	} 
} 

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
