package a01_06_DiarioClasse_Construtor;

// construtor DiarioClasse utilizado para especificar o nome do curso na
// hora em que cada objeto DiarioClasse � criado.
public class TesteDiarioClasse
{
	// m�todo main inicia a execu��o do programa
	public static void main(String args[])
	{
		// cria objeto DiarioClasse
		DiarioClasse diarioClasse1 = new DiarioClasse("Projeto de Software I");
		DiarioClasse diarioClasse2 = new DiarioClasse("Projeto de Software II");

		// exibe valor inicial de nomeCurso para cada DiarioClasse
		System.out.printf("O nome da disciplina do primeiro di�rio �: %s\n", diarioClasse1.getNomeCurso());
		System.out.printf("O nome da disciplina do segundo di�rio �: %s\n", diarioClasse2.getNomeCurso());
	}
} 

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
