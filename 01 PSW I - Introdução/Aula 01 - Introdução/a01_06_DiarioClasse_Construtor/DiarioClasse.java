package a01_06_DiarioClasse_Construtor;

// Classe DiarioClasse com um construtor para inicializar o nome de um curso.
public class DiarioClasse
{
	private String	nomeCurso;	// nome do curso para esse DiarioClasse

	// construtor inicializa nomeCurso com String fornecido como argumento
	public DiarioClasse(String nome)
	{
		nomeCurso = nome; // inicializa nomeCurso
	} 

	// m�todo para configurar o nome do curso
	public void setNomeCurso(String nome)
	{
		nomeCurso = nome; // armazena o nome do curso
	} 

	// m�todo para recuperar o nome do curso
	public String getNomeCurso()
	{
		return nomeCurso;
	} 

	// exibe uma mensagem de boas-vindas para o usu�rio DiarioClasse
	public void mostraMensagem()
	{
		// essa instru��o chama getNomeCurso para obter o
		// nome do curso que esse DiarioClasse representa
		System.out.printf("Diario de Classe: Bem vindo ao curso\n%s!\n", getNomeCurso());
	} 
} 

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
