package a01_08_Conta_Construtor_MetodoAcesso;

import java.util.Scanner;

// Cria e manipula um objeto Conta.
public class TesteConta
{
	// m�todo principal inicia a execu��o do aplicativo Java
	public static void main(String args[])
	{
		Conta conta1 = new Conta(50.00); // cria o objeto Conta
		Conta conta2 = new Conta(-7.53); // cria o objeto Conta
		
		// exibe saldo inicial de cada objeto
		System.out.printf("Saldo de conta1: $%.2f\n", conta1.getSaldo());
		System.out.printf("Saldo de conta2: $%.2f\n\n", conta2.getSaldo());
		
		// cria Scanner para obter entrada a partir da janela de comando
		Scanner input = new Scanner(System.in);
		double depositAmount; // quantia de dep�sito lida a do usu�rio
		
		System.out.print("Forne�a valor de dep�sito para conta1: "); // prompt
		depositAmount = input.nextDouble(); // obt�m a entrada do usu�rio
		System.out.printf("\nadicionando %.2f ao saldo de conta1\n\n", depositAmount);
		conta1.credito(depositAmount); // adiciona o saldo de conta1
		
		// exibe os saldos
		System.out.printf("Saldo de conta1: $%.2f\n", conta1.getSaldo());
		System.out.printf("Saldo de conta2: $%.2f\n\n", conta2.getSaldo());
		
		System.out.print("Forne�a valor de dep�sito para conta2: "); // prompt
		depositAmount = input.nextDouble(); // obt�m a entrada do usu�rio
		System.out.printf("\nadicionando %.2f ao saldo de conta2\n\n", depositAmount);
		conta2.credito(depositAmount); // adiciona ao saldo de conta2
		
		// exibe os saldos
		System.out.printf("Saldo de conta1: $%.2f\n", conta1.getSaldo());
		System.out.printf("Saldo de conta2: $%.2f\n\n", conta2.getSaldo());
	} 

} 

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
