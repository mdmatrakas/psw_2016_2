package a01_08_Conta_Construtor_MetodoAcesso;

// classe Conta com um construtor para
// inicializar a vari�vel de inst�ncia saldo.
public class Conta
{
	private double	saldo;	// vari�vel de inst�ncia que armazena o saldo

	public Conta(double saldoInicial)
	{
		// valida que saldoInicial � maior que 0,0;
		// se n�o, o saldo � inicializado como o valor padr�o 0.0
		if (saldoInicial > 0.0)
			saldo = saldoInicial;
		else 
			saldo = 0.0;
	} 
	
	// credita (adiciona) uma quantia � conta
	public void credito(double valor)
	{
		saldo = saldo + valor; // adiciona valor ao saldo
	} 
	
	// retorna o saldo da conta
	public double getSaldo()
	{
		return saldo; // fornece o valor de saldo ao m�todo chamador
	} 
} 


/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
