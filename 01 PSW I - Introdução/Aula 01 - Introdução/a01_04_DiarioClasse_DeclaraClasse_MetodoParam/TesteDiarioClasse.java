package a01_04_DiarioClasse_DeclaraClasse_MetodoParam;

import java.util.Scanner; // programa utiliza Scanner

// Cria objeto DiarioClasse e passa uma String para
// seu m�todo mostraMensagem.
public class TesteDiarioClasse
{
	// m�todo main inicia a execu��o de programa
	public static void main(String args[])
	{
		// cria Scanner para obter entrada a partir da janela de comando
		Scanner input = new Scanner(System.in);
		// cria um objeto DiarioClasse e o atribui a meuDiarioClasse
		DiarioClasse meuDiarioClasse = new DiarioClasse();
		// prompt para entrada do nome do curso
		System.out.println("Por favor entre com o nome do curso:");
		String curso = input.nextLine(); // l� uma linha de texto
		System.out.println(); // gera sa�da de uma linha em branco
		// chama m�todo mostraMensagem de meuDiarioClasse
		// e passa nomeCurso como um argumento
		meuDiarioClasse.mostraMensagem(curso);
	} 
} 

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
