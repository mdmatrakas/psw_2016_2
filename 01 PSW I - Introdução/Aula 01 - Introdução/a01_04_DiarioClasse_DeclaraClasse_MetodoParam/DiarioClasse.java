package a01_04_DiarioClasse_DeclaraClasse_MetodoParam;

// Declara��o de classe com um m�todo que tem um par�metro.
public class DiarioClasse
{
	// exibe uma mensagem de boas-vindas para o usu�rio
	public void mostraMensagem(String nomeCurso)
	{
		System.out.printf("Diario de Classe: Bem vindo ao curso \n%s!\n", nomeCurso);
	} 
} 

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
