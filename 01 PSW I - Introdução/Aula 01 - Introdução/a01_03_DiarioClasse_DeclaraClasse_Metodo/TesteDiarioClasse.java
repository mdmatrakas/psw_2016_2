package a01_03_DiarioClasse_DeclaraClasse_Metodo;

// Cria um objeto DiarioClasse e chama seu m�todo mostraMensagem.
public class TesteDiarioClasse
{
	// m�todo main inicia a execu��o de programa
	public static void main(String args[])
	{
		// declara um objeto DiarioClasse 
		DiarioClasse meuDiarioClasse;
		// cria um objeto DiarioClasse e o atribui a meuDiarioClasse
		// instanciar o objeto
		meuDiarioClasse = new DiarioClasse();

		// chama m�todo mostraMensagem de meuDiarioClasse
		meuDiarioClasse.mostraMensagem();
	} 
} 

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/