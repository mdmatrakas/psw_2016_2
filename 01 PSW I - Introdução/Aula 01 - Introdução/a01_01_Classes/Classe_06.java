package a01_01_Classes;

//Declara��o de uma classe
//Nome: Classe_06

//Uma classe com um segundo m�todo
//Cria��o de um objeto da Classe_06
//Utiliza��o do objeto da Classe_06
public class Classe_06
{
	public static void main(String[] args)
	{
		System.out.println("Mensagem do m�todo Java main!");
		
		// Declara��o de um objeto
		Classe_06 obj;
		
		// Cria��o ou inst�ncia��o de um objeto
		obj = new Classe_06();
		
		// Chamada de um m�todo de obj
		obj.mensagem();
	}

	public void mensagem()
	{
		System.out.println("Mensagem do segundo m�todo Java!");
	}
}

/**************************************************************************
(C) Copyright 2011 by Miguel Matrakas
*************************************************************************/