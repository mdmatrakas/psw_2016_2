package a01_01_Classes;

// Declara��o de uma classe
// Nome: Classe_04

// Uma classe com um segundo m�todo
public class Classe_04
{
	public static void main(String[] args)
	{
		System.out.println("Mensagem do m�todo Java main!");
		mensagem();
	}

	public static void mensagem()
	{
		System.out.println("Mensagem do segundo m�todo Java!");
	}
}

/**************************************************************************
(C) Copyright 2011 by Miguel Matrakas
*************************************************************************/