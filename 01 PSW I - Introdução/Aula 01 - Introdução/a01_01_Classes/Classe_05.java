package a01_01_Classes;

//Declara��o de uma classe
//Nome: Classe_05

//Uma classe com um segundo m�todo
//Cria��o de um objeto da Classe_05
public class Classe_05
{
	public static void main(String[] args)
	{
		System.out.println("Mensagem do m�todo Java main!");
		
		// Declara��o de um objeto
		@SuppressWarnings("unused")
		Classe_05 obj;
		
		// Cria��o ou inst�ncia��o de um objeto
		obj = new Classe_05();
	}
}

/**************************************************************************
(C) Copyright 2011 by Miguel Matrakas
*************************************************************************/