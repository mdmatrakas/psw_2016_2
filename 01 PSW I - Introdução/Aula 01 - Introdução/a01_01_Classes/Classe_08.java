package a01_01_Classes;

// Declara��o de uma classe com vari�veis de inst�ncia e com dois m�todos
//Cria��o de um objeto da Classe_08
//Utiliza��o do objeto da Classe_08
public class Classe_08
{
	// Declara��o de vari�veis de inst�ncia
	int		n;
	float	f;
	double	d;
	char	c;

	public static void main(String[] args)
	{
		System.out.println("Classe Java com vari�veis de inst�ncia!");
		Classe_08 obj = new Classe_08();// Cria��o de um objeto da Classe_08
		obj.inicializa();// chama o m�todo de inicializa��o das vari�veis de inst�ncia	
		obj.mostrar();// Chama ao m�todo mostrar()
	}
	
	public void inicializa()
	{// Inicializa as vari�veis de inst�ncia
		n	= 1;
		f	= 1.2F;
		d	= 2.3;
		c	= 'a';
	}

	public void mostrar()
	{
		// Mostrar os valores de cada uma das vari�veis de inst�ncia
		System.out.println("Valores das vari�veis de inst�ncia de Classe_08:");
		System.out.printf("valor de n1 = %d\n", n);
		System.out.printf("valor de f = %f\n", f);
		System.out.printf("valor de d = %f\n", d);
		System.out.printf("valor de c = %c\n", c);
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
