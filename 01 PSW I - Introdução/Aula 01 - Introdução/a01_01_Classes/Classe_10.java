package a01_01_Classes;

import java.util.Scanner;

// Classe com leitura de um valor informado pelo usu�rio
public class Classe_10
{
	public static void main(String[] args)
	{
		// vari�vel local a ser lida do teclado
		int n;	
		// cria Scanner para obter entrada a partir da janela de comando
		Scanner input = new Scanner(System.in);
		
		// Solicita ao usu�rio um valor inteiro
		System.out.println("Por favor entre com um valor inteiro:");
		n = input.nextInt(); // l� um valor inteiro
		// Mostra o valor digitado pelo usu�rio
		System.out.printf("\nO valor digitado foi: %d", n); 
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/