package a01_01_Classes;

// Declara��o de uma classe com vari�veis de inst�ncia inicializadas com valores padr�o
public class Classe_09_01
{
	// Declara��o e inicializa��o de vari�veis de inst�ncia
	int		n	= 2;
	float	f	= 2.3F;
	double	d	= 3.5;
	char	c	= 'b';

	public static void main(String[] args)
	{
		System.out.println("Classe Java com vari�veis de inst�ncia inicializadas!");

		// Cria��o de dois objetos da Classe_09_02
		System.out.println("Criar obj1.");
		Classe_09_01 obj1 = new Classe_09_01();
		System.out.println("Criar obj2.");
		Classe_09_01 obj2 = new Classe_09_01();

		// chama o m�todo de inicializa��o das vari�veis de inst�ncia
		System.out.println("Inicializar vari�veis do obj2 usando o m�todo inicializa.");
		obj2.inicializa();

		// Chama ao m�todo mostrar()
		System.out.println("Mostrar os valores para vari�veis de obj1:");
		obj1.mostrar();
		System.out.println("Mostrar os valores para vari�veis de obj2:");
		obj2.mostrar();
	}

	public void inicializa()
	{
		System.out.println("Inicializando vari�veis de inst�ncia usando o m�todo inicializa.");
		// Inicializa as vari�veis de inst�ncia
		n = 1;
		f = 1.2F;
		d = 2.3;
		c = 'a';
	}

	public void mostrar()
	{
		// Mostrar os valores de cada uma das vari�veis de inst�ncia
		System.out.println("Valores das vari�veis de inst�ncia de Classe_09_02:");
		System.out.printf("valor de n1 = %d\n", n);
		System.out.printf("valor de f = %f\n", f);
		System.out.printf("valor de d = %f\n", d);
		System.out.printf("valor de c = %c\n", c);
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
