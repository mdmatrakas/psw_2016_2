package a01_01_Classes;


//Declara��o de uma classe com vari�veis no m�todo main
//Nome: Classe_03
public class Classe_03
{
	public static void main(String[] args)
	{
		//Declara��o de vari�veis locais
		int n1 = 1;
		int n2 = 2;
		float f = 1.2F;
		double d = 2.3;
		char c = 'a';
		
		// Mostrar os valores de cada uma das vari�veis locais
		System.out.println("Valores das vari�veis locais de main:");
		System.out.printf("valor de n1 = %d\n", n1);
		System.out.printf("valor de n2 = %d\n", n2);
		System.out.printf("valor de f = %f\n", f);
		System.out.printf("valor de d = %f\n", d);
		System.out.printf("valor de c = %c\n", c);
	}
}

/**************************************************************************
(C) Copyright 2011 by Miguel Matrakas
*************************************************************************/
