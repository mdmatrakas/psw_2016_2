package a01_01_Classes;

import java.util.Scanner;

//Classe com leitura de um valor informado pelo usu�rio
public class Classe_11
{
	public static void main(String[] args)
	{
		// vari�vel local a ser lida do teclado
		String str;
		
		// cria Scanner para obter entrada a partir da janela de comando
		Scanner input = new Scanner(System.in);
		
		// Solicita ao usu�rio um valor inteiro
		System.out.println("Por favor entre com uma string:");
		str = input.nextLine(); // l� um valor inteiro
		// Mostra o valor digitado pelo usu�rio
		System.out.printf("\nA string digitada foi:\n%s", str); 
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/