package a01_01_Classes;

// Declara��o de uma classe com vari�veis de inst�ncia
// Nome: Classe_07
public class Classe_07
{
	// Declara��o de vari�veis de inst�ncia
	int		n;
	float	f;
	double	d;
	char	c;

	public static void main(String[] args)
	{
		System.out.println("Classe Java com vari�veis de inst�ncia!");
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
