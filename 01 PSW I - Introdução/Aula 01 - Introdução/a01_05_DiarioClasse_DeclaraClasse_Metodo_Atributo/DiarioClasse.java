package a01_05_DiarioClasse_DeclaraClasse_Metodo_Atributo;

// classe DiarioClasse que cont�m uma vari�vel de inst�ncia nomeCurso
// e m�todos para configurar e obter seu valor.
public class DiarioClasse
{
	private String	nomeCurso;	// nome do curso para esse DiarioClasse

	// m�todo para configurar o nome do curso
	public void setNomeCurso(String name)
	{
		nomeCurso = name; // armazena o nome do curso
	} 

	// m�todo para recuperar o nome do curso
	public String getNomeCurso()
	{
		return nomeCurso;
	} 

	// exibe uma mensagem de boas-vindas para o usu�rio DiarioClasse
	public void mostraMensagem()
	{
		// essa instru��o chama getNomeCurso para obter o
		// nome do curso que esse DiarioClasse representa
		System.out.printf("Diario de Classe: Bem vindo ao curso\n%s!\n", 
				getNomeCurso());
	} 
} 

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
