package a01_05_DiarioClasse_DeclaraClasse_Metodo_Atributo;

import java.util.Scanner; // programa utiliza Scanner

// Cria e manipula um objeto DiarioClasse.
public class TesteDiarioClasse
{
	// m�todo main inicia a execu��o de programa
	public static void main(String args[])
	{
		// cria Scanner para obter entrada a partir da janela de comando
		Scanner input = new Scanner(System.in);
		// cria um objeto DiarioClasse e o atribui a meuDiarioClasse
		DiarioClasse meuDiarioClasse = new DiarioClasse();
		// exibe valor inicial de nomeCurso
		System.out.printf("O nome inicial do curso �: %s\n\n", 
				meuDiarioClasse.getNomeCurso());
		// solicita e l� o nome do curso
		System.out.println("Entre com o nome do curso:");
		String oNome = input.nextLine(); // l� uma linha de texto
		meuDiarioClasse.setNomeCurso(oNome); // configura o nome do curso
		System.out.println(); // gera sa�da de uma linha em branco
		// exibe mensagem de boas-vindas depois de especificar nome do curso
		meuDiarioClasse.mostraMensagem();
	} 
} 

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
