package a01_02_Classes_2;

import java.util.Scanner; // programa utiliza a classe Scanner

/**
 * Programa de adi��o que exibe a soma de dois n�meros.
 * @author Matrakas
 *
 */
public class Adicao 
{
   // m�todo principal inicia a execu��o do aplicativo Java
   public static void main( String args[] )
   {
      // cria Scanner para obter entrada a partir da janela de comando
      Scanner in = new Scanner( System.in );            
      int num1; // primeiro n�mero a somar
      int num2; // segundo n�mero a adicionar
      int soma; // soma de number1 e number2
      System.out.print( "Entre com o primeiro inteiro: " ); // prompt
      num1 = in.nextInt(); // l� primeiro o n�mero fornecido pelo usu�rio
      System.out.print( "Entre com o segundo inteiro: " ); // prompt
      num2 = in.nextInt(); // l� o segundo n�mero fornecido pelo usu�rio
      soma = num1 + num2; // soma os n�meros
      System.out.printf( "A soma dos numeros � %d\n", soma ); // exibe a soma
   }
} 

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/