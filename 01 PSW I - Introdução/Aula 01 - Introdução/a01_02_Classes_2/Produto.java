package a01_02_Classes_2;



import java.util.Scanner; // programa utiliza Scanner

/**
 *  Calcula o produto de tr�s inteiros.
 */
public class Produto 
{
   public static void main( String args[] )
   {
      // cria Scanner para obter entrada a partir da janela de comando
      Scanner input = new Scanner( System.in );

      int x; // primeiro n�mero inserido pelo usu�rio
      int y; // segundo n�mero inserido pelo usu�rio
      int z; // terceiro n�mero inserido pelo usu�rio
      int resultado; // produto dos n�meros

      System.out.print( "Enter first integer: " ); // solicita entrada
      x = input.nextInt(); // l� o primeiro inteiro

      System.out.print( "Enter second integer: " ); // solicita entrada
      y = input.nextInt(); // l� o segundo inteiro
      
      System.out.print( "Enter third integer: " ); // solicita entrada
      z = input.nextInt(); // l� o terceiro inteiro

      resultado = x * y * z; // calcula o produto dos n�meros

      System.out.printf( "Produto is %d\n", resultado );
   } 
} 

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/