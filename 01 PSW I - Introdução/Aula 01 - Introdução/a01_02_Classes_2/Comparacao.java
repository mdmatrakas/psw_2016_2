package a01_02_Classes_2;

import java.util.Scanner;


/**
 * Compara inteiros utilizando instru��es if, operadores relacionais e operadores de igualdade.
 * @author Matrakas
 *
 */
public class Comparacao 
{
   /**
    *  m�todo principal inicia a execu��o do aplicativo Java
    * @param args
    */
   public static void main( String args[] )
   {
      // cria Scanner para obter entrada a partir da janela de comando
      Scanner entrada = new Scanner( System.in );

      int num1; // primeiro n�mero a comparar
      int num2; // segundo n�mero a comparar

      System.out.print( "Entre com o primeiro inteiro: " ); // prompt
      num1 = entrada.nextInt(); // l� primeiro o n�mero fornecido pelo usu�rio
      System.out.print( "entre com o segundo inteiro: " ); // prompt
      num2 = entrada.nextInt(); // l� o segundo n�mero fornecido pelo usu�rio
      
      if ( num1 == num2 )                              
         System.out.printf( "%d == %d\n", num1, num2 );
      if ( num1 != num2 )                              
         System.out.printf( "%d != %d\n", num1, num2 );
      if ( num1 < num2 )                              
         System.out.printf( "%d < %d\n", num1, num2 );
      if ( num1 > num2 )                              
         System.out.printf( "%d > %d\n", num1, num2 );
      if ( num1 <= num2 )                              
         System.out.printf( "%d <= %d\n", num1, num2 );
      if ( num1 >= num2 )                              
         System.out.printf( "%d >= %d\n", num1, num2 );
   } 
} 

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/