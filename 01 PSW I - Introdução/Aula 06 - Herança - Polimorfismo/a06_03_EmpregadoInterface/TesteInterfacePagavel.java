package a06_03_EmpregadoInterface;

/**
 * Testa a interface Pagavel.
 * 
 * @author Matrakas
 * 
 */
public class TesteInterfacePagavel
{
	public static void main(String args[])
	{
		// cria um vetor Pagavel de quatro elementos
		Pagavel objetosPagaveis[] = new Pagavel[4];

		// preenche o vetor com objetos que implementam Pagavel
		objetosPagaveis[0] = new Fatura("01234", "banco", 2, 375.00);
		objetosPagaveis[1] = new Fatura("56789", "pneu", 4, 220.00);
		objetosPagaveis[2] = new EmpregadoAssalariado("Jos�", "Gomes", "111.111.111-11", 800.00);
		objetosPagaveis[3] = new EmpregadoAssalariado("Luiza", "Brunet", "222.222.222-22", 1200.00);

		System.out.println("Faturas e empregados processados polimorficamente:\n");

		// processa genericamente cada elemento no vetor de objetosPagaveis
		for (Pagavel pagavelCorrente : objetosPagaveis)
		{
			// gera sa�da do pagavelCorrente e seu valor de pagamento correspondente
			System.out.printf("%s \n%s: $%,.2f\n\n", pagavelCorrente.toString(), "valor pagamento",
					pagavelCorrente.getQuantiaPagamento());
		}
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
