package a06_03_EmpregadoInterface;

/**
 * Classe Fatura que implementa Pagavel.
 * 
 * @author Matrakas
 */
public class Fatura implements Pagavel
{
	private String	codigoItem;
	private String	descricaoItem;
	private int		quantidade;
	private double	precoPorItem;

	/**
	 * Construtor com quatro argumentos.
	 * 
	 * @param cod
	 *            String com o c�digo do item.
	 * @param desc
	 *            String com a descri��o do item.
	 * @param quant
	 *            Inteiro designando o quantidade de itens.
	 * @param preco
	 *            Ponto flutuante com o pre�o individual do item.
	 */
	public Fatura(String cod, String desc, int quant, double preco)
	{
		codigoItem = cod;
		descricaoItem = desc;
		setQuantidade(quant); // valida e armazena a quantidade
		setPrecoPorItem(preco); // valida e armazena o pre�o por item
	}

	/**
	 * M�todo de acesso para configurar o c�digo do item da fatura.
	 * 
	 * @param cod
	 *            String com o c�digo do item.
	 */
	public void setCodigoItem(String cod)
	{
		codigoItem = cod;
	}

	/**
	 * M�todo de acesso para receber o c�digo do item da fatura.
	 * 
	 * @return String com o c�digo do item.
	 */
	public String getCodigoItem()
	{
		return codigoItem;
	}

	/**
	 * M�todo de acesso para configurar a descri��o do item da fatura.
	 * 
	 * @param desc
	 *            String com a descri��o do item.
	 */
	public void setDescricaoItem(String desc)
	{
		descricaoItem = desc;
	}

	/**
	 * M�todo de acesso para receber a descri��o do item da fatura.
	 * 
	 * @return String com a descri��o do item.
	 */
	public String getDescricaoItem()
	{
		return descricaoItem;
	}

	/**
	 * M�todo de acesso para configurar quantidade de itens da fatura.
	 * 
	 * @param quant
	 *            Inteiro designando o quantidade de itens.
	 */
	public void setQuantidade(int quant)
	{
		quantidade = (quant < 0) ? 0 : quant; // quantidade n�o pode ser negativa
	}

	/**
	 * M�todo de acesso para receber quantidade de itens da fatura.
	 * 
	 * @return Inteiro designando o quantidade de itens.
	 */
	public int getQuantidade()
	{
		return quantidade;
	}

	/**
	 * M�todo de acesso para configurar o pre�o por item da fatura.
	 * 
	 * @param preco
	 *            Ponto flutuante com o pre�o individual do item.
	 */
	public void setPrecoPorItem(double preco)
	{
		precoPorItem = (preco < 0.0) ? 0.0 : preco; // valida pre�o
	}

	/**
	 * M�todo de acesso para receber o pre�o por item da fatura.
	 * 
	 * @return Ponto flutuante com o pre�o individual do item.
	 */
	public double getPrecoPorItem()
	{
		return precoPorItem;
	}

	/**
	 * Implementa��o do m�todo toString, que devolve a representa��o String do objeto Fatura
	 */
	public String toString()
	{
		return String.format("%s: \n%s: %s (%s) \n%s: %d \n%s: $%,.2f", "Fatura", "codigo item",
				getCodigoItem(), getDescricaoItem(), "quantidade", getQuantidade(),
				"pre�o por item", getPrecoPorItem());
	}

	/**
	 * M�todo requerido para executar o contrato com a interface Payable
	 */
	public double getQuantiaPagamento()
	{
		return getQuantidade() * getPrecoPorItem(); // calcula custo total
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
