package a06_02_EmpregadoHierarquia;

/**
 * A classe EmpregadoComissionado representa um empregado comissionado, e extende a classe
 * Empregado.
 * 
 * @author Matrakas
 */
public class EmpregadoComissionado extends Empregado
{
	/**
	 * Vendas brutas semanais
	 */
	private double	vendasBrutas;
	/**
	 * Porcentagem da comiss�o
	 */
	private double	taxaComissao;

	/**
	 * Construtor de cinco argumentos
	 * 
	 * @param primeiro
	 *            String com o primeiro nome do empregado.
	 * @param ultimo
	 *            String com o �ltimo nome do empregado.
	 * @param cpf
	 *            String com o n�mero do CPF do empregado.
	 * @param vendas
	 *            Ponto flutuante com o valor de vendas realizadas pelo empregado.
	 * @param taxa
	 *            Ponto flutuante com o valor da taxa de comiss�o do empregado.
	 */
	public EmpregadoComissionado(String primeiro, String ultimo, String cpf, double vendas,
			double taxa)
	{
		super(primeiro, ultimo, cpf);
		setVendasBrutas(vendas); // valida e armazena as vendas brutas
		setTaxaComissao(taxa); // valida e armazena a taxa de comiss�o
	}

	/**
	 * M�todo de acesso para configurar a taxa de comiss�o.
	 * 
	 * @param taxa
	 *            Ponto flutuante com o valor da taxa de comiss�o do empregado.
	 */
	public void setTaxaComissao(double taxa)
	{
		taxaComissao = (taxa > 0.0 && taxa < 1.0) ? taxa : 0.0;
	}

	/**
	 * M�todo de acesso para receber a taxa de comiss�o.
	 * 
	 * @return Ponto flutuante com o valor da taxa de comiss�o do empregado.
	 */
	public double getTaxaComissao()
	{
		return taxaComissao;
	}

	/**
	 * M�todo de acesso para configurar a quantidade de vendas brutas.
	 * 
	 * @param vendas
	 *            Ponto flutuante com o valor de vendas brutas do empregado.
	 */
	public void setVendasBrutas(double vendas)
	{
		vendasBrutas = (vendas < 0.0) ? 0.0 : vendas;
	}

	/**
	 * M�todo de acesso para receber a quantidade de vendas brutas.
	 * 
	 * @return Ponto flutuante com o valor de vendas brutas do empregado.
	 */
	public double getVendasBrutas()
	{
		return vendasBrutas;
	}

	/**
	 * Calcula os proventos totais do empregado.
	 * 
	 * Sobrescreve o m�todo proventos em Empregado.
	 * 
	 * @return Ponto flutuante com o valor total a ser pago ao empregado.
	 */
	public double proventos()
	{
		return getTaxaComissao() * getVendasBrutas();
	}

	/**
	 * Implementa��o do m�todo toString, que devolve a representa��o String do objeto
	 * EmpregadoComissionado
	 */
	public String toString()
	{
		return String.format("%s: %s\n%s: %.2f\n%s: %.2f", "Empregado comissionado",
				super.toString(), "Vendas brutas", getVendasBrutas(), "Taxa de comiss�o",
				getTaxaComissao());
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
