package a06_02_EmpregadoHierarquia;

/**
 * Classe SalariedEmployee estende Employee.
 * 
 * @author Matrakas
 * 
 */
public class EmpregadoAssalariado extends Empregado
{
	private double	valorSalario;

	/**
	 * Construtor com quatro argumentos
	 * 
	 * @param primeiro
	 *            String com o primeiro nome do empregado.
	 * @param ultimo
	 *            String com o �ltimo nome do empregado.
	 * @param cpf
	 *            String com o n�mero do CPF do empregado.
	 * @param salario
	 *            Ponto flutuante com o valor do sal�rio base do empregado.
	 */
	public EmpregadoAssalariado(String primeiro, String ultimo, String cpf, double salario)
	{
		super(primeiro, ultimo, cpf); // passa para o construtor Employee
		setValorSalario(salario); // valida e armazena o sal�rio
	}

	/**
	 * Valida e configura o sal�rio do empregado.
	 * 
	 * @param salario
	 */
	public void setValorSalario(double salario)
	{
		valorSalario = salario < 0.0 ? 0.0 : salario;
	}

	/**
	 * M�todo de acesso para o sal�rio.
	 * 
	 * @return Ponto flutuante com o valor do sal�rio.
	 */
	public double getValorSalario()
	{
		return valorSalario;
	}

	/**
	 * Calcula os proventos totais do empregado.
	 * 
	 * Sobrescreve o m�todo proventos em Empregado.
	 * 
	 * @return Ponto flutuante com o valor total a ser pago ao empregado.
	 */
	public double proventos()
	{
		return getValorSalario();
	}

	/**
	 * Implementa��o do m�todo toString, que devolve a representa��o String do objeto
	 * EmpregadoAssalariado.
	 */
	public String toString()
	{
		return String.format("Empregado assalariado: %s\n%s: $%,.2f", super.toString(),
				"Valor do sal�rio", getValorSalario());
	}
}

/**************************************************************************
 * (C) Copyright 2011 by Miguel Matrakas
 *************************************************************************/
