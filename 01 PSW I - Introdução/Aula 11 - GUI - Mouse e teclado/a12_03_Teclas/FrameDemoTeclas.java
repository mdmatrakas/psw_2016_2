package a12_03_Teclas;
 
import java.awt.Color;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import javax.swing.JTextArea;

/**
 * Demonstrando os eventos de pressionamento de tecla.
 * 
 * @author MDMatrakas
 * 
 */
@SuppressWarnings("serial")
public class FrameDemoTeclas extends JFrame implements KeyListener
{
	private String		linha1	= "";	// primeira linha da �rea de texto
	private String		linha2	= "";	// segunda linha da �rea de texto
	private String		linha3	= "";	// terceira linha da �rea de texto
	private String		linha4	= "";	// quarta linha da �rea de texto
	private JTextArea	areaTexto;		// �rea de texto para exibir a sa�da

	public static void main(String args[])
	{
		FrameDemoTeclas f = new FrameDemoTeclas();
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setSize(350, 100); // configura o tamanho do frame
		f.setVisible(true); // exibe o frame
	}

	/**
	 * construtor KeyDemoFrame
	 */
	public FrameDemoTeclas()
	{
		super("Demonstrando eventos do teclado");

		areaTexto = new JTextArea(10, 15); // configura JTextArea
		areaTexto.setText("Pressione uma tecla no teclado...");
		areaTexto.setEnabled(false); // desativa textarea
		areaTexto.setDisabledTextColor(Color.BLACK); // configura cor de texto
		add(areaTexto); // adiciona textarea ao JFrame

		addKeyListener(this); // permite que o frame processe os eventos de teclado
	}

	/**
	 * Trata pressionamento de qualquer tecla
	 */
	@SuppressWarnings("static-access")
	public void keyPressed(KeyEvent event)
	{
		linha1 = String.format("Tecla pressionada: %s",
				event.getKeyText(event.getKeyCode())); // gera sa�da de tecla pressionada
		setLines2and3(event); // configura a sa�da das linhas dois e tr�s
	} 

	/**
	 *  Trata libera��o de qualquer tecla
	 */
	@SuppressWarnings("static-access")
	public void keyReleased(KeyEvent event)
	{
		linha1 = String.format("Tecla liberada: %s",
				event.getKeyText(event.getKeyCode())); // gera sa�da de tecla liberada
		setLines2and3(event); // configura a sa�da das linhas dois e tr�s
	} 

	/**
	 *  Trata pressionamento de uma tecla de a��o
	 */
	public void keyTyped(KeyEvent event)
	{
		linha1 = String.format("Tecla pressionada (Typed): %s", event.getKeyChar());
		setLines2and3(event); // configura a sa�da das linhas dois e tr�s
	} 

	/**
	 *  Configura segunda e terceira linhas de sa�da
	 * @param event
	 */
	@SuppressWarnings("static-access")
	private void setLines2and3(KeyEvent event)
	{
		linha2 = String.format("Esta tecla %s� uma tecla de a��o",
				(event.isActionKey() ? "" : "n�o "));

		String temp = event.getKeyModifiersText(event.getModifiers());

		linha3 = String.format("Teclas modificadoras pressionadas: %s",
				(temp.equals("") ? "none" : temp)); // modificadores de sa�da

		linha4 = String.format(
				"Tecla pressionada �: %s",
				Character.isDigit(event.getKeyChar()) ? "digito." : 
					    Character.isLetter(event.getKeyChar()) ? 
					    Character.isLowerCase(event.getKeyChar()) ? "letra min�scula."
						: "letra mai�scula" : "nem letra nem digito.");

		areaTexto.setText(String.format("%s\n%s\n%s\n%s\n", linha1, linha2,
				linha3, linha4)); // gera sa�da de tr�s linhas de texto
	} 
} 