package a08_04_FrameTextField;

import java.awt.FlowLayout;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JPasswordField;

/**
 * Demonstrando a classe JTextField e JPasswordField.
 * @author MDMatrakas
 *
 */
@SuppressWarnings("serial")
public class FrameCampoTexto extends JFrame
{
	private JTextField			campoTexto1;	// campo de texto com tamanho configurado
	private JTextField			campoTexto2;	// campo de texto constru�do com texto
	private JTextField			campoTexto3;	// campo de texto com texto e tamanho
	private JPasswordField		campoSenha;		// campo de senha com texto
															
	// construtor FrameCampoTexto adiciona JTextFields a JFrame
	public FrameCampoTexto()
	{
		super("Testando JTextField e JPasswordField");
		setLayout(new FlowLayout()); // configura o layout de frame
		// constr�i campoTexto com 10 colunas
		campoTexto1 = new JTextField(10);
		add(campoTexto1); // adiciona campoTexto1 ao JFrame
		// constr�i campo de texto com texto padr�o
		campoTexto2 = new JTextField("Digite texto aqui");
		add(campoTexto2); // adiciona campoTexto2 ao JFrame
		// constr�i campoTexto com o texto padr�o e 21 colunas
		campoTexto3 = new JTextField("Campo n�o edit�vel.", 21);
		campoTexto3.setEditable(false); // desativa a edi��o
		add(campoTexto3); // adiciona campoTexto3 ao JFrame
		// constr�i campoSenha com o texto padr�o
		campoSenha = new JPasswordField("Texto escondido");
		add(campoSenha); // adiciona campoSenha ao JFrame
	} 
	
	public static void main(String[] args)
	{
		FrameCampoTexto f = new FrameCampoTexto();
		// Determina a opera��o padr�o ao se fechar a janela
		f.setDefaultCloseOperation(EXIT_ON_CLOSE);
		// Determina o tamanho da janela
		f.setSize(500, 200);
		// Torna a janela visivel
		f.setVisible(true);
	}
} 