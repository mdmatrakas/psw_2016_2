package a08_03_FrameLabel;

import java.awt.Color;
import java.awt.FlowLayout; // especifica como os componentes s�o organizados
import javax.swing.JFrame; // fornece recursos b�sicos de janela
import javax.swing.JLabel; // exibe texto e imagens
import javax.swing.SwingConstants; // constantes comuns utilizadas com Swing
import javax.swing.Icon; // interface utilizada para manipular imagens
import javax.swing.ImageIcon; // carrega imagens

/**
 * Demonstrando o uso das classes JLabel, Color, FlowLayout e Icon.
 * @author MDMatrakas
 *
 */
@SuppressWarnings("serial")
public class FrameJLabel extends JFrame
{
	private JLabel	rotulo1;		// JLabel apenas com texto
	private JLabel	rotulo2;		// JLabel constru�do com texto e �cone
	private JLabel	rotulo3;		// JLabel com texto e �cone adicionados
	
	// construtor LabelFrame adiciona JLabels a JFrame
	public FrameJLabel()
	{
		super("Testando JLabel");
		setLayout(new FlowLayout()); // configura o layout de frame
		
		// Construtor JLabel com um argumento de string
		rotulo1 = new JLabel("R�tulo de texto.");
		rotulo1.setForeground(Color.BLUE);
		rotulo1.setToolTipText("Este � o r�tulo 1");
		add(rotulo1); // adiciona o rotulo1 ao JFrame
		
		// construtor JLabel com string, Icon e argumentos de alinhamento
		Icon bug = new ImageIcon(getClass().getResource("bug1.gif"));
		rotulo2 = new JLabel("R�tulo com texto e icone.", bug, SwingConstants.LEFT);
		rotulo2.setToolTipText("Este � o r�tulo 2");
		add(rotulo2); // adiciona rotulo2 ao JFrame
		
		rotulo3 = new JLabel(); // Construtor JLabel sem argumentos
		rotulo3.setText("R�tulo com texto e icone na base.");
		rotulo3.setIcon(bug); // adiciona o �cone ao JLabel
		rotulo3.setHorizontalTextPosition(SwingConstants.LEFT);
		rotulo3.setVerticalTextPosition(SwingConstants.BOTTOM);
		rotulo3.setToolTipText("Este � o r�tulo 3");
		add(rotulo3); // adiciona rotulo3 ao JFrame
	} 
} 