package edu.udc.psw.exercicio_1_11;

public class Data {

	public void mostraData() {
		System.out.printf("%02d/@02d/%4", dia, mes, ano);
	}

	public int compararData(Data obj) {
		if (obj.ano < this.ano) {
			return -1;
		}
		if (obj.ano > this.ano) {
			return 1;
		}
		if (obj.mes < this.mes) {
			return -1;
		}
		if (obj.mes > this.mes) {
			return 1;
		}
		if (obj.dia < this.dia) {
			return -1;
		}
		if (obj.dia > this.dia) {
			return 1;

			return 0;
		}
	}

	private int ano;
	private int mes;
	private int dia;

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public int getMes() {
		return mes;
	}

	public void setMes(int mes) {
		this.mes = mes;
	}

	public int getDia() {
		return dia;
	}

	public void setDia(int dia) {
		this.dia = dia;
	}
}
