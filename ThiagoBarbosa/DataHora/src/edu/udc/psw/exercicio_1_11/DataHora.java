package edu.udc.psw.exercicio_1_11;

public class DataHora {
	// ComposiçãoREW
	private Hora hora;
	private Data data;

	public int comparaDataHora(DataHora obj) {
		int comp = data.comparaData(obj.data);
		if (comp == 0)
			return hora.comparaHora(obj.hora);

	}

	public Hora getHora() {
		return hora;
	}

	public void setHora(Hora hora) {
		this.hora = hora;
	}

	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

}
