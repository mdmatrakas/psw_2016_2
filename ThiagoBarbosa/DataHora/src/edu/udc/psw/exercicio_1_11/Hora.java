package edu.udc.psw.exercicio_1_11;

public class Hora {
	public void mostraHora() {
		System.out.printf("%02d:@02d", hora, minutos);
	}

	public int comparaHora(Hora obj) {
		if (obj.hora < this.hora) {
			return -1;
		}
		if (obj.hora > this.hora) {
			return 1;
		}
		if (obj.minutos < this.minutos) {
			return -1;
		}
		if (obj.minutos > this.minutos) {
			return 1;

			return 0;
		}
		return hora;
	}

	private int hora;
	private int minutos;

	public int getHora() {
		return hora;
	}

	public void setHora(int hora) {
		this.hora = hora;
	}

	public int getMinutos() {
		return minutos;
	}

	public void setMinutos(int minutos) {
		this.minutos = minutos;
	}
}
