package edu.udc.psw;

import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Digite os valores x e y para um ponto");
		Ponto2D p1 = new Ponto2D(sc.nextInt(), sc.nextInt());
		System.out.println("Digite os valores x e y para o outro ponto");
		Ponto2D p2 = new Ponto2D(sc.nextInt(), sc.nextInt());

		System.out.println("Os pontos s�o:" + p1 + "e" + p2);

		if (p1.equals(p2))
			System.out.println(p1 + " � igual a " + p2);
		else
			System.out.println(p1 + " � diferente de " + p2);

		Linha2D linha = new Linha2D(p1, p2);
		System.out.println("A linha formada pelo pontos � " + linha);

		Ponto2D p = p1.clone();

		Linha2D l = new Linha2D(p1, p);

		System.out.println("A nova linha formada � " + l);
		p1.setX(25);
		System.out.println("A linha �:" + linha);
		System.out.println("A linha �:" + l);
		sc.close();
	}

}
