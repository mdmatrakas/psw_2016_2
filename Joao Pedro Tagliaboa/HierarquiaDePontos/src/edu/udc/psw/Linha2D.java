package edu.udc.psw;

public class Linha2D extends FormaGeometrica2D {
	private Ponto2D a;
	private Ponto2D b;
	public Linha2D(Ponto2D a, Ponto2D b) {
		if(a != null && b != null) {
			if(!a.equals(b)) {
				this.a = a.clone();
				this.b = b.clone();
			} else {
				this.a = a.clone();
				this.b = new Ponto2D(0, 0);
				if(a.equals(b)) {
					b.setX(1);
					b.setY(1);
				}
			}
		}
	}
	public Linha2D() {
		this.a = new Ponto2D(0, 0);
		this.b = new Ponto2D(1, 1);
	}
	public Ponto2D getA() {
		return a;
	}
	public void setA(Ponto2D a) {
		if(!a.equals(b)) {
			this.a = a.clone();
		}
	}
	public Ponto2D getB() {
		return b;
	}
	public void setB(Ponto2D b) {
		if(!a.equals(a)) {
			this.b = b.clone();
		}
	}
	@Override
	protected Linha2D clone() {
		return new Linha2D(this.a, this.b);
	}
	@Override
	public String toString() {
		return String.format("[%s%s]", a, b);
	}
	@Override
	public float area() {
		return 0;
	}
	@Override
	public float tamHorizontal() {
		return a.distHorizontal(b);
	}
	@Override
	public float tamVertical() {
		return a.distVertical(b);
	}
	@Override
	public float perimetro() {
		return 0;
	}
	@Override
	public Ponto2D centro() {
		return new Ponto2D ((a.getX() + b.getX()) / 2, (a.getY() + b.getY()) / 2); 
	}

}
