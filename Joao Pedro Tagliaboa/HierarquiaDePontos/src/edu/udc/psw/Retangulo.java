package edu.udc.psw;

public class Retangulo extends FormaGeometrica2D {
	Ponto2D a;
	Ponto2D b;

	public Retangulo(Ponto2D a, Ponto2D b) {
		this.a = a.clone();
		this.b = b.clone();
	}
	
	public Retangulo() {
		a = new Ponto2D(0, 0);
		b = new Ponto2D(1, 1);
	}
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return new Retangulo(a, b);
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(this.getClass() != obj.getClass())
			return false;
		if(this == obj)
			return true;
		//if(this.a.equals(((Retangulo)obj).a) && this.b.equals(((Retangulo)obj).b))
		//	return true;
		if(centro().equals(((Retangulo)obj).centro()) && tamHorizontal() == ((Retangulo)obj).tamHorizontal() && tamVertical() == ((Retangulo)obj).tamVertical())
			return true;
		return false;
	}

	@Override
	public String toString() {
		return String.format("{%s%s}", a, b);
	}
	
	public Ponto2D getA() {
		return a;
	}
	public void setA(Ponto2D a) {
		this.a = a;
	}
	public Ponto2D getB() {
		return b;
	}
	public void setB(Ponto2D b) {
		this.b = b;
	}

	@Override
	public float area() {
		return a.distHorizontal(b) * a.distVertical(b);
	}

	@Override
	public float tamHorizontal() {
		return a.distHorizontal(b);
	}

	@Override
	public float tamVertical() {
		return a.distVertical(b);
	}

	@Override
	public float perimetro() {
		return (a.distHorizontal(b) + a.distVertical(b)) * 2;
	}

	@Override
	public Ponto2D centro() {
		return new Ponto2D((a.getX() + b.getX())/2, (a.getY() + b.getY())/2);
	}
	

}
