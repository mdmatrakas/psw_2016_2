package edu.udc.psw;

public class Ponto2D extends FormaGeometrica2D {
	private int x;
	private int y;
	
	public Ponto2D() {
		this.x = 0;
		this.y = 0;
	}
	public Ponto2D(int x, int y) {
		this.x = x;
		this.y = y;
	}
	public int getX() {
		return x;
	}
	public void setX(int x) {
		this.x = x;
	}
	public int getY() {
		return y;
	}
	public void setY(int y) {
		this.y = y;
	}
	@Override
	public Ponto2D clone() {
		return new Ponto2D(x, y);
	}
	@Override
	public String toString() {
		return String.format("(%d; %d)", x, y);
	}
	@Override
	public boolean equals(Object obj) {
		if(obj == null)
			return false;
		if(this.getClass() != obj.getClass())
			return false;
		if(this == obj)
			return true;
		if(this.x == ((Ponto2D)obj).x && this.y == ((Ponto2D)obj).y)
			return true;
		return false;
	}
	public float distancia(Ponto2D p) {
		// d = sqrt( (x2 - x1)^2 + (y2 - y1)^2 )
		
		return (float) Math.sqrt((x - p.x) * (x - p.x) + (y - p.y) * (y - p.y));
		
	}
	public int distHorizontal(Ponto2D p) {
		return Math.abs(x - p.x);
	}
	public int distVertical(Ponto2D p) {
		return Math.abs(y - p.y);
	}
	@Override
	public float area() {
		return 0;
	}
	@Override
	public float tamHorizontal() {
		return 0;
	}
	@Override
	public float tamVertical() {
		return 0;
	}
	@Override
	public float perimetro() {
		return 0;
	}
	@Override
	public Ponto2D centro() {
		return this;
	}

}
