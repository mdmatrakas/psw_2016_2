package edu.udc.psw;

public class Principal3 {

	public static void main(String[] args) {
		Retangulo r1 = new Retangulo(new Ponto2D(2,3), new Ponto2D(10,7));
		
		System.out.println("Area " + r1.area());
		System.out.println("Perimetro " + r1.perimetro());
		System.out.println("Centro " + r1.centro());
		System.out.println("Horizontal " + r1.tamHorizontal());
		System.out.println("Vertical " + r1.tamVertical());
		
		Retangulo r2 = new Retangulo(new Ponto2D(2,3), new Ponto2D(10,7));
		Retangulo r3 = new Retangulo(new Ponto2D(2,7), new Ponto2D(10,3));
		Retangulo r4 = new Retangulo(new Ponto2D(2,2), new Ponto2D(10,10));
		
		System.out.println("r1 " + (r1.equals(r2) ? "igual " : "diferente ") + "r2");
		System.out.println("r1 " + (r1.equals(r3) ? "igual " : "diferente ") + "r3");
		System.out.println("r1 " + (r1.equals(r4) ? "igual " : "diferente ") + "r4");

	}

}
