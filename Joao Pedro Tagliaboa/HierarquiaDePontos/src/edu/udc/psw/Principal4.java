package edu.udc.psw;

public class Principal4 {

	public static void main(String[] args) {
		FormaGeometrica2D vetor[] = new FormaGeometrica2D[10];
		
		for(int i = 0; i < 10; i++) {
			if(i % 3 == 0)
				vetor[i] = new Ponto2D(i, 2*i);
			if(i % 3 == 1)
				vetor[i] = new Linha2D(new Ponto2D(i, i+1), new Ponto2D(3*1, i/2));
			if(i % 3 == 2)
				vetor[i] = new Retangulo(new Ponto2D(i, 3*i), new Ponto2D(2*i, i*i));
		}
		
		for(int i = 0; i < 10; i++)
			System.out.println(vetor[i]);
	}

}
