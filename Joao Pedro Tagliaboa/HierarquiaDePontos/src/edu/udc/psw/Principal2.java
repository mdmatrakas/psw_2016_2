package edu.udc.psw;

import java.util.Scanner;

public class Principal2 {

	public static void main(String[] args) {
		Ponto2D pontos[];
		
		pontos = new Ponto2D[10];
		
		Scanner sc = new Scanner(System.in);
		
		for(int i = 0; i < 3; i++) {
			System.out.println("Entre as coordenadas de um ponto:");
			pontos[i] = new Ponto2D(sc.nextInt(), sc.nextInt());
		}
		
		System.out.println("Os pontos informados s�o:");
		for(int i = 0; i < 3; i++) {
			System.out.println(pontos[i]);
			System.out.println("O centro � " + pontos[i].centro());
		}		
		sc.close();

	}

}
