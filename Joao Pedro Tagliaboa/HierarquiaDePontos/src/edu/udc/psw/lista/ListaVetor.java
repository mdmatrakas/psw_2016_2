package edu.udc.psw.lista;

public class ListaVetor implements Lista {
	private class IteradorLista implements Iterador
	{
		public int indiceAtual;
		
		public IteradorLista(int indice)
		{
			indiceAtual = indice;
		}
		
		@Override
		public Object dado() {
			return vetor[indiceAtual];
		}

		@Override
		public Object proximo() {
			if(indiceAtual < tamanho)
			{
				indiceAtual++;
				return vetor[indiceAtual-1];
			}
			return null;
		}

		@Override
		public Object anterior() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean temProximo() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public boolean temAnterior() {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public int insereAntes(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}

		@Override
		public int insereDepois(Object obj) {
			// TODO Auto-generated method stub
			return 0;
		}
		
	}
	
	private Object vetor[];
	
	private int tamanho;
	private int numAcessos;
	
	public ListaVetor(int tam)
	{
		vetor = new Object[tam];
		tamanho = 0;
		numAcessos = 0;
	}
	
	public Iterador inicio()
	{
		return new IteradorLista(0);
	}
	
	public Iterador fim()
	{
		return new IteradorLista(tamanho-1);
	}
	
    public int tamanho()
	{
		return tamanho;
	}
	
	public int numTotalAcessos()
	{
		return numAcessos;
	}
	
	public int insere(Object obj, int pos)
	{
		return tamanho;
	}
	
	public Object remove(int pos)
	{
		return null;
	}
	
	public Object consulta(int pos)
	{
		return null;
	}
}
