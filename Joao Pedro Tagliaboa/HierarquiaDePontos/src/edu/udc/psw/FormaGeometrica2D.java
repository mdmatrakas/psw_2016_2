package edu.udc.psw;

public abstract class FormaGeometrica2D {
	//Area
	//largura
	//altura
	//centro
	//circulo que inscreve a forma geometrica
	//perimetro
	
	public abstract float area();
	public abstract float tamHorizontal();
	public abstract float tamVertical();
	public abstract float perimetro();
	public abstract Ponto2D centro();
	
}
