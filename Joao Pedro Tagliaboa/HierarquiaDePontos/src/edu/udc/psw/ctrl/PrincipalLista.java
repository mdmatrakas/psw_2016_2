package edu.udc.psw.ctrl;

import edu.udc.psw.Linha2D;
import edu.udc.psw.Ponto2D;
import edu.udc.psw.Retangulo;
import edu.udc.psw.lista.ListaDupEnc;

public class PrincipalLista {

	public static void main(String[] args) {
		ListaDupEnc lista = new ListaDupEnc();
		
		for(int i = 0; i < 10; i++) {
			if(i % 3 == 0)
				lista.insere(new Ponto2D(i, 2*i), 1);
			if(i % 3 == 1)
				lista.insere(new Linha2D(new Ponto2D(i, i+1), new Ponto2D(3*1, i/2)), 7);
			if(i % 3 == 2)
				lista.insere(new Retangulo(new Ponto2D(i, 3*i), new Ponto2D(2*i, i*i)), 7);
		}
		
		for(int i = 0; i < 10; i++)
			System.out.println(lista.consulta(i));
	}

}
