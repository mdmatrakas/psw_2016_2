package edu.udc.psw.ctrl;

import java.util.Scanner;

import edu.udc.psw.Linha2D;
import edu.udc.psw.Ponto2D;

public class Principal {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("Digite os valores x e y para um ponto:");
		Ponto2D p1 = new Ponto2D(sc.nextInt(), sc.nextInt());
		System.out.println("Digite os valores x e y para outro ponto:");
		Ponto2D p2 = new Ponto2D(sc.nextInt(), sc.nextInt());
		
		sc.close();
		
		System.out.println("Os pontos s�o:" + p1 + " e " + p2);
		
		if(p1.equals(p2))
			System.out.println(p1 + " � igual a " + p2);
		else
			System.out.println(p1 + " � diferente de " + p2);
		
		Linha2D linha = new Linha2D(p1, p2);
		System.out.println("A linha formada pelos pontos � " + linha);
		
		Ponto2D p = p1.clone();
		
		Linha2D l = new Linha2D(p1, p);
		System.out.println("A nova linha formada � " + l);
		
		p1.setX(25);
		System.out.println("A linha1 � " + linha);
		System.out.println("A linha2 � " + l);
		
		System.out.printf("A distancia ente p1 e p � %f\n\n", p1.distancia(p));
		
		System.out.println("A �rea da linha1 � " + linha.area());
		System.out.println("O tamanho horizontal da linha1 � " + linha.tamHorizontal());
		System.out.println("O tamanho vertical da linha1 � " + linha.tamVertical());
		System.out.println("O perimetro da linha1 � " + linha.perimetro());
		System.out.println("O centro da linha1 � " + linha.centro());
		
	}

}
