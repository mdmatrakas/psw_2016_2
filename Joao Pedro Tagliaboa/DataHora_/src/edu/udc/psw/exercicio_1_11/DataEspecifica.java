package edu.udc.psw.exercicio_1_11;

public class DataEspecifica extends Hora {
	private int dia;
	private int mes;
	private int ano;
	
	public int comparaDataEspecifica(DataEspecifica obj) {
		if(obj.ano < this.ano)
			return -1;
		if(obj.ano > this.ano)
			return 1;
		
		if(obj.mes < this.mes)
			return -1;
		if(obj.mes > this.mes)
			return 1;
		
		if(obj.dia < this.dia)
			return -1;
		if(obj.dia > this.dia)
			return 1;

		return comparaHora(obj);
	}
	
	/**
	 * @return the dia
	 */
	public int getDia() {
		return dia;
	}
	/**
	 * @param dia the dia to set
	 */
	public void setDia(int dia) {
		this.dia = dia;
	}
	/**
	 * @return the mes
	 */
	public int getMes() {
		return mes;
	}
	/**
	 * @param mes the mes to set
	 */
	public void setMes(int mes) {
		this.mes = mes;
	}
	/**
	 * @return the ano
	 */
	public int getAno() {
		return ano;
	}
	/**
	 * @param ano the ano to set
	 */
	public void setAno(int ano) {
		this.ano = ano;
	}
	
	
}
