package edu.udc.psw.exercicio_1_11;

public class Hora {
	private int hora;
	private int minuto;
	
	public Hora(int hora, int minuto) {
		setHora(hora);
		setMinuto(minuto);
	}
	
	public Hora() {
		hora = 0;
		minuto = 0;
	}
	
	public void mostraHora() {
		System.out.printf("%02d:%02d", hora, minuto);
	}
	
	@Override
	protected Hora clone() throws CloneNotSupportedException {
		Hora novaHora = new Hora(hora, minuto);
		novaHora.hora = this.hora;
		novaHora.minuto = this.minuto;
		return novaHora;
	}
	
	/**
	 * 
	 * @return Negativo se o obj < hora,
	 * zero se iguais,
	 * positivo se obj maior que this.
	 */
	
	public int comparaHora(Hora obj) {
		if(obj.hora < this.hora)
			return -1;
		if(obj.hora > this.hora)
			return 1;
		
		if(obj.minuto < this.minuto)
			return -1;
		if(obj.minuto > this.minuto)
			return 1;
		
		return 0;
	}
	
	@Override
	public String toString() {
		return String.format("%02d:%02d", hora, minuto);
	}
	
	public int getHora() {
		return hora;
	}
	public void setHora(int hora) {
		this.hora = hora;
	}
	public int getMinuto() {
		return minuto;
	}
	public void setMinuto(int minuto) {
		this.minuto = minuto;
	}

}
