package edu.udc.psw.exercicio_1_11;

public class Principal {
	
	public static void main(String[] args) {
		Data data = new Data();
		
		data.setAno(2016);
		data.setMes(8);
		data.setDia(16);
		
		data.mostraData();
		
		System.out.print(data.toString()+"\n");
		
		DataEspecifica de = new DataEspecifica();
		de.setAno(2016);
		de.setMes(8);
		de.setDia(16);
		de.setHora(19);
		de.setMinuto(47);
		System.out.println(de.toString());
		
		Object obj;
		obj = de;
		System.out.println(obj);
		de.setMes(11);
		System.out.println(obj);
		
		DataHora dh = new DataHora();
		dh.setData(data);
		Hora h = new Hora(20, 5);
		dh.setHora(h);
		System.out.println(dh);
		System.out.println(dh.getHora());
		System.out.println(dh.getData());
		
	}

}
