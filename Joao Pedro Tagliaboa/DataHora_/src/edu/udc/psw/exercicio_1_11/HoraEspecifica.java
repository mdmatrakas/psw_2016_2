package edu.udc.psw.exercicio_1_11;

public class HoraEspecifica extends Data {
	private int hora;
	private int minuto;
	
	public int comparaHoraEspecifica(HoraEspecifica obj) {
		int comp = comparaData(obj);
		if(comp == 0) {
			if(obj.hora < this.hora)
				return -1;
			if(obj.hora > this.hora)
				return 1;
			
			if(obj.minuto < this.minuto)
				return -1;
			if(obj.minuto > this.minuto)
				return 1;
			
			return 0;
		}
		return comp;
	}
	
	/**
	 * @return the hora
	 */
	public int getHora() {
		return hora;
	}
	/**
	 * @param hora the hora to set
	 */
	public void setHora(int hora) {
		this.hora = hora;
	}
	/**
	 * @return the minuto
	 */
	public int getMinuto() {
		return minuto;
	}
	/**
	 * @param minuto the minuto to set
	 */
	public void setMinuto(int minuto) {
		this.minuto = minuto;
	}
	
	

}
