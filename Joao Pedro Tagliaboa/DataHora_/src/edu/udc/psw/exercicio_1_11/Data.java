package edu.udc.psw.exercicio_1_11;

public class Data extends Object {
	private int dia;
	private int mes;
	private int ano;
	
	public void mostraData() {
		System.out.printf("%02d/%02d/%04d\n", dia, mes, ano);
	}
	
	public String toString() {
		return String.format("%02d/%02d/%04d", dia, mes, ano);
	}
	
	/**
	 * 
	 * @return Negativo se o obj < hora,
	 * zero se iguais,
	 * positivo se obj maior que this.
	 */
	
	public int comparaData(Data obj) {
		if(obj.ano < this.ano)
			return -1;
		if(obj.ano > this.ano)
			return 1;
		
		if(obj.mes < this.mes)
			return -1;
		if(obj.mes > this.mes)
			return 1;
		
		if(obj.dia < this.dia)
			return -1;
		if(obj.dia > this.dia)
			return 1;
		
		return 0;
	}
	
	public int getDia() {
		return dia;
	}
	public void setDia(int dia) {
		this.dia = dia;
	}
	public int getMes() {
		return mes;
	}
	public void setMes(int mes) {
		this.mes = mes;
	}
	public int getAno() {
		return ano;
	}
	public void setAno(int ano) {
		this.ano = ano;
	}

}
